#########
Abenteuer
#########

*******
Elisera
*******

0. Charaktererstellung
======================

B (richtiger Name nicht gekannt)
--------------------------------

- Elf
- Dieb
- 19 Jahre


Berengar Eisenhauer
-------------------

- Feuerzwerg
- Priester (Barros)


Dellongr Hammerschlag
---------------------
- auf dem Weg nach Padora


Wilma Wetterquarz
-----------------

- Kind von Wilbur Wetterquarz (bekannter Gnommagier)
- 15


1. Das Abenteuer beginnt
========================

2. Die Reise zum Gamskogel
==========================

3. Die Helka-Enge
=================

4. Die Orkhöhlen
================

.. image:: /image/hoehle.png