.. Aborea documentation master file, created by
   sphinx-quickstart on Sat Nov 18 18:29:16 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


######
Aborea
######

.. toctree::
   :maxdepth: 2
   :glob:

   abenteuer
   welt
   regeln