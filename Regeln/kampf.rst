*****
Kampf
*****

Ablauf
======

.. sidebar:: Checkliste

   #. Ermitteln Sie die Initiative
   #. Ermitteln Sie den Kampfbonus (Attributsbonus + Waffen-Rang + Magie)
   #. Legen Sie Ihren Offensivbonus fest
   #. Sagen Sie die Aktionen an (die niedrigste Initiative zuerst)
   #. Wickeln Sie die Angriffe der Initiative nach ab (höchste beginnt)
   #. Würfeln Sie den Würfelwurf
   #. Ermitteln Sie den Angriffswert: Würfelwurf + Offensivbonus + Reichweiten-Malus
   #. Stellen Sie fest, ob getroffen wurde: Angriffswert > Verteidigungswert (Rüstung + Defensivbonus) des Gegners.
   #. Wenn ein Treffer erzielt wurde, dann ermitteln und verteilen Sie den Schaden: Angriffswert + Schadenswert der Waffe Verteidigungswert des Gegners = Schadenspunkte (Bei kritischen Treffern wird der Schadenswert der Waffe zweimal eingerechnet.)
   #. Wenn der Kampf nicht beendet wurde, beginnt nun eine neue Runde.
      Beachten Sie, bevor Sie bei 1. wieder beginnen, etwaige Blutungen (sofern Sie mit dieser Option spielen).

Der Kampf wird in Runden unterteilt, von denen jede 6 Sekunden dauert.
Obwohl alle Beteiligten an einem Kampf innerhalb dieser einen Runde handeln, werden die Aktionen nacheinander behandelt.
Die Reihenfolge wird dabei mittels der Initiative bestimmt.
Der Kampfteilnehmer, der den höchsten Initiativewert hat, besitzt daher einen enormen Vorteil: Sollte sein Angriff den anderen außer Gefecht gesetzt haben, dann kann dieser nicht mehr zurückschlagen.

Es gibt noch einen weiteren Vorteil: Der Teilnehmer, der die niedrigste Initiative hat, sagt seine Aktion als erster an.
Der Initiative-Sieger hat daher einen Informationsvorsprung und kann seine Aktion daran anpassen.


Initiative
----------
Die initiative (INI) ist gleich dem Attributsbonus der Geschicklichkeit.
Einige Waffen geben auch einen Initiative-Bonus oder Malus.
Dieser wird hinzugerechnet.

Der mit dem höchsten Initiativewert gewinnt diese.
Haben zwei oder mehr Teilnehmer die gleiche Initiative, dann würfeln sie es aus.
Der mit dem höchsten Wurf hat die Initiative gewonnen.


Ansagen der Aktion
------------------

Jeder Teilnehmer sagt seine Aktion an.
Der Kampfbeteiligte mit der niedrigsten Initiative beginnt mit dem Ansagen.
Eine einmal angesagt Aktion kann nicht mehr geändert werden.

Man muss auch ansagen, wie offensiv oder defensiv man sich verhält (siehe auch: Kampfbonus).


Durchführen der Aktionen
------------------------

Die angesagten Aktionen werden nun mit der höchsten Initiative beginnend abgewickelt.

Manöver werden nach den Manöver-Regeln (Seiten 29f.) erledigt.
Alle Angriffe und besonderen Kampfaktionen werden nach den folgenden Regeln gespielt.
Zunächst wird der reguläre Angriff beschrieben und dann die besonderen Kampfaktionen.


Angriff
-------

Ein Angriff war erfolgreich, wenn der Angriffswert den Verteidigungswert übersteigt.
Der Schaden wird auf die gleiche Art ermittelt.

.. admonition:: Beispiel
   :class: admonition note

   Erreicht der Angreifer beispielsweise einen Angriffswert von 12 und hat der Gegner einen Verteidigungswert von 8, dann liegt ein Treffer vor.
   Dabei wurde dann ein Schaden von 12 - 8 = 4 erzielt.
   Diesen Schaden muss der Verteidiger nun von seinen Trefferpunkten abziehen.

Je höher das Ergebnis ist, desto effektiver war der Angriff.
Einige Waffen verursachen bei einem Treffer einen geringeren oder höheren Schaden.
Aber zunächst der Reihe nach die Modifikatoren.


Kampfbonus
----------

Als erstes wird der Kampfbonus ermittelt.
Dieser besteht aus dem Attributsbonus und dem Rang der Waffenfertigkeit.
Welches Attribut zu beachten ist und welche Waffenfertigkeit, hängt von der jeweiligen Waffe ab.

.. admonition:: Beispiel
   :class: admonition note

   Verwendet der Angreifende einen Zweihänder, dann darf er seinen Rang aus der Waffenfertigkeit (Lange Klingenwaffen) und seinen Attributsbonus (Stärke) einrechnen.

Manche Waffen sind mit zwei Attributen angegeben.
Dann dürfen Sie sich den höheren Bonus aussuchen.
Es darf aber immer nur ein Attributsbonus hinzuaddiert werden.

Einige Waffen gehören mehreren Waffengruppen an.
Sie müssen die Gruppe verwenden, die der konkreten Benutzung entspricht.

.. admonition:: Beispiel
   :class: admonition note

   Verwenden Sie beispielsweise einen Dolch zum Werfen, dann ist die Waffengruppe „ Wurfwaffen" einschlägig.

Ist der Angreifer in der entsprechenden Waffengruppe ungeübt (kein Rang), dann ist ein Malus von -2 einzurechnen.
Im Ergebnis kann der Kampfbonus negativ werden.
Bitte beachten Sie, dass Krieger den Malus für „ungeübt“ nicht erhalten (siehe oben unter der Fertigkeit „Waffen”).

Des Weiteren können magische Eigenschaften einer Waffe den Kampfbonus erhöhen.

Als Formel ausgedrückt bedeutet dies:

Kampfbonus = Attributsbonus + WaffengruppenRang + Waffenbonus

Der Kampfbonus bleibt in der Regel während des gesamtes Kampfes gleich.

Den Kampfbonus können Sie nun nach Belieben für die Offensive oder die Defensive einsetzen.
Allerdings müssen Sie dies bei der Ansage der Aktion mit angeben.

.. note::

   Möchte sich ein Unbewaffneter (lediglich) verteidigen, so wird sein Rang aus der Fertigkeit Waffen (Waffenlos) bei der Berechnung des Kampfbonus eingerechnet.


Offensivbonus
-------------

Sie können sich entscheiden, Ihren gesamten Kampfbonus oder nur Teile davon für die Offensive einzusetzen.
Je mehr Sie einsetzen, desto größer wird die Wahrscheinlichkeit, dass Sie einen Treffer landen werden und dass dieser besonders schwer wird.

Zugleich verringert ein hoher Offensivbonus aber den Defensivbonus im Extremfall sogar bis Null.
Sie könnten dann leichter getroffen werden.

Sie können jede Runde den Offensivbonus neu bestimmen.
Beachten Sie aber, dass Sie dies auch vorher ansagen müssen.

Der Offensivbonus wird auch mit OB abgekürzt.


Würfelwurf für den Angriff
--------------------------

Bei jedem Angriff würfeln Sie einen Würfel.
Eine 1 bedeutet immer einen Fehlschlag und eine 10 ist „nach oben offen”.
Sie dürfen bei einem „nach oben offenen" Wurf erneut würfeln und die Ergebnisse zusammenaddieren.
Eine 10 ist zudem auch ein kritischer Treffer, bei dem der Schaden nochmals erhöht wird (siehe weiter unten).


Angriffswert
------------

Um nun den Angriffswert zu bestimmen, rechnen Sie zunächst das Ergebnis des Würfelwurfs und ihren Offensivbonus zusammen.

Im Fernkampf werden noch Malusse für die Reichweite eingerechnet.

Der Spielleiter kann weitere Abzüge bestimmen zum Beispiel auf Grund schlechter Sicht.

Als Formel ausgedrückt bedeutet dies:

Angriffswert = Würfelwurf + Offensivbonus des Angreifenden ± Situationsmodifikatoren


Verteidigungswert
-----------------

Der Verteidigungswert eines Kampfbeteiligten besteht aus der Rüstung und dem Defensivbonus.


Rüstung
-------

Der Wert der Rüstung eines Kampfbeteiligten ist 5 + Rüstungsbonus.
Der Rüstungsbonus ist unterschiedlich und hängt davon ab, welche Rüstung der Kampfbeteiligte verwendet (siehe Tabelle Rüstungen, Seite 54).
Auch Magie kann auf den Rüstungsbonus wirken.


Defensivbonus
-------------

Falls nicht der gesamte Angriffswert für den Offensivbonus verwendet wurde, darf der nicht genutzte Teil als Defensivbonus eingesetzt werden.

Der Defensivbonus verringert die Trefferchance des Angreifers (Nahkampf, Fernkampf und gezielte Zauber).
Er wird mit DB abgekürzt.

Bei mehreren Angreifern kann der DB aufgeteilt werden.
Reicht der DB nicht für die Anzahl der Angreifer, dann wird er nur bei denen abgezogen, für die er gereicht hat.

.. admonition:: Beispiel
   :class: admonition note

   Ein Charakter hat einen DB von 4.

   Er wird von 6 Personen attackiert: 2 Nahkämpfer vor ihm und vier Fernkämpfer.
   Er entscheidet, seine Aufmerksamkeit den beiden Nahkämpfern zu widmen, die er für bedrohlicher hält.
   Einer der beiden Angreifer ist stärker, weshalb er für ihn 3 Punkte im DB bestimmt und für den anderen einen.
   Die Fernkämpfer erhalten keinen Abzug durch seinen DB, da keiner mehr übrig ist (3 DB für den ersten Nahkämpfer +1 DB für den zweiten Nahkämpfer = 4).

Ein negativer Kampfbonus wirkt sich nicht auf den DB aus, sondern ist vollständig dem OB zuzurechnen.


Treffer
-------

Der Angriff ist erfolgreich, wenn der Angriffswert größer als der Verteidigungswert ist.


Schaden des Angriffs
--------------------

Wenn ein Treffer erzielt wurde, dann darf der Schaden errechnet werden.
Der Angriffswert ergibt nicht nur das Ob, sondern auch das Wie des Angriffs.
Der Angriffswert des Angreifers weniger den Verteidigungswert des Verteidigers ergibt die Höhe des Schadens.
Bei einigen Waffen wird allerdings der Schaden verringert oder kann erhöht werden.
Dazu wird der Schadenswert zum Schaden hinzuaddiert.

Als Formel aufgeschrieben heißt dies:

Schaden = Angriffswert - Verteidigungswert + Schadenswert der Waffe

.. admonition:: Beispiel
   :class: admonition note

   Der Angreifer hat mit seinem Langschwert einen Angriffswert von 10 erreicht.
   Der Verteidiger hat einen Verteidigungswert von 5.
   10 weniger 5 ergibt 5.
   Das Langschwert hat zudem einen Schaden von +1.
   Demnach ist der Schaden 5 + 1 = 6.
   Der Angegriffene muss 6 Trefferpunkte streichen.

Bei einem Treffer wird immer mindestens ein Schadenspunkt erzielt.

Wie man verlorene Trefferpunkte zurückerhält, siehe weiter unten unter „Heilen“.


Kritische Treffer
-----------------

Wurde bei dem Würfelwurf eine 10 geworfen, dann darf jeder positive (Bonus-) Schaden einer Waffe bei der Schadensberechnung zweimal eingerechnet werden.


.. admonition:: Beispiel
   :class: admonition note

   Der Angreifer hat mit seinem Langschwert einen Angriffswert von 12 erreicht.
   Der Verteidiger hat einen Verteidigungswert von 5.
   12 weniger ergibt 7.
   Das Langschwert hat zudem einen Schaden von +1.
   Demnach ist der Schaden 7 + 1 = 8.
   Er hatte allerdings auch eine 10 beim Würfelwurf.
   Der Angriff ist daher kritisch und der Waffenschaden darf nochmals eingerechnet werden: 7 + 1 + 1 = 9.
   Der Angegriffene muss 9 Trefferpunkte streichen.

Wenn Sie mit der Regel-Option „Blutungen“ spielen, dann sollte ein kritischer Treffer immer mindestens eine Blutung verursachen.


Optionale Regel: Blutungen
--------------------------

Manche Angriffe sind so immens, dass der Treffer auch in späteren Runden fortwirkt.
Wurde bei einem einzelnen Angriff ein Schaden von 5 oder mehr erzielt, dann blutet der Angegriffene fortan.
Bis die Blutung gestillt wurde, verliert der Angegriffene nun einen Trefferpunkt pro Runde.
Dies gilt für je 5 Schadenspunkte.
Wurde mit einem einzelnen Angriff ein Vielfaches von 5 an Schaden angerichtet, dann ist dieses Vielfache die Höhe der Blutung.

Diese Regel ist optional.
Die Gruppe entscheidet gemeinsam zu Beginn über die Anwendung der RegeL

Falls Sie mit „Blutungen” spielen, dann sollte ein kritischer Treffer immer mindestens eine Blutung verursachen unabhängig vom erzielten Schaden.

Wie man verlorene Trefferpunkte zurückerhält und Blutungen stillen kann, siehe weiter unten unter „Heilen”.


Nächster Kampfbeteiligter
-------------------------

Nach der Abwicklung des ersten Angriffs werden alle nachfolgenden Aktionen ausgeführt.
Dies wird solange wiederholt, bis jeder Teilnehmer seine Aktion hatte.

Falls Sie den ersten Angriff hatten und Ihr Gegner noch lebt, dann ist nun Ihr Gegner an der Reihe.
Er wird versuchen, Sie zu treffen.


Rundenende
----------

Wenn jeder Beteiligte, der noch aktionsfähig war, seine Aktion vollzogen hat, dann endet die Runde.
Sofern der Kampf nicht beendet wurde, beginnt nun eine neue Runde.


Reichweiten-Malusse
-------------------

.. csv-table::
   :file: Kampf/reichweite.csv
   :header-rows: 1
   :delim: ;


Besondere Kampfaktionen
=======================

In einem Kampf dürfen Sie neben Angriffen und Manövern auch besondere Kampfaktionen vornehmen.
Diese sind im Folgenden beschrieben.


Bewegung im Kampf
-----------------

Während eines Kampfes darf ein Charakter sich 6m weiter bewegen und zugleich angreifen.
Er kann sich auch doppelt so schnell bewegen, erhält aber auf seinen Kampfbonus einen Malus von -4 in der Runde.

Für Bewegungen außerhalb des Kampfes siehe Seite 55.


Deckung/Hindernisse
-------------------

In bestimmten Situationen kann es sein, dass ein Gegner ganz oder teilweise verdeckt wird.
Der Spielleiter bestimmt dann einen Situationsmodifikator.
Näheres dazu im Spielleiterheft.


Waffen ziehen
-------------

Sollten Sie noch keine Waffen gezogen haben, als der Kampf beginnt, dann können Sie dies jederzeit noch tun.
Allerdings verringert sich Ihre Initiative dadurch für die Dauer des Kampfes um 1.


Waffe wechsel
-------------

Wenn Sie die Waffe wechseln, dann verringern Sie Ihre Initiative für die Dauer des Kampfes um 1.
Wenn die neue Waffe einen anderen Initiativebonus besitzt als die alte Waffe, so muss die Initiative neu berechnet werden: Geschicklichkeitsbonus + Initiative-Bonus/Malus (Waffe) -1.
Findet der Waffenwechsel nach einem Angriff statt, so gilt die veränderte Initiative erst in der folgenden Runde.
Der Wechsel einer Waffe während eines Mehrfachangriffes (siehe „Kampf mit zwei Waffen" im Spielleiterheft) ist nicht möglich.


Nachladen
---------

Manche Waffen müssen nachgeladen werden.
Einen Bogen können Sie jederzeit nachladen.
Sie dürfen allerdings nur einmal pro Runde schießen.

Das Laden einer Armbrust dauert hingegen eine Runde.
Sie können in der Runde dann nicht schießen, sondern erst in der folgenden.


Überraschung
------------

Wenn Sie Ihren Gegner überraschen (er Sie also vorher nicht wahrgenommen hat), dann gewinnen Sie die Initiative in der ersten Runde.
Der überraschte Gegner kann auch keinen |DB| gegen den Überraschungsangriff einsetzen.

Sobald der Gegner den Angriff bemerkt, ist die Überraschung vorbei.
In der Regel gibt es daher die Möglichkeit der Überraschung nur in der ersten Runde.

Diebe können bei einer Überraschung Ihren verursachten Schaden verdoppeln (siehe auch oben unter dem Beruf „Dieb").


Flucht
------

Aus einem (Nah-) Kampf zu flüchten ist schwer.
Wenn der Flüchtende eine niedrigere Initiative hat, dann erhält der Andere noch einen Angriff auf den Flüchtenden, bevor er frei laufen kann.
Hat er eine schnelle Initiative, dann kann der andere zwar auch noch einmal angreifen, aber der Flüchtende erhält einen Bonus auf seinen |DB| in Höhe der Differenz in der Initiative.

.. admonition:: Beispiel
   :class: admonition note

   Der Flüchtende hat eine Initiative von +2 und der Gegner eine Initiative von -1.
   Wenn der Flüchtende nun die Flucht antritt, kann der Gegner (noch) einmal auf ihn einschlagen (ohne dass der Flüchtende angreifen kann der flüchtet schließlich und kann keine andere/weitere Aktion vornehmen).
   Allerdings hat der Flüchtende eine um 3 bessere Initiative.
   Diese Differenz (2 - -1 = 3) erhält er als Bonus auf seinen |DB|.

Einmal die Flucht angetreten, kann natürlich eine Verfolgung passieren.
Dabei kommt es dann darauf an, wer schneller und ausdauernder ist (siehe Bewegung und Athletik).


Tod und Verletzungen
====================

Der Charakter ist verletzt, wenn er Trefferpunkte verloren hat oder blutet.
Er ist auch verletzt, wenn er einen Attributsschaden erhalten hat.
So einen Schaden kann beispielsweise durch Magie angerichtet werden näheres dazu im Spielleiterheft.

Bei genau Null Trefferpunkten wird der Charakter ohnmächtig.
Ausgenommen sind davon Krieger und Zwerge.

Sinken die Trefferpunkte unter Null, dann stirbt der Charakter grundsätzlich.
Sinkt ein Attribut auf oder unter Null, dann stirbt der Charakter ebenfalls.


Trefferpunkte
-------------

Die Anzahl der Trefferpunkte bestimmt sich anhand des Berufes des Charakters, seines Konstitutionsbonusses und seiner Stufe.
Die Formel dazu lautet: (TP des Berufs + KO-Bonus) x Stufe

Zauberer erhalten 4 TP (zuzüglich KO-Bonus) pro Stufe, Diebe, Barden und Schamanen 6, Waldläufer und Priester 8 und Krieger 10.


Heilen
------

Die Charaktere heilen auf natürliche Weise, indem sie pro Tag 1 + ihren Konstitutionsbonus regenerieren (mindestens aber 1).

Ein erfolgreiches einfaches Manöver mit Wissen (Heilkunst) kann die natürliche Heilung beschleunigen.
Je höher der Erfolg des Manövers ist, desto schneller heilt der Charakter.

Ein einfacher Erfolg verdoppelt die Genesungsrate.
Ein Erfolg, der 5 höher als gefordert ist, verdreifacht die Rate.
Und wenn sogar mehr als 10 darüber erreicht wurden, dann heilt der Charakter mit dem vierfachen Konstitutionsbonus pro Tag.

Vergiftete Charakter können auch geheilt werden.
Allerdings ist das Manöver um den Grad des Giftes erschwert.

Blutungen können ebenfalls gestoppt werden.
Je einem Punkt Blutung wird die Manöverschwierigkeit aber um einen Punkt schwerer.

Attributsschäden können nur magisch geheilt werden.

Innerhalb von einigen Minuten (Anzahl der Minuten gleich Stufe des Sterbenden) kann ein sterbender Charakter noch mit einem Heilkunst-Manöver reanimiert werden.
Er hat dann genau Null Trefferpunkte.
Blutungen werden dadurch aber nicht gestillt.
Diese müssen (am besten vorher) gesondert behandelt werden.

Bestimmte Umstände können das Manöver [Wissen (Heilkunst)] erschweren (zum Beispiel kein Zugang zu frischem Wasser) oder erleichtern, wie eine Heilertasche oder Heilkräuter.

Magische Heilung, beispielsweise durch Zauber oder Heiltränke, kann wesentlich stärker wirken.
Wie stark sie wirkt, hängt von der jeweiligen Magie ab.
Nähere informationen finden sich bei den entsprechenden Gegenständen oder Zaubern.


Angriffe durch Zauber
---------------------

Zauberangriffe funktionieren nach den gleichen Regeln wie normale Angriffe, wenn es sich um gezielte Zauber handelt (siehe Seite 40).
Allerdings wird der Kampfbonus durch den entsprechenden Attributsbonus des Berufes (in der Regel Intelligenz, außer beim Priester Charisma) und den Fertigkeitsrang in „Gezielte Sprüche” ermittelt.
Der Kampfbonus kann nur für die Offensive eingesetzt werden.
Nicht gezielte Zauber werden regulär abgewickelt.
Wurde der Magieverwender in der gleichen Runde erheblich verletzt, dann kann der Spielleiter ein Manöver abverlangen.
Dies und weitere Besonderheiten sind auf Seite 40 erläutert.


Beispiel eines Kampfes
======================

Ein Zwergen Krieger und ein menschlicher Waldläufer sind in den Ausläufern des Ghalgrat-Gebirges an einen Troll geraten.

Hier die Werte der Beteiligten:

**Zwerg**, 24 TP, GE-Bonus +0, ST-Bonus +2, EinhandAxt (in der Hand), Zweihand-Axt (nicht in der Hand), Fertigkeit Äxte 4 Ränge, Rüstung 6 (4 durch Platte, 1 durch Schild und 1 durch Größe)

**Waldläufer**, 16 TP, GE-Bonus +2, ST-Bonus +0, Kurzschwert, Fertigkeit Kurze Klingenwaffe 4 Ränge, Rüstung 2 (Kettenhemd)

**Troll**, TP 30, GE-Bonus +0, ST-Bonus +4, Flegel, Fertigkeit Wuchtwaffe 2, Rüstung 1 (Haut)

.. rubric:: Initiative der Beteiligten

Der Waldläufer beginnt, da er den höchsten |GE| Bonus hat (2) und durch das Kurzschwert sogar noch einen Bonus von +1 hat (3).
Der Troll hat den gleichen GE-Bonus (0) wie der Zwerg, zwischen ihnen muss der Würfel entscheiden.
Der Troll erreicht das höhere Ergebnis und ist damit vor dem Zwerg, aber nach dem Waldläufer.

.. rubric:: Kampfbonus

Der Waldläufer hat durch Fertigkeit +4 und durch seinen |GE|-Bonus (er darf beim Kurzschwert entweder den |ST|-Bonus oder den |GE|-Bonus einsetzen) nochmals +2.
Er hat damit insgesamt einen Kampfbonus von 6.

Der Troll hingegen hat +2 durch seine Fertigkeit und +4 von seinem |ST|-Bonus und kommt damit auch auf 6.
Unser Zwerg hat auch insgesamt eine 6 (4 durch die Fertigkeit und 2 durch |ST|-Bonus).

.. rubric:: 1. Runde

Der Waldläufer entscheidet sich, 3 Punkte aus seinem Kampfbonus in den |DB| zu legen.
Damit bleibt ein |OB| von 3.
Der Troll nimmt wütend alles in den Angriff und der Zwergen-Krieger ebenfalls.

Der Waldläufer würfelt eine 8.
Er addiert seinen |OB| von 3 hinzu und erhält einen Angriffswert von 11.
Der Troll hat einen Verteidigungswert von 6 (5 + 1 Rüstung + 0 |DB|).
Der Troll wurde getroffen!
Der Schaden ist 11 - 6 = 5.
Allerdings hat das Kurzschwert einen Schadensmalus von -1.
Der Angriff hat daher „nur” 4 Punkte Schaden angerichtet.
Der Troll hat nun noch 26 TP (30 - 4 = 26).
Nun ist der Troll an der Reihe.
Seine Ur-Feinde sind Zwerge und daher haut er schnaubend auf den kleinen Zwerg ein.
Er würfelt eine 6.
Mit seinem |OB| von 6 beträgt der Angriffswert daher 12 (6 + 6 = 12).
Der Zwerg hat einen Verteidigungswert von 11 (5 + 6 Rüstung + 0 DB = 11).
Der Troll fügt dem Zwerg daher 2 Schadenspunkte zu (Angriffswert 12 Verteidigungswert 11 + 1 Schadensbonus Flegel = 2).
Damit hat der Zwerg noch 22 TP.
Doch nun ist er an dran und hackt auf das stämmige Bein des Trolls ein: Er würfelt unglücklich eine 1 und schlägt damit automatisch daneben.

.. rubric:: 2. Runde

Wieder beginnt unser Waldläufer.
Er würfelt diesmal eine 10.
Das ist ein kritischer Treffer und zudem nach oben offen.
Er würfelt erneut und erzielt eine 3.
Er addiert seinen unveränderten |OB| hinzu (3) und hat damit einen Angriffswert von 16 (1. Wurf 10, 2. Wurf 3 und |OB| 3).
Minus dem Verteidigungswert des Trolls (6) und dem Schadensmalus von -1 (Kurzschwert), erreicht er 9 Schadenspunkte.
Leider gibt es keinen besonderen Bonus für den kritischen Treffer, da seine Waffe keinen positiven Schadensbonus hat.
Der Troll ist nun bei 17 TP.
Diesmal schlägt der Troll mit einer 1 daneben.
Der bislang unglückliche Zwerg wechselt nun seine Waffe.
Da er bereits der Letzte in der Runde ist, hat es keine wesentlichen Auswirkungen auf seine Initiative.
Er hat nun seine Zweihand-Axt dafür hat er seine Einhand-Axt und seinen Schild fallen lassen.
(Der verschlechterte Ini-Wert seiner Waffe hat auch keine großen Auswirkungen.)
Diesmal ist es an ihm, eine 10 zu würfeln!
Er würfelt erneut und erreicht noch eine 8.
Jetzt zählen wir zusammen: 10 für den ersten Wurf, 8 für den zweiten Wurf, 6 für den |OB| und 2 für den Schadensbonus der Waffe und weitere 2 für den kritischen Treffer (28).

Der Troll hatte noch 17 TP und ist damit besiegt!
