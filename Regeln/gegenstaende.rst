***********
Gegenstände
***********

Waffen
======

.. csv-table::
   :file: Gegenstaende/waffen.csv
   :header-rows: 1
   :delim: ;


Rüstungen
=========

.. csv-table::
   :file: Gegenstaende/ruestungen.csv
   :header-rows: 1
   :delim: ;


Items
=====

.. csv-table::
   :file: Gegenstaende/items.csv
   :header-rows: 1
   :delim: ;


Kräuter
=======

.. csv-table::
   :file: Gegenstaende/krauter.csv
   :header-rows: 1
   :delim: ;


Tränke
======

.. csv-table::
   :file: Gegenstaende/traenke.csv
   :header-rows: 1
   :delim: ;


Gifte
=====

.. csv-table::
   :file: Gegenstaende/gifte.csv
   :header-rows: 1
   :delim: ;


.. [#Netz] Wurde ein Treffer erzielt, dann erhält der Gegner fortan einen Malus von -4 auf seinen Kampfbonus, solange, bis es sich befreit hat (sehr schweres Athletik-Manöver)