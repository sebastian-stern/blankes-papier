*****
Magie
*****

Obwohl man sagen würde, dass es leere Räume gibt, sind sie es in Wirklichkeit nicht.
Grundsätzlich ist alles von Magie erfüllt, und alles wird von Magie durchwirkt.
Was diese Magie ist, weiß niemand.
Verwegene Theoretiker halten die Magie für intelligent: Vielleicht ist sie lebendig und eine uns völlig unerklärliche Form von (Kollektiv-) Intelligenz.
Dieser Ansatz vermag die nachfolgenden Beschreibungen noch am besten zu erklären.
Andere sehen in ihr die göttliche Essenz oder sprechen von der lebendigen Aborea und ihrer Lebensenergie.
Viele Gelehrte verwenden daher den Begriff Essenz und Magie gleichwertig.
Auch diese Erklärungsmodelle sind durchaus reizvoll und interessant.
Aber nur die Erläuterungen der Phänomene um die Magie gelten im Gegensatz zu der Erklärung ihres Wesens als hinreichend sicher.


Der erste und der zweite Grundsatz
==================================

Grundsätzlich ist die Magie unsichtbar, und es ist eine außergewöhnliche Fähigkeit, sich ihrer bewusst zu sein.
Vielleicht konnten einmal alle oder zumindest viele sie wahrnehmen und sogar manipulieren.
Aber heute gibt es jedenfalls nur noch wenige, die diese Fähigkeit besitzen.
Diese Wesen sind Träger des Ingeniums.

Es ist nicht klar, ob das lngenium bei allen gleich stark auftritt oder ob andere Faktoren maßgeblich sind.
Eines ist jedoch ein wichtiges Faktum: Man muss an Magie glauben.
Damit ist nicht ein starker Glaube an sich selbst und die Fähigkeit die Magie zu manipulieren gemeint, sondern der Glaube an die Existenz der Magie.
Landstriche, in denen Magie als Ammenmärchen gilt und in das Reich der Träume gehört, weisen deswegen überproportional weniger Wesen auf, die das lngenium besitzen.
Diese Tatsache wird auch der Erste Grundsatz genannt.
Das Nutzen der Magie durch Manipulation ist umso schwieriger, je stärker die Magie ist.
Sie neigt dazu, schon fast chaotisch oder eigenwillig zu werden.
Ein großer Zauberer erfährt immer mehr über die Unbeherrschbarkeit der Magie.
Mancher würde auch sagen, dass die Magie lebt und einen eigenen Willen hat.
Vielleicht ist dies aber auch nur eine Erklärung für das eigene Versagen, die wahre Ursache zu erkennen.
Man ist durchaus in der Lage, schon in den Anfängen der Nutzung die wahre Größe zu erahnen, und es hat schon manchen Anfänger aus Ehrfurcht vertrieben.
Aber mit zunehmender Erfahrung und Wissen erkennt man nur noch mehr die Unwissenheit.
In der Folge sind verschiedenste philosophische, theoretische Ansätze entstanden, wie ein möglichst großer Nutzen der Magie zu ermöglichen wäre.
Diese reichen von einem kooperativen bis hin zu einem befehlenden Umgang mit der großen Macht.
Einig sind sich alle über eines: Magie ist unendlich mächtig.
Diese Aussage ist der Zweite Grundsatz.


Weiße und Schwarze Magie
========================

Gerade der Zweite Grundsatz ist die Ursache für eine entscheidende Entwicklung auf fast ganz Palea.
Jeder ist sich dessen bewusst und erkennt die Magie als großen Machtfaktor an.
Kein Herrscher ist bereit, einen solchen unbeachtet zu lassen.
Daher verwundert es nicht, dass sich schon recht früh in der Geschichte der Kodex durchgesetzt hat.
Der Kodex reguliert den Umgang mit der Magie und organisiert die Nutzer, die man im Allgemeinen auch Zauberer nennt.
Ein weiterer Vorteil der Organisation ist sicherlich, dass es eine strukturierte Forschung und Lehre in Sachen Magie gibt.
Durch regionale und kulturelle Differenzen ist die Organisation auch vielseitig ausgestaltet.
Doch die unterschiedlichen Gesichter basieren alle auf der Einhaltung des Kodexes.
Diese regulierte Form wird die Weiße Magie genannt.
Jede andere Form der Magie ist in nahezu allen Ländern Paleas verboten.
Das Verbot bedeutet allerdings nicht, dass es nicht die anderen Arten gibt.
Wenn es nach den Vertretern der Weißen Magie geht, dann ist alles andere die Schwarze Magie.
Diese Pauschale ist so aber nicht richtig, und es ist vielmehr zwischen den verschiedenen Ansätzen zu unterscheiden.
Allen gemein ist jedenfalls, dass sie sich nicht an den Kodex der Weißen Magie halten.
Dennoch weisen einige Arten auch Regeln und Gesetze usw. auf.
Die Schwarze Magie kennzeichnet sich insbesondere durch den Leitspruch, dass es keine Begrenzungen geben darf, denn diese hindern nur jeden Fortschritt.
Beide großen Gegenströmungen haben ihre Vor- und Nachteile, die aus den grundlegenden Unterschieden resultieren.
Während die Waiße Magie anerkannt und gefördert wird, muss sich die Schwarze Magie verstecken.
Dafür kann sie sich frei bewegen, und die Weiße Magie unterliegt strengen Regulierungen.
Weiter unten finden sich zu den einzelnen Arten nähere Erläuterungen.


Göttliche Leitmagie
===================

Keiner würde je diese These tatsächlich äußern, denn sie ist zu befremdlich (und gefährlich) für die Bewohner Paleas, aber die anfänglichen Ausführungen zur Magie legen es bereits nahe: Die Leitmagie, oder auch göttliche Magie, göttliche Kraft oder göttliche Macht, ist nichts anderes als die „eine" Magie.
Sie wird nur anders wahrgenommen.
Aber bei der Zurückführung der Leitmagie auf die „eine” Magie bewegt man sich auf den Scheiterhaufen zu.
Es mag revolutionär klingen, und sicherlich hat die These auch ihren Charme.
Tatsache ist aber, dass es dann erhebliche Erklärungsnöte gibt, warum die Leitmagie so viel mächtiger und spontaner sein kann.
Es gilt als gesichert, dass die Leitmagie in Notsituationen überaus machtvoll sein kann, wenn der Nutzer besonders treu den Glaubensgrundsätzen handelt.
Das ist ein Verhalten der Magie, das nicht in dem Maße bei der „einen“ Magie, oder Essenz, beobachtet werden kann.
Die tragfähigste Erklärung ist, dass die göttliche Magie von eben den Göttern gelenkt (und fokussiert wird).
Sie wird „geleitet“ weshalb man auch von Leitmagie spricht.
Möglicherweise ist aber auch die „eine“ Magie intelligent und lebendig, und man kann daraus „ableiten”.
Mit alldem bewegen wir uns im sicheren Bereich der Spekulation und überlassen dies den Philosophen unter uns.
Fakt ist, dass die Leitmagie auf Palea völlig verschieden von der Essenz ist und auch gesondert behandelt wird.
Sie unterliegt nicht der Einteilung in die Weiße und Schwarze Magie.


Ingenium
========

Die Fähigkeit, Magie zu erkennen (und zu nutzen) wird Ingenium genannt.
Man hat das Ingenium, oder eben nicht.
Es gibt durchaus Gelehrte, wenn auch schwindend wenige, die der Meinung sind, dass alle intelligente Wesen Träger des Ingeniums sind, es nur vom Glauben an die Magie (und vielleicht noch ein paar Faktoren mehr) abhinge, ob es in Erscheinung trete.
Das lässt sich aber nicht belegen, und es kann nur festgehalten werden, dass es immer weniger mit dem Ingenium gibt.
Wer Träger des lngeniums ist, der spürt die Vibrationen, den warmen Fluss der Essenz um sich mal leichter, mal stärker.
Aber es braucht schon einiger Sensibilität, selbst die stärkste Essenz zu erkennen, wenn sie noch nicht manipuliert ist.
Einmal manipuliert, oder wie man auch sagt: geformt, kann sie über große Entfernungen zu spüren sein.
Einige Formungen können selbst die ohne Ingenium erkennen.

Die Träger des Ingeniums können nur selten etwas mit dem neuen Gefühl anfangen.
In stark emotionalen Situationen kann es aber zu einer unbewussten Manipulation der Essenz kommen.
In der Regel sind diese Ausbrüche auch das erste eindeutige Zeichen für das lngenium.
Vorher gibt es keine gesicherte Möglichkeit, dieses festzustellen.
Allerdings forschen Weiße Zauberer schon seit Generationen an Wegen, das Ingenium schon bei der Geburt erkennen zu können.
Aufgrund der Regulierung der Magie suchen sie permanent die Personen, die das lngenium besitzen.
Sie werden registriert und Teilen der Organisation zugewiesen, wo sie in Kasten, Rollen und Schulen geordnet werden.
Schulen entsenden sogenannte „Sucher” (es gibt noch viele weitere Bezeichnungen dafür nicht alle sind unbedingt positiv belegt), die meistens ins kleineren Gruppen mysteriösen Vorfällen nachspüren.
Jeder Geschichte versucht man nachzugehen, um möglichst schnell einen Träger zu identifizieren.
Es ist ein wahrer Wettlauf, denn nicht nur die Weißen Zauberer interessieren sich für diese Personen.
Andere Organisationen sind ebenfalls auf der Suche nach Magiebegabten.
Dazu kommt noch, dass diese Ausbrüche für Unwissende unerklärlich sind.
Und vor Unerklärlichem ängstigt man sich.
Diese Furcht (und manchmal auch Neid und andere Treiber) führt dazu, dass der Träger sich schnell verfolgt und gehasst sieht.
Das kann in Schmähungen, simplem Meiden und Schneiden, aber auch in Jagden und Tötungen des Trägers enden.
Einmal von der Gemeinschaft deshalb ausgestoßen, ist der Träger schnell auch für andere unerklärliche Phänomene verantwortlich die schlechte Ernte, das verendete Vieh usw.


Formungen
=========

Manipulierte Essenz wird auch geformte Essenz oder Formung genannt.
Das Manipulieren wird daher auch als „Formen“ bezeichnet oder eben einfach als Zaubern.
Dem Formen liegt eine einfache aber bedeutsame Gesetzmäßigkeit zu Grunde: Man muss Kraft aufwenden, um eine Formung zu erreichen.
Die meisten Zauberer verwenden ihren ganzen Körper und lassen ausdrucksstark ihre Arme machtvoll gestikulieren.
Als Ausdruck höchster Konzentration und Schulung zeichnen sie energische Runen in die Essenz.
In aller Regel schreien sie dabei die uralten Formeln, gerade so, als würden sie ein Ventil damit öffnen und den innerlich aufgebauten Druck plötzlich entlassen.
Weitaus schwieriger, aber nicht weniger kraftraubend, ist das Formen der Essenz ohne jegliche äußerlich sichtbare Anstrengungen.
Das bedarf sehr viel Übung und Erfahrung.

Die Formung der Essenz selbst passiert, wenn nicht weiteres hinzukommt, regelmäßig äußerst effektvoll.
Als Faustformel gilt: Je stärker die Formung ist, desto größer der Effekt.
Die Ladung oder Entladung der Essenz passiert oftmals durch Kanäle, was stark an Blitze erinnert.
Sanftere Zauber können auch durch andere Lichteffekte begleitet werden.
Wenn sich die Essenz konzentriert, dann kann es auch zu lautem Knallen oder anderen Geräuschen kommen.
Bezauberte Gegenstände, Personen oder Orte können leuchten, schimmern, summen, brummen, vibrieren und vieles mehr.
Manchmal bleibt auch die geformte Essenz als Rune oder Runenkreis, Formelschatten oder ähnliches vorhanden.
Allerdings können diese Effekte auch unterdrückt werden.
Diese schwere Kunst braucht ebenso viel Übung und Erfahrung wie das Zaubern ohne äußerliche Anstrengung.


Weiße Magie
===========

Die Weiße Magie ist die einzig erlaubte Form der Zauberei in den meisten Ländern auf Palea.
Sie unterliegt strengen Regeln, die von Region zu Region oder von Schule zu Schule unterschiedlich sein können.
Ihnen allen gemein ist aber, dass sie einen Kodex haben.
Die ersten drei Gesetze des Kodexes sind nahezu wort- und jedenfalls bedeutungsgleich in sämtlichen Varianten.
Die nachfolgenden Regeln können hingegen anders gestaltet sein.
Sie bestehen aus Paragraphen gefüllt mit Einzelheiten, wie der Verantwortung eines Lehrers für seinen Schüler oder einer Auflistung der verbotenen Disziplinen, sowie Regeln zur Aufnahme, zur Mitwirkung und zum Ausschluss aus der Schule oder Akademie.
Ebenso werden Gerichtsbarkeit, Hierarchien und Beiträge, falls sie erhoben werden, darin geregelt.

Die Weiße Magie ist über ganz Palea organisiert.
Die unterste Ebene sind die Zauberer selbst.
Sie gehören Schulen, Akademien oder Orden an.
Schulen usw. können über viele Länder verteilt sein und selbst eine vollständige Organisation darstellen.
Aber trotzdem stellt jede Organisationseinheit nur einen oder mehrere Sprecher zentral für die Magierzirkel ab, derer es sieben in Palea gibt.
Die sieben Zirkel selbst benennen einen Erzmagier, der sie im Rat der Magier vertritt.
Die sieben Erzmagierinnen und Erzmagier treffen alle sieben Jahre persönlich zusammen und halten über Tage gemeinsam Rat.

Der Titel des Erzmagiers sagt dabei nichts über seine Fähigkeiten in der Formung aus, wohl aber über seine Reputation gleich woher sie stammt.
Die Anzahl der Zirkelmagier, die eine Einheit stellt, hängt von der Größe, der Macht und dem Erbrecht ab.
Ein Orden kann über die Jahrhunderte sich das Recht für mehr Zirkelmagier erarbeitet haben.

Überhaupt ist das Recht innerhalb der Weißen Magier ein Recht aus Präzedenzfällen und uralten Weisheiten.
Regeln werden teilweise deshalb befolgt „weil es schon immer so war“.
Die Organisation ist sehr traditionsbewusst, und man hält nicht viel von schnellen Änderungen und Modernisierungen.

.. sidebar:: Gut und Böse

   Die Unterscheidung von Weißer und Schwarzer Magier hat nichts mit der Trennung von Gut und Böse gemein.
   Weiße Magie ist nicht per definitionem Gut und Schwarze nicht Böse.
   Hier ist vielmehr vieles vertretbar und bleibt der konkreten Ausgestaltung der Schule, der Zauberer usw. überlassen.


Der Kodex
=========

Die drei obersten Gebote des Kodex werden von allen Weißen Zauberern befolgt.
Sie sind der Reihe nach Achtung vor der Schöpfung, Verantwortung im Umgang mit der Essenz und Achtsamkeit im Leben.

Was die drei Gebote praktisch bedeuten ist Gegenstand der Auslegung.
Allerdings ist diese sehr strengen Regeln unterworfen.
Diese alleine stellen eine Wissenschaft ähnlich der Rechtswissenschaft dar.
Im Laufe der Zeit sind etliche weitere (geschriebene und ungeschriebene) Gebote daraus entstanden.
Nicht alles ist frei von Widersprüchen aber es ist Generationen von Gelehrten gelungen, selbst die widersprüchlichsten Regeln irgendwie in Einklang zu bringen.
Selbstverständlich standen bei der Genese der meisten Gesetze nicht (nur) der Ur-Kodex Pate, sondern vielfach auch einfachere Motive...

In aller Regel werden die obersten drei Gebote wir folgt ausgelegt:

Aus Achtung vor dem Geschaffenen sollen Nichteinmischung und Respekt als oberstes Prinzip gelten.
In der Vielzahl der Auslegungen gilt auch die Ablehnung von zerstörerischer bzw. besser formuliert von aggressiver Zauberei als strengstens verboten.
Den meisten Weißen Zauberern sind daher Angriffszauber untersagt.

Die stärkste Form der Freiheitsausübung für die Weiße Magie ist im Übrigen die freiwillige Bindung.
Die Bindung an einen Orden versteht sich nicht als Ausdruck der Unterwerfung oder Bindung im Sinne von Einengung, sondern als größte Freiheitsform.
Die in einigen Kulturkreisen bestehende Bindung von Partnern, wie zum Beispiel die Ehe, wird von den Weißen Zauberern geachtet (und geschützt).

Da alles in einem Kreislauf miteinander verflochten ist und Ursache und Wirkung ein Grundprinzip der Natur ist, bedarf jede Handlung einer Rechtfertigung und genauen Untersuchung über die Wechselwirkungen.
Die Macht der Magie geht folglich mit großer Verantwortung einher, wenn die Nichteinmischung gewahrt bleiben will bzw. auf ein Minimum reduziert sein soll.
Es ist einer Weißen Zauberin erlaubt, und sogar unter gewissen Umständen geboten, das Leben anderer zu schützen (und sich damit einzumischen), wenn dies zunächst rein defensiv passiert.
Erst wenn alle defensiven Mittel genutzt wurden, darf ein anderes gewählt werden hierbei gilt das Gebot des relativ mildesten Mittels.
Das heißt, es muss immer das mildeste erfolgsversprechende Mittel ausgewählt werden, wenn mehrere gleichgeeignet sind.

Die Achtsamkeit im Leben bedeutet, dass Beobachtung der Weg zur Erkenntnis ist und deshalb auch der Geist nicht in seiner Wahrnehmung getrübt werden darf.
Entsprechend beeinflussen Weiße Zauberer nicht ihre Wahrnehmung durch die Zuführung von Stoffen wie Alkohol und anderes.
(Zugegeben, es gibt natürlich Ausnahmen unter anderem mit der Begründung, dass man den Rausch nur so wahrhaft wahrnehmen könne).

Aus dem dritten Gebot und dem ersten folgert aber auch eine Ächtung von Beeinflussungszaubern.
Allerdings sind diese in der Regel auch das mildere Mittel gegenüber manchem Kampfzauber weshalb diese in der Verteidigung eine große Rolle spielen.

Die drei Gebote sind bei Widersprüchen untereinander in Ausgleich zu bringen.
Kein Gebot soll dabei vollkommen unbeachtet bleiben sie sollen alle optimal zur Geltung kommen.
Im Ergebnis bedeutet das für einen Weißen Zauberer im Spiel, dass er vom Spielleiter Vorgaben seines Ordens o.ä. erhalten kann, sowie dass er ständig die obersten drei Gebote des Kodex beachten muss.
Praktisch zusammengefasst sind das: Grundsätzlich keine Angriffszauber und Beeinflussungszauber (außer, es liegt ein Fall der Notwehr oder Nothilfe vor und es ist kein milderes Mittel sichtbar).

Verstöße gegen den Kodex werden schwer bestraft.
Siehe dazu „Keine Toleranz”.


Schulen und Orden der Weißen Magie
----------------------------------

Im Spielleiterheft finden sich exemplarische Schulen und Orden.
Sicherlich gibt es viele mehr, und man könnte ganze Bücher nur damit füllen.
Hier nun nur einige der größten Schulen und Orden.
Dabei gilt es zu bedenken, dass viele Namen von Zauberern nur einer von vielen möglichen ist.
Ein Zauberer kann bürgerlich vielleicht Egon Zimmermann heißen, aber mit Eintritt in die Schule den Namen Inthar (Seelenblicker) erhalten haben.
Als er seine Weihung als Zauberer und Absolvent der Schule erhielt, mag er nochmal den Namen geändert haben in Oanthar (Großer Seelenblicker).
Seinen Kommilitonen wird er vielleicht als Rabenschwinge, Hohlauge, Dünnebier (ein Spitzname, der sicherlich auf einen lustigen und verbotenen Streich gefolgt ist) oder Ini (Koseform von Inthar) bekannt sein.
Später wird er vielleicht noch aufsteigen und nochmals den Namen ändern vielleicht erhält er auch Zusätze, wie zum Beispiel Oanthar der Große oder Oanthar Nachtschwalbe.
Dazu kommen noch etliche Rufnamen und Namen, die er in den Geschichten der Barden führen wird.
Manchen Bauern wird er vielleicht als der Weiße oder ähnliches vorgestellt.
Weil ein Zauberer so viele Namen haben kann, ist manchmal ein und derselbe möglicherweise der Mann hinter verschiedenen Namen.
Das gilt nicht nur für Zauberer, sondern natürlich auch für andere Gruppen.

Auf Grund der Organisation der Weißen Magie ist es grundsätzlich einfacher, das Zaubern als Weißer zu lernen als sonst als Schwarzer oder sonst wer.
Die Schulen und Akademien haben die Lernmethoden immer mehr verfeinert und greifen auf unglaubliche Ressourcen zurück.
In einer Schule unterrichten häufig ein Dutzend Lehrer.
Es gibt Fachlehrer für Spezialgebiete, und die Forschung eröffnet dann und wann neuartige Formungen der Essenz.
Es gibt sogar Lehrbücher und zugängliche Bibliotheken.


Keine Toleranz
--------------

Wer sich nicht an die Regeln seiner Organisation hält, kann mit angemessenen bis hin zu drakonischen Strafen rechnen.
Gar keine Toleranz gibt es bei dem Verstoß gegen eines oder alle drei obersten Gebote des Kodex.
Es mag zu Recht eigenwillig anmuten, aber die Gebote gelten in dem Fall nicht untereinander.
Weiße Magier, die den Kodex brechen, und Schwarze Magier können nicht (mehr) auf die Geltung der Regeln vertrauen, sondern werden mit aller Macht verfolgt.
Natürlich gilt dies nicht, wenn ein Ordensbeitrag nicht sofort wie geschuldet erfüllt wurde aber wer häufiger unentschuldigt die obersten Prinzipien verrät, der kann als Ultima Ratio den Tod befürchten.
Wer einmal von einem Gericht (und manchmal reicht auch zunächst die Anordnung eines Zirkelmagiers) verurteilt wurde, wird zu Freiwild.
An dieser Hetzjagd beteiligen sich aber nicht alle Weißen, sondern auch hier gibt es Ausnahmen.
Auch wird die Jagd nicht immer bis zur letzten Konsequenz geführt, vielmehr kann es auch bei üblen Schmähungen oder folgenlosen Disputen bleiben.
Es ist durchaus möglich, dass Schwarze und Weiße an einem Lagerfeuer ihr Essen teilen und über ihre unterschiedlichen Ansätze philosophieren.
Andererseits kann es aber auch Einsatzkommandos geben, die nach dem Tod eines Anderen trachten.


Schwarze Magie
==============

Wenn ein Zauberer einmal der Weißen Magie angehört hat, heißt das nicht, dass er zeitlebens dort bleibt, sondern er kann sich wandeln.
Diese Änderung wird ihm sicherlich viele Feinde einbringen.
Er wird von seinen ehemaligen „Freunden” geächtet und teilweise sogar gejagt.
Interessanterweise gibt es Geschichten von Zauberern, die eben genau deswegen den Kreis der Weißen verlassen haben.
Manchmal auch nach einem langen Leidensweg, wenn sie versucht haben, das System zu ändern.
Allerdings heißt das nicht automatisch, dass aus ihnen Schwarze werden.
Schwarze Magie hat vielmehr auch drei Prinzipien, auf denen sie fußt.
Alles andere sind andere Formen der Magie, die daran im Anschluss beschrieben werden.

Vielleicht weil Schwarze Magier verfolgt werden, gibt es nur wenige von ihnen, und diese sind in aller Regel außerordentlich mächtig.

Sehr selten sind Organisationen Schwarzer Magier.
Aber wenn es sie gibt, dann vermögen sie einflussreich zu sein und bergen starke Zauberer.

Manche haben auch mächtige Förderer, die eigene Ziele damit verfolgen.


Drei Prinzipien
---------------

Vollkommen konträr zu dem Kodex der Weißen Magie stehen die Leitsprüche der Schwarzen Magie.
Auch wenn Weiße Magier pauschal alle Zauberer, die nicht der Weißen Magie angehören, Schwarze Magier nennen, so sind es nur diejenigen, die diesen drei Prinzipien folgen.

Das oberste Prinzip ist die Maßgabe zur Grenzenlosigkeit.
Es darf keine Regeln geben, die das Zaubern einschränken.
Absolute Freiheit sei daher nur durch das Fernbleiben von Regeln erreichbar.
Jede Begrenzung würde überdies die Weiterentwicklung hindern.
Denn Forschung ist das Überwinden bekannter Grenzen, der Aufbruch in das Unbekannte.

Dem obersten Prinzip folgen noch zwei weitere Leitsprüche, die aber nicht mehr annähernd diese Schlagkraft erreichen und deren Reihenfolge gleichgültig ist.
Das eine ist die Aussage, dass die Kenntnis des Gegenteils eines Begriffs zugleich auch die Kenntnis über den Begriff selbst ist.
Was so banal klingt, hat im Detail interessante Nuancen.
Die Erforschung des Lebens ist durch Vertiefung der Kenntnisse im Bereich des Todes möglich.
Und schließlich vielleicht als Antwort auf das Achtsamkeitsgebot der Weißen noch die Rechtfertigung für die Existenz der Lüge: Wer mit der Wahrheit nicht leben kann, dem muss die Lüge genügen.
Die Entscheidung, wer nicht mit was leben kann, obliegt wiederum dem Schwarzen Zauberer.

Die Verletzung der drei Prinzipien hat keine Konsequenzen zur Folge, außer der, dass man vielleicht eher nicht der Schwarzen Magie sondern einer anderen Richtung angehört.


Organisationen der Schwarzen Magie
----------------------------------

Auch die Schwarze Magie weist durchaus Organisationen auf, auch wenn diese weitaus weniger sind und diese auch zumeist im Untergrund oder versteckt und abgelegen angesiedelt sind.
Und es gibt eine große Anzahl von Zaubermeistern, die mit je einem Schüler arbeiten.
Einige von ihnen sind in Gilden, Orden und Gruppen verbunden, die durchaus auch Regeln für die Zusammenarbeit usw. haben.
Selbstverständlich stehen diese Regeln dann in Einklang mit dem Prinzip der grenzenlosen Formung der Magie.

Die Illegalität (zumindest in den meisten Ländern Paleas) erschwert den Schwarzen Zauberern das Leben.
Durch die Verfolgung durch (einige) Weiße Zauberer, die Öffentlichkeit (gleich ob zum Beispiel Bauern oder die Truppen des Königs) bedingt, muss man als Schwarzer eine Menge „leisten” können, um Erfolg zu haben.
Man kann durchaus der Mehrzahl der Schwarzen Zauberer eine gehörige Portion Macht zusprechen.
Im Gegensatz dazu gibt es bei den Weißen auch „einfachere” Zauberer.
Deshalb, und weil die Schwarzen auch durch keine Regeln in der Formung eingeschränkt sind, fühlen sich viele von ihnen als die Elite der Magiebegabten.

Die wenigen Schulen der Schwarzen, die es auf Palea gibt, geben sich deshalb in aller Regel elitär.
Aufnahmeriten können daher durchaus auch tödlich sein, wenn sie eben auch der Selektion dienen sollen.
Das schreckt wiederum auch Aspiranten ab, wenn sie überhaupt eine Schule finden.
Deshalb lernen wohl die meisten Schwarzen ihre Künste im Selbststudium oder bei einem Zaubermeister.
Dort gehen sie dann klassisch in die Lehre.

Natürlich gibt es auch den ein oder anderen Mächtigen, der sich gerne auch der Dienste einer Schwarzen Schule oder Ordens durch großzügige Geschenke versichert.

Grundsätzlich sind die Schwarzen Schulen trotzdem nicht annähernd so gut ausgestattet wie ihre Weißen Gegenstücke.


Wilde Magie
===========

Weiße Zauberer würden die Wilde Magie einfach auch als Schwarze bezeichnen.
Dabei gibt es aber einen gewichtigen Unterschied: Die Essenz wird als lebendig verstanden.
Die Formung passiert durch ein Miteinander.
Man versteht die Essenz und die Magiebegabten als in Symbiose befindlich.
Dieses Miteinander verbietet ein Beherrschen und erlaubt nur ein Zusammenwirken.
Beschränkungen in der Anwendung ergeben sich „nur“ durch die Moralvorstellungen des Anwenders (und natürlich der Essenz, sollte man in dem Fall hinzufügen).

Für Wilde ist es deshalb absolut notwendig, die Gefühle und Motive der Essenz zu erkennen.
Sie deuten daher Zeichen jeder Art, um dadurch die momentanen Launen der Essenz zu verstehen.
Beispiele hierfür können die Lage von Runensteinen nach einem Wurf, das Rauschen der Blätter eines Baumes und der Flug bestimmter Vögel sein.

Ihre Vertreter sind in aller Regel Einzelgänger.

Organisationen sind äußerst selten.
Insbesondere die nichtmenschiichen Völker bringen Wilde Magie vor.
Dort kann es auch Schulen oder andere Gruppierungen geben, die in Ausnahmefällen von den Weißen akzeptiert werden.


Elementare Magie
================

Die Elementare Magie stellt nicht die Gesamtheit aller Former der Elementarmagie dar, sondern nur einen Teil davon.
Es handelt sich bei den Elementaren um Essenzformer, die aufgrund ihrer Verbundenheit zu einem oder mehreren Elementen sich nicht den Regeln der Weißen unterwerfen wollen und die aber auch nicht dem obersten Prinzip der Schwarzen Magie folgen.
Des Weiteren sehen sie auch kein Miteinander mit den Elementen.
Sie sind entweder Beherrscher oder Diener der Elemente, abhängig vom eigenen Rang (und eventuell dem Stand des TeilEiementes, das gerade relevant ist).
In der Regel sind die Elementaren sehr gut organisiert (wenn auch üblicherweise in kleineren Einheiten).
Sie sind grundsätzlich Schwarze aus der Sicht der Weißen und in der Folge dann auch verboten.
Die Elementaren sind aber noch die am häufigsten akzeptierte Ausnahme dazu.
Gerade die nichtmenschlichen Elementaren werden zum Großteil nicht nur toleriert, sondern auch respektiert.

Die Schulen der Elementaren Magie sind selten, was in diesem Fall vor allem auf die geringe Anzahl der Elementaren zurückzuführen ist, und liegen manchmal sehr versteckt.
Sie gelten aber als mächtig und beeindruckend.


Freie Magie
===========

Als Freie Magie wird jede andere Art bezeichnet.
Es ist ein Sammelbegriff, der alle Minderheiten in Gänze erfasst.
Dabei ist Frei als Gegensatz zu den Regeln der Weißen Magie und als nicht der Schwarzen, Wilden oder Elementaren zugehörig zu verstehen.
Als nicht-Weiße wird die Freie Magie von den Weißen natürlich in der Hauptsache auch für Schwarz erklärt.


Zaubern
=======

Um zu zaubern muss ein Charakter grundsätzlich zwei Voraussetzungen erfüllen: Er muss die entsprechende Spruchliste im notwendigen Rang erlernt haben und über ausreichend Magiepunkte verfügen.

Dann sagt der Spieler den Zauber an, und sein Charakter formt die Magie.
Nach der Formung gleich ob der Zauber funktioniert hat oder nicht müssen so viele Magiepunkte abgezogen werden, wie der Zauber Ränge hat.

.. admonition:: Beispiel
   :class: admonition note

   Ein Zauberer formt einen Zauber aus der Spruchliste Schwarze Magie im Rang drei.
   Er muss demnach drei Magiepunkte aufwenden.

Einige Zauber bieten noch mehr Optionen, die durch den Einsatz von mehr Magiepunkten genutzt werden können.

Grundsätzlich ist zum Zaubern kein Manöver notwendig.
Etwas anderes gilt dann, wenn der Spielleiter eine herausfordernde Situation festlegt oder der Spruch gezielt werden muss.

Ein Zauberer braucht zum Formen der Magie in der Regel eine (Kampf-) Runde.


Zaubern in herausfordernden Situationen
---------------------------------------

Unter bestimmten Gegebenheiten kann es erforderlich sein, dass ein erfolgreiches Manöver zum Zaubern vorausgesetzt wird.
Dieses wird wie ein Manöver einer anderen Fertigkeit ausgeführt nur dass hier der Rang in der entsprechenden Spruchliste und der entsprechende Attributsbonus (Intelligenz, bei Priestern Charisma) addiert werden.

Zum Beispiel kann der Spielleiter ein Manöver verlangen, wenn der Zauberer mitten in einem Orkan einen komplizierten Zauber versucht.

Das Zaubern im Kampf hingegen ist grundsätzlich kein Grund.
Wird der Zauberer allerdings verletzt, dann ist ein einfaches Manöver notwendig.

Sollte der Zauberer eine Metall-Rüstung tragen, dann ist stets ein einfaches Manöver zum Zaubern vorausgesetzt.


Zaubern über der eigenen Stufe
------------------------------

Übersteigt der Zauber (bzw. der Rang des Zaubers) die Stufe des Zauberers, dann ist die Situation herausfordernd.
Ein Manöver ist dann notwendig.
Der Schwierigkeitsgrad erhöht sich dabei mit jedem Rang über der Stufe des Zauberers.

.. admonition:: Beispiel
   :class: admonition note

   Ein Zauberer will einen Zauber des Ranges 6 formen und ist selbst nur auf der 3. Stufe.
   Der Rang übersteigt seine Stufe um drei.
   Statt einem einfachen Manöver ist nun ein „äußerst schweres Manöver” (-6) zu schaffen.
   Wäre die Differenz aus Rang und Stufe sogar 4, dann wäre es „blanker Leichtsinn " (-8).


Optionale Regel: Kraftvolles Formen
-----------------------------------

Der Charakter kann sein Manöver durch den Einsatz weiterer Magiepunkte erleichtern.
Für je drei Magiepunkte kann er einen Spezialbonus von +1 erhalten.


Magiepunkte
-----------

Die Höhe der Magiepunkte wird durch den Rang in der Fertigkeit „Magie entwickeln (X)" und den Bonus in dem entsprechenden Attribut festgelegt (siehe die Erläuterungen zur Fertigkeit „Magie entwickeln (X)‘").

Jedes Zaubern kostet Magiepunkte.
Der Rang eines Spruches entspricht dabei den Grundkosten.
In den Grundkosten sind alle Effekte enthalten, die in der Beschreibung des Zaubers aufgelistet werden, es sei denn es ist ausdrücklich von ,oder' die Rede.
Im Falle des Zaubers Schutz (Weiße Magie 1) bedeutet dies, dass für die Grundkosten von 1 MP genau eine Person +1 Rüstung erhält.
Einige Zauber kosten mehr, wenn man die dort angeführten Optionen nutzt.


.. admonition:: Beispiel
   :class: admonition note

   Ein Spruch des Ranges 2 kostet zwei Magiepunkte (Grundkosten).
   Er führt aber noch eine Option an, dass man die Wirkung vergrößern kann, wenn man mehr Magiepunkte einsetzt.
   Dort steht dann „heilt |TP|/|MP| und eine Person/1|MP|”.
   Sie können dann für die zwei bereits verwendeten Magiepunkte einer Person einen Trefferpunkt wiedergeben.
   Wenn Sie sieben Magiepunkte einbringen, dann verstärkt sich der Zauber, und Sie können 2 Personen jeweils 5 Trefferpunkte heilen oder 6 Personen jeweils einen oder jede andere Kombination.

.. admonition:: Beispiel
   :class: admonition note

   Ein Zauberwirker möchte den Zauber Schutz auf sich und zwei seiner Kameraden wirken.
   Der Zauber soll ihnen einen Rüstungsbonus von +2 gewähren.
   In den Grundkosten von 1 MP ist bereits ein Bonus von +1 für eine Person enthalten.
   Jede weitere Person kostet 1 MP zusätzlich (in unserem Beispiel also +2 |MP|).
   Die Erhöhung des Rüstungsbonus von +1 auf +2 kostet ebenfalls 1 |MP| zusätzlich.
   Die Gesamtkosten des Zaubers belaufen sich also auf 4 MP (1+2+1).
   Der Zauber schützt dann insgesamt 3 Personen mit einem Rüstungsbonus von +2.


Regenerieren von Magiepunkten
-----------------------------

Magiepunkte regenerieren pro Stunde in Höhe des Attributsbonus (Intelligenz / bei Priestern Charisma) +1.


Gezielte Sprüche
----------------

Einige Sprüche müssen gezielt werden, damit sie ihr Ziel treffen.
Diese Zauber sind besonders gekennzeichnet.
Für diese werden ähnliche Regeln wie die Kampfregeln angewendet.
Der Rang in „Gezielte Sprüche” und der Attributsbonus (bei allen Intelligenz nur beim Priester wird Charisma angewandt) werden zum Würfelwurf hinzuaddiert.
Einige Zauberstäbe haben noch einen weiteren Bonus, der auch noch hinzugezählt werden darf.
Dieser Angriffswert muss den Verteidigungswert (Rüstung, Defensivbonus und möglicherweise Magieresistenz) des Angegriffenen übertreffen (genau wie beim Waffenangriff).
Der Schaden wird durch den Zauber festgelegt.

Der Würfelwurf kann auch hier „nach oben offen" sein.

Der gezielte Spruch wird wie jede Aktion in der Reihenfolge der Initiative abgewickelt.
Die Initiative wird auch hier aus dem Attributsbonus (Geschicklichkeit) und dem Bonus der Waffe (hier: Waffenlos oder Stab also +1) gebildet.

Die Regeln für Überraschung im Kampf usw. gelten entsprechend.


Zauberstäbe
-----------

Das vielleicht wichtigste Utensil eines Zauberers ist sein Zauberstab.
In einem Zauberstab kann eine Menge Magie stecken.
Allerdings kauft man einen Zauberstab nicht „mal eben”.
Sie sind äußerst selten und kostbar.
Und trotz ihrer Magie sind sie auch nicht unzerstörbar.
Einige brechen wie das Holz, aus dem sie gemacht sind andere sind mit Schutzzaubern versehen.

Mit einem Zauberstab können darin liegende Zauber benutzt werden, ohne dass der Zauberer Magiepunkte aufwenden muss (bis auf ein paar Ausnahmen).
Aber jeder Zauber aus einem Zauberstab muss über ein Manöver „Gezielte Sprüche” verwendet werden, dabei ist er um den Rang des Zaubers erschwert.

Einige Zauberstäbe geben einen Bonus auf dieses Manöver.

Ein Zauberer beginnt seine Karriere mit einem einfachen Zauberstab (siehe dazu die Erläuterungen zum Beruf des Zauberers).

Vertiefende informationen zu den Sprüchen finden sich im Spielleiterheft.


Weitere Hinweise zu den Spruchlisten
------------------------------------

In der Spalte „Reichweite“ finden sich entweder konkrete Angaben in Entfernungseinheiten Oder:

**Speziell** Hier obliegt es dem Spielleiter, einschränkende Auflagen zu formulieren.
Es könnte beispielsweise sein, dass man für einen „Fluch“ einen persönlichen Gegenstand des Opfers braucht, es aber dann gleichgültig ist, wo sich das Opfer befindet.

**Sichtweite** Der Magier muss eine freie Sichtverbindung zum Ort des Effekts haben und selbst auch in der Lage sein „zu sehen”.

**Berührung** Der Zauberer muss das Ziel selbst berühren.
Bei nicht-willigen Opfern, ist dies ein waffenloser Angriff, der zunächst gelingen muss.
Wenn ein Spruch in der Spalte „Gezielt“ mit einem Kreuz markiert ist, dann gilt dieser Spruch als „Gezielter Spruch“ und muss ähnlich einem Angriff eingesetzt werden (siehe oben).
