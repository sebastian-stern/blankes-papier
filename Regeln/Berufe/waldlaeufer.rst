Waldläufer
==========

Überall auf Aborea gibt es große Wälder manche von Ihnen sind größer als ein Königreich.
Die Waldläufer fühlen sich diesen Wäldern besonders verbunden.
Sie verbringen viel Zeit in diesen und kommen nur selten in die Städte.
Daher werden sie häufig für Sonderlinge gehalten.

Waldläufer verstehen sich auf Tiere und Pflanzen und sind herausragende Schützen.


Magie
-----

In einem ketzerischen Bericht eines Gelehrten heißt es, dass Aborea selbst lebe.
Die Welt höchstpersönlich solle einem Waldläufern in der Not helfen, in dem sie ihre Kinder, die Pflanzen und Kreaturen, zur Rettung sendet.
Denn Waldläufer lieben und verehren Aborea.

Andere werden dies mit der Nähe zu den Naturgottheiten begründen und wieder andere einfach mit Magie.

Der Waldläufer kann die Spruchliste „Wilde Magie“ erlernen.

Ein Waldläufer erhält so viele Magiepunkte, wie er Ränge in Magie entwickeln (Fertigkeit) hat, multipliziert mit dem Ergebnis aus 3 plus seinen Intelligenzbonus: (IN-Bonus + 3) x Rang.


Bogenschütze
------------

Waldläufer verstehen sich gut auf den Einsatz von Bögen und Armbrust als Waffe.
Diese Waffengruppen gelten für sie nicht als ungelernt, auch wenn sie darin keinen Rang erworben haben.


Naturverbundenheit
------------------

Der Waldläufer erhält durch seine Naturverbundenheit einen Situationsbonus von +1 bei Manövern auf die Fertigkeit Natur.
Sie können diese Fertigkeit zum Beispiel in der Natur auch auf Schleichen, Verstecken, Spuren lesen, Kräuter suchen usw. anwenden.


Tiere
-----

Waldläufer haben ein besonderes Verständnis für Tiere.
Und die Tiere scheinen dies zu spüren.
Die Wirkung eines Waldläufers auf Tiere kann ab der fünften Stufe so groß sein, dass einige sich zu ihnen hingezogen fühlen.
Ab und zu gibt es sogar einige Tiere, die einem Waldläufer zunächst folgen und dann diesen sogar regelrecht begleiten.
Die Stufe des Tieres ist mindestens vier Stufen unter der des Waldläufers, das heißt, ein Waldläufer der Stufe 5 kann höchstens ein Tier der Stufe 1 als tierischen Freund finden.
Diese Bindung wächst mit der Zeit, und es kann sogar eine emphatische Verbindung zwischen den beiden entstehen.
Mittels dieser kann rudimentär (Gefühle, keine Worte) kommuniziert werden.
Für diese Verbindung müssen sich beide in Hör-, Sicht- oder Tast-Reichweite befinden.
Das Tier steht dem Waldläufer dann in Gefahrensituationen bei.
Der Verlust eines befreundeten Tieres ist dramatisch für den Waldläufer, und er wird einige Wochen trauern.
Nach frühestens einer Woche ist er bereit, sich auf eine neue Bindung einzulassen.


=====   =========================================
Stufe   Freund des Waldläufers
=====   =========================================
    1   Eule, Chamäleon, Salamander, Viper, Ratte
    2   Adler, Falke, Hund, Otter, Waschbär
    3   Wolf, Katze, Boa
    4   Affe, Pferd
    5   Löwe, Tiger, kleiner Bär, Gorilla
=====   =========================================

Näheres zum Freund des Waldläufers findet sich im Spielleiterheft.
Der Spielleiter kann die Wahl des Freundes einschränken beispielsweise, weil das Tier selten oder gar nicht in dem Umfeld zu finden ist.


Trefferpunkte
-------------

Waldläufer sind überdurchschnittlich zäh.
Ein Waldläufer erhält 8 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.
