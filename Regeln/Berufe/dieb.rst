Dieb
====

Diebe stehlen anderen ihr Hab und Gut.
Dazu knacken sie Schlösser, betrügen, lügen, verstecken sich oder nutzen jede andere Art der List.
Sie sind aber nicht unbedingt schlechte Wesen es gibt durchaus auch Schurken, die Reiche bestehlen, um es den Armen zu geben.
Die meisten der Diebe sind allerdings eher darauf aus, sich selbst zu bereichern.

In einer Abenteuer-Gruppe sind Diebe trotzdem gerne gesehen, denn sind eine unentbehrliche Hilfe (und sie können manchmal auch teilen): Sie können Fallen finden und entschärfen, Schlösser öffnen und gefährliche Monster hinterrücks meucheln.
Sie sind hervorragende Spione und können im Kampf mit ihrem Geschick überzeugen.

Diebe sind häufig in Gilden organisiert.
Der Gilde steht ein Meister vor, und wenn ein Dieb irgendwann erfahren genug ist, kann er diesen ersetzen oder selbst eine Gilde gründen.


Fallen
------

Diebe sind auf das Finden und Entschärfen von Fallen trainiert.
Sie erhalten einen Bonus von +1 auf entsprechende Manöver.


Schlösser öffnen
----------------

Mittels der Fertigkeit List können Diebe Schlösser öffnen.
Sie erhalten hierbei einen Bonus von +1.


Geheimtüren entdecken
---------------------

Diebe sind geschult in dem Entdecken von Verstecktem.
Mit einem sechsten Sinn und der Fähigkeit „Wahrnehmung” können sie Geheimtüren entdecken.
Sie erhalten hierbei einen Bonus von +1,


Heimlichkeit
------------

Diebe sind Meister des Schleichens und der Tarnung.
Sie können sich im Schatten verstecken und in der Menge untertauchen.
Sie erhalten dafür einen +1 Bonus auf diese Manöver.

Wenn ein Dieb unbemerkt in die Kampfreichweite gelangt (der Gegner also überrascht ist), dann kann er seinen Schaden verdoppeln (vorausgesetzt er erzielt einen Treffer).
In der Regel gibt es nur einen heimlichen Angriff, da sich der Gegner meistens nach der ersten Angriff dessen bewusst ist (und ab dann nicht mehr als überrascht gilt).


Ausweichen
----------

Ein Dieb kann, wenn er keine Rüstung trägt (also auch keinen Schild) und nicht überrascht ist, seinen Rang in der Fertigkeit Athletik als Rüstungsbonus (max. 5) einsetzen.


Gift
----

Diebe kennen sich mit Giften aus und erhalten einen +1 Bonus auf entsprechende Manöver


Magische Gegenstände
--------------------

Diebe verstehen die Funktionsweise von magischen Gegenständen auf ihre völlig Eigene Weise.
Sie können diese daher sofort einsetzen


Magie
-----

Der Dieb kann die Spruchliste „Schwarze Magie” erlernen.

Ein Dieb erhält so viele Magiepunkte‚ wie er Ränge in Magie entwickeln (Fertigkeit) hat, multipliziert mit dem Ergebnis aus 3 plus seine” lntelligenzbonus: (IN-Bonus + 3) X Rang.


Trefferpunkte
-------------
Ein Dieb erhält 6 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.
