Priester
========

Priester sind Diener einer Gottheit.
Sie haben ihr ganzes Leben der Religion verschrieben.
Alles wird nach den Prinzipien ihrer Gottheit organisiert.
Sie sind zwar der Kirche der Gottheit verpflichtet, aber im Zweifel unterstehen sie nur ihrer Gottheit.
Sie errichten Tempel, missionieren Kulturräume, helfen Anderen oder treiben Kirchensteuern ein.

Wenn Priester ausziehen, dann kann dies viele Motive haben: weltliche und geistliche.
Und was immer vor ihnen liegt, sie können dabei auf die Kraft aus ihrem Glauben vertrauen.
Sie sind in der Lage, die sogenannte Leitmagie zu formen (von den/durch die Götter geleitete).
Sie können oftmals aber auch auf ihre Wehrhaftigkeit mit der weltlichen Waffe zurückgreifen.
Nicht wenige sind im Umgang mit ihrer Waffe geschult und viele tragen schwere Rüstung (die Priester nicht am Zauber hindert).

Priester sind in der Regel in Orden organisiert, die strengen Regeln folgen.
Es ist das Privileg großer Erfahrung und Weisheit, irgendwann dem Orden vorzustehen und die Geschicke des Bundes zu leiten.

Typisch für einen Priester ist auch die strenge Befolgung der Rituale seiner Religion: Morgengebete, Abendgebete, Feiertage, Fastenzeiten, Opfer, Meditationen oder anderes.


Magie
-----

Der Priester kann die Spruchlisten „Zeichen“ und „Wunder“ erlernen.

Ein Priester erhält so viele Magiepunkte, wie er Ränge in der Fertigkeit Magie entwickeln hat, multipliziert mit dem Ergebnis aus 3 plus Seinen Charismabonus: (CH-Bonus + 3) x Rang.


Heiliges Symbol
---------------

Der Priester beginnt das Spiel mit einem heiligen Symbol seines Gottes (in der Regel ein Amulett).
Solange er dieses trägt, erhält er einen Bonus von +1 auf seine Rüstung.
Es kennzeichnet ihn aber auch als Anhänger seines Gottes.
Das Ablegen des Symbols kann das Missfallen der Gottheit mit sich bnngen.


Schriftrollen
-------------

Priester können mittels Schriftrollen ungelernte Zauber verwenden.
Allerdings verlieren Schriftrollen schnell ihre Magie die Anwendungen sind begrenzt.
Zauber aus Schriftrollen kosten den Priester Magiepunkte.


Heilen
------

Priester sind ausgesprochen gute Heiler.
Sie erhalten einen Bonus von +1 auf entsprechende Manöver.


Göttliche Waffe
---------------

Der Priester ist im Umgang mit der Waffe der Gottheit vertraut, der er dient.
Auch wenn er keinen Rang in dieser Fertigkeit hat, gilt diese nicht als „ungeübt“.
Der Malus von -2 entfällt demnach.


Messe
-----
Ein Priester kann ab der 5. Stufe einmal am Tag eine besonders inspirierende Messe abhalten.
Alle Teilnehmer können vom Priester mit einem +4 Manöver beeinflusst werden.
Motiviert er diese, dann erhalten sie für vier Stunden einen Bonus von +1 auf alle Manöver.


Segnung
-------

Ab der 10. Stufe kann der Priester alle in Höhrweite einmal am Tag segnen.
Sie erhalten dann für einen Tag einen Bonus von +2 auf alle Mannöer und Angriffe, sowie auf den Angriff.
Eine Segnung dauert 10 Minuten.


Trefferpunkte
-------------

Priester sind im Allgemeinen eher kräftig und widerstandsfähig als schwächlich.
Ein Priester erhält 8 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.

