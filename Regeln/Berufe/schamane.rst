Schamane
========

Träger des Ingeniums finden sich in allen Kulturen, auch denen, die von den meisten anderen als „wild" oder „unzivilisiert“ bezeichnet werden.
Ihre Priester verehren ursprüngliche Götter, Tiere oder auch die Natur selbst.
Eine strikte Glaubenshierarchie lehnen sie bewusst ab.
Zwischen den höheren Mächten und den ihnen dienenden Kreaturen existiert nur der Schamane als Vermittler.
Auch bei Völkern und Kulturen, die die Magie im Allgemeinen oder Zauberer im Speziellen fürchten, gibt es in der Regel die eine oder andere Form des Schamanen.
Selbst in den sogenannten „zivilisierten” Reichen und Regionen sind Schamanen anzutreffen, auch wenn sie sich hier häufig als „Druiden“ bezeichnen und durchaus stärker differenzierte Hierarchien ausgebildet haben.


Fokus
-----

Schamanen verwenden keine Schriftrollen oder Zauberstäbe.
Stattdessen verwenden Sie einen Fokus (meist in Form eines Kristalls), der bis zu einen Rang pro Stufe an Zaubern speichern kann.
Die Zauber müssen dem Schamanen bekannt sein (d.h. er muss die Spruchliste auf entsprechendem Rang erlernt haben) und nach jedem Gebrauch erneut an den Fokus gebunden werden (2 MP pro Rang des Zaubers zzgl. weiterer Verstärkungen).


Blutmagie
---------

In größter Not schrecken Schamanen nicht davor zurück, ihre eigene Lebenskraft zu opfern, um sie in reine magische Energie umzuwandeln.
Hierzu muss der Schamane sich selbst eine blutende Wunde zufügen, während er einen Zauber wirkt.
Für jeden vergossenen TP an Blut erhält er einen zusätzlichen Magiepunkt, den er für den gewirkten Zauber verwenden kann.


Magie
-----

Schamanen verwenden eine spezielle, nach zivilisierten Maßstäben sehr krude Form der Leitmagie.
Ihre Zauber werden häufig durch Kristalle, Knochen, Federn und andere (tierische) Foki verstärkt, was das Bild des „Wilden“ zusätzlich unterstreicht.
Neben ihrer eigenen „Schamanenmagie“, einem Sammelsurium von Zaubern unterschiedlicher Magiebereiche, beherrschen viele Schamanen auch „Wilde Magie".

Ein Schamane erhält so viele Magiepunkte, wie er Ränge in der Fertigkeit Magie entwickeln hat, multipliziert mit dem Ergebnis aus 3 plus seinem Charisma-Bonus: (CH-Bonus +3) x Rang.


Trefferpunkte
-------------

Schamanen sind von Kindesbeinen an das harte Überleben in der Wildnis gewöhnt.
Ein Schamane erhält 6 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.

.. admonition:: Hexen & Druiden
    :class: admonition note

    Schamanen trifft man unter vielen Bezeichnungen an.
    Die Kräuterhexe kann beispielswiese ebenso eine Schamanin sein, wie eine Zauberin oder eine Priesterin.
    Namen entstehen manchmal auf sonderbare Art und Weise.

    Den meisten Schamanen ist aber gemein, dass sie eben nicht organisiert sind und daher in aller Regel als „Schwarze Magier" angesehen werden.
    Dies wohl zu Unrecht (wenn man überhaupt in dem Zusammenhang von Recht und Unrecht reden darf), da sie die Leitmagie, nicht die Essenz formen (sofern es den Unterschied gibt, siehe auch Seite 37).

    Druiden hingegen sind zwar oft in Zirkeln oder ähnlichem organisiert, aber sie werden in ihren Kulturkreisen häufig akzeptiert, weshalb sie nicht als „Schwarze Magier“ angesehen werden.
    Verlassen sie aber ihren Kulturkreis, können sie ebenso unter Verfolgung leiden, wie die Hexen.
