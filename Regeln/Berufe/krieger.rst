Krieger
=======

Krieger sind im Umgang mit tödlichen Waffen geschult und darauf spezialisiert, in Auseinandersetzungen einen gewaltsamen Sieg zu erreichen.
Sie sind Meister der Kampfkünste, und einige von ihnen führen Armeen.

Manche sitzen hoch zu Ross in schwerster Rüstung und tragen furchterregende Langschwerter.
Andere sind mutige Seefahrer und mit Rundschild und Axt bewaffnet.
Ihnen ist gemein, dass sie in Konflikten die größte Macht sind.
Kein König, der nicht wenigstens eine kleine Armee von Kriegern unterhält.
Manchmal reicht auch die bloße Anwesenheit von Kriegern aus, um einen Konflikt zu beenden.

Krieger lernen ihr Leben lang und werden immer meisterhafter im Umgang mit ihrer Waffe.
Sie führen irgendwann Trupps oder ganze Armeen, werden Könige und Eroberer.
Sie tragen legendäre Schwerter oder verzauberte Rüstungen.

Einige werden in Kasernen ausgebildet und andere in Kampfschulen.
Manche reisen jahrelang auf der Suche nach einem berühmten Kampflehrer, um bei ihm die geheime Kunst einer besonderen Art des Kampfes zu lernen.

Krieger sind geborene Anführer.
Schnell setzen sie sich in Gruppen an die Spitze.
Sie werden respektiert und gefürchtet.
Erfahrene Krieger scharen Untertanen um sich.
Alleine ihr Ruf reicht aus, dass sich viele ihnen anschließen wollen.
Gefolgsleute suchen den Schutz des Kriegers und sind im Gegenzug bereit, gemeinsam mit ihm zu kämpfen.
In der Geschichte gab es grausame Tyrannen, sowie heroische Heerführer.


Magie
-----

Der Krieger kann die Spruchlisten „Weiße Magie”, „Schwarze Magie" und „Wilde Magie” erlernen, aber es ist für ihn äußerst schwer.

Ein Krieger erhält so viele Magiepunkte, wie er Ränge in Magie entwickeln (Fertigkeit) hat, multipliziert mit dem Ergebnis aus 3 plus seinen Intelligenzbonus: (IN-Bonus + 3) x Rang.


Magische Waffen und Rüstungen
-----------------------------

Ein Krieger kann magische Waffen und Rüstungen benutzen, ohne sie zu verstehen.


Unzerstörbar
------------

Seine unbezwingbare Natur verleiht dem Krieger unglaublichen Willen und Stärke.
Er wird daher nicht bei 0 TP bewusstlos.


Waffenkunding
-------------

Ein Krieger erhält keinen Malus bei der Verwendung von Waffen, deren Waffengruppe er nicht gelernt hat.


Drohen & Führen
---------------

Krieger können durch ihr Auftreten Angst einflößen und motivieren.
Sie erhalten einen Bonus von +1 auf Einflussnahme, wenn sie entsprechend auftreten.
Sie sind die geborenen Führer im Schlachtfeld und können ihre Mitstreiter zu Höchstleistungen anstacheln.
Mit einem entsprechenden Manöver erhalten alle in Hör- und Sichtweite des Kriegers einen Bonus von +1 für die ersten zwei Kampfrunden.


Gefolgsleute
------------

Ein Krieger kann in höheren Stufen Gefolgsleute anwerben.
Der Spielleiter muss Gefolgsleute gestatten und kann Manöver dazu verlangen.

+-------+---------------------------------------+
|       | Gefolgsleute (Stufe)                  |
|       +---------+---------+---------+---------+
| Stufe | Stufe 1 | Stufe 2 | Stufe 3 | Stufe 4 |
+=======+=========+=========+=========+=========+
|     8 |       4 |         |         |         |
+-------+---------+---------+---------+---------+
|     9 |       8 |       2 |         |         |
+-------+---------+---------+---------+---------+
|    10 |      12 |       3 |         |         |
+-------+---------+---------+---------+---------+
|    11 |      24 |       6 |       3 |         |
+-------+---------+---------+---------+---------+
|    12 |      48 |      12 |       6 |       3 |
+-------+---------+---------+---------+---------+
|    13 | zusätzliche Gefolgsleute abhängig von |
|       | Titel, Land, Vermögen und Ruf         |
+-------+---------+---------+---------+---------+


Gefolgsleute kosten Unterhalt pro Tag:

=======  ====
Stufe 1  1 KL
Stufe 2  2 KL
Stufe 3  5 KL
Stufe 4  1 TT
=======  ====


Trefferpunkte
-------------

Krieger sind robust und unverwüstlich.
Sie zeichnen sich durch Stärke und Durchhaltevermögen aus.
Ein Krieger erhält 10 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.


Besonderes
----------

Elfen werden keine Krieger (der Spielleiter kann eine Ausnahme bei Schwarzelfen machen).
