Barde
=====

Barden sind anscheinend Spielleute, Unterhalter und Musikanten, die bei Hofe und vor geneigtem Publikum auftreten.
Sie durchwandern die Lande und reisen selbst in die gefährlichsten Regionen.
Dabei sind sie in aller Regel gern gesehene Gesellen.
Stets zaubern sie ein paar Stunden Vergnüglichkeit selbst in dunklen Zeiten und bringen Kunde aus fernen Landen.

Aber es ist weit mehr an diesen Barden, als die meisten auf den ersten Blick vermuten würden.
Einige von ihnen sind über Grenzen hinweg organisiert, in geheimen Bünden verschworen und auch als Spione im Auftrag eines höheren Wohls unterwegs.
Sie verfügen über ein ausgeklügeltes Codesystem und verstehen sich auf mystische Symbolik.
Viele Barden sind herausragende Kenner von Legenden und Volksweisen und natürlich große Künstler.

Die Bruderschaft der Wohlwissenden ist einer der legendären Bünde der Barden im Suderland.
Entgegen dem Namen gehören diesem auch einige Frauen an.
Mit Wegzeichen markieren die Mitglieder besondere Orte und Pfade.
Wenn sie einander begegnen, erkennen sie sich gegenseitig an einem geheimen Handzeichen.

Weniger bekannt, aber dafür umso erfolgreicher, soll eine Gruppe von Bardinnen sein, die unter dem Schutz des Großkönigs Trions höchstselbst stehen.
Ihnen könnte er seine teils hervorragenden Informationen über die Nachbarhäuser verdanken.


Informationen
-------------

Barden sind Naturtalente, wenn es darum geht, Anderen Informationen zu entlocken.
Sie erhalten auf Manöver, die der lnformationsbeschaffung dienen (z.B. Einflussnahme, List oder Wahrnehmung), einen Bonus von +1.


Magie
-----

Barden verstehen sich auf eine sonderbare Lautund Zeichenmagie.
Sie haben Zugriff auf die Listen „Bardenmagie” und „Schwarze Magie”.
Sie brauchen zum Formen der Magie aber stets einen Fokus, sei es ein Instrument, ihre Stimme oder eine ausdrucksstarke Gestik.

Der Wirkzusammenhang ist bisher vollkommen unbekannt.
Einige Gelehrte vermuten aber, dass die Götter Barden besonders lieben, da ihre Kunst ihnen schmeichelt.

Ein Barde erhält so viele Magiepunkte, wie er Ränge in der Fertigkeit Magie entwickeln hat, multipliziert mit dem Ergebnis aus 3 plus seinem Charisma-Bonus: (CH-Bonus +3) x Rang.


Zeichen
-------

Es gibt eine Geheimschrift (Sprache) und einen großen Fundus an Zeichen, die nur Barden benutzen können.
Allerdings soll es mittels Magie und Verrat schon zu Mitwissern gekommen sein.
Bardische Nachrichten zu lesen gilt als absurdes Manöver (MS 18).
Durch entsprechende Zauber oder Hilfsmittel kann das Manöver erleichtert werden,


Trefferpunkte
-------------

Barden durchqueren die Reiche bei jedem Wetter.
Sie sind wesentlich widerstandsfähiger als viele meinen.
Ein Barde erhält 6 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.


Wie lange dauert ein Lied?
--------------------------

Bardische Magie wirkt ebenso schnell (oder langsam) wie die anderer Magieformer.
Manche Lieder bestehen nur aus ein paar Worten.
(lm Zweifel einigen sich die Spieler von Barden zu Beginn des Spieles über die konkreten Zeitanforderungen für ein Lied mit dem Spielleiter.)
Auch die Initiative ist wie bei allen anderen Manövern.


Gemeinsames Spiel
-----------------

Die Wirkung bardischer Magie kann durch das gemeinsame Formen mehrerer Barden verstärkt werden.
Einer Legende nach gelang es einst einer Gruppe von Barden, gemeinsam eine Stadtmauer zum Einsturz zu bringen.

Barden können dazu einen Magiepunkte-Pool bilden: Vor der Formung legen die Beteiligten die Höhe der Magiepunkte fest.
Jeder muss für sich die Magie formen, aber hinsichtlich der Wirkungen werden diese addiert.
Dies gilt nicht, wenn ein Maximalwert angegeben ist.
