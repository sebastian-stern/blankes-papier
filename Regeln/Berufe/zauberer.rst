Zauberer
========

Zauberer sind mächtige Magieformer.
Sie verfügen über das Wissen und die Fähigkeit, unglaubliche Dinge zu vollbringen.
Meist wirken sie schwächlich und kränklich doch der Anblick kann täuschen.
Dank der Magie können sie weit über sich hinauswachsen.
Zauberer haben in der Regel einen Lehrmeister und gehören entweder der Weißen Magie an oder der Schwarzen.
Schwarze Zauberer sind in vielen Regionen gefürchtet und werden teilweise auch verfolgt.
Weiße Zauberer hingegen werden fast überall respektiert (siehe Magie weiter unten).


Magie wahrnehmen
----------------

Zauberer lernen schon früh Magie wahrzunehmen.
Dabei konzentrieren sie sich auf eine bestimmte Stelle für eine Runde und können dann dort vorhandene aktive Magie erkennen.
Verlieren sie die Konzentration oder den freien Blick darauf, dann endet auch die Wahrnehmung.
Auf diese Weise kann man allerdings keine passive Magie feststellen.
Und man kann auch keine versteckte Magie enttarnen.
Ob eine Kreatur überhaupt über die Fähigkeit Magie zu formen verfügt, kann ebenfalls nicht bestimmt werden.


Grundzauber
-----------

Weiße Zauberer verfügen nach ihrer Grundausbildung alle über die Möglichkeit, einen Lichtbogen mit ihrem Finger oder einem beliebigen Gegenstand zu formen.
Dieser erhellt für eine Runde pro Stufe des Zauberers den Ort wie eine helle Laterne.

Alle anderen hingegen können stattdessen einen Funkenregen mit den sonst gleichen Effekten formen.


Magie
-----

Auf Grund der Schulen kann ein Zauberer alle Spruchlisten erlernen bis auf die Leitmagie-Listen („Zeichen" und „Wunder").
Der Spielleiter kann aber den Zugang zu einzelnen Listen erschweren (mehr dazu im Spielleiterheft, Seite 22).

Ein Zauberer erhält so viele Magiepunkte, wie er Ränge in Magie entwickeln (Fertigkeit) hat, multipliziert mit dem Ergebnis aus 3 plus seinen lntelligenzbonus: (IN-Bonus + 3) x Rang.


Zauberbuch
----------

Der Einsatz eines Zauberbuches kann wenn man sich nicht in Zeitnot befindet ein Zauber-Manöver vereinfachen (+2 Bonus).
Der Zeitaufwand für den Spruch erhöht sich um 10 Minuten pro Rang.


Schriftrollen
-------------

Zauberer können mittels Schriftrollen ungelernte Zauber verwenden.
Allerdings verlieren Schriftrollen schnell ihre Magie, die Anwendungen sind begrenzt.
Zauber aus Schriftrollen kosten den Zauberer Magiepunkte.


Zauberstab
----------

Der Zauberer beginnt das Spiel mit einem einfachen Zauberstab.
Darin befindet sich ein Rang 1 Zauber einer beliebigen (nicht leitmagischen) Spruchliste.
Diesen kann er ohne Aufwand von Magiepunkte dreimal am Tag einsetzen.
Spruchoptionen (wie Verstärkungen) müssen allerdings mit Magiepunkte bezahlt werden.

Jeder Zauber durch einen Zauberstab gilt als gezielter Zauber.


Metallrüstungen
---------------

Zauberer sollten keine Metallrüstungen tragen, da die Formung der Essenz dadurch erschwert wird (siehe unter Zaubern).


Freund des Zauberers
--------------------

Ein Zauberer kann ab der 10. Stufe einen ungewöhnlichen Freund erhalten.
Es kann sich dabei um eine magische Kreatur handeln.
Die Stufe der Kreatur ist aber keinesfalls höher als die Stufe des Zauberers weniger fünf.
Der Spielleiter entscheidet über den Freund.
Der Freund steht dem Zauberer bei, wenn sie einander begegnen.
Er begleitet aber den Zauberer nicht dauerhaft (mehr dazu im Spielleiterheft).


Trefferpunkte
-------------

Zauberer widmen sich hauptsächlich geistigen Problemen und trainieren seltener ihre körperlichen Fähigkeiten.
Zauberer verlassen sich stark auf die Magie, die sie schützen und stärken kann.
Trotzdem gibt es auch Zauberer mit einer hohen Konstitution und daher auch hohen Trefferpunkten.

Ein Zauberer erhält 4 Trefferpunkte (plus Konstitutionsbonus) pro Stufe.


Besonderes
----------

Zwerge werden keine Zauberer.
