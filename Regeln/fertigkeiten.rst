************
Fertigkeiten
************

Fertigkeiten stellen Erfahrungsgebiete dar.
Wenn Sie in einem der Gebiete etwas „können”, dann besitzen Sie dort einen sogenannten Rang.
Die Höhe des Ranges symbolisiert, wie gut Sie darin sind.
Wenn Sie zum Beispiel in der Fertigkeit Reiten den Rang 2 haben, dann sind Sie ein ganz passabler Reiter.

Zunächst erfahren Sie nun, wie Sie Fertigkeiten erlernen und dann wie Sie diese einsetzen können.

Erlangen von Rängen
===================

Sie können jede Fertigkeit erlernen.
Und zwar am Anfang, wenn Sie Ihren Charakter erschaffen haben, danach bei jedem Stufenaufstieg.
Sie beginnen das Spiel in der ersten Stufe und damit nach einmal Lernen.

Für das Erlernen von Fertigkeiten brauchen Sie Ausbildungspunkte.
Diese erhalten Sie immer dann, wenn Sie eine Stufe aufsteigen.
Jeder erhält dann 8 Ausbildungspunkte (kurz: AP).

Wenn Sie Ihren Charakter erschaffen, dann dürfen Sie 8 Ausbildungspunkte für die erste Stufe benutzen.
Beachten Sie, dass die Menschen +2 AP für das erste Lernen auf Stufe 1 einmalig erhalten (nicht aber bei Stufenaufstiegen).

Sie dürfen grundsätzlich jede Fertigkeit nur einmal pro Stufenaufstieg lernen.
Es gibt dazu aber zwei Ausnahmen:

1. Es gibt drei Fertigkeiten, die Sie auf jeder Stufe zweimal lernen dürfen.
   Dabei handelt es sich um Spruchlisten, Magie entwickeln und Waffen.
   Das zweite Lernen ist aber teurer, siehe Kosten des Lernens unten.
2. Die Fertigkeiten mit einem (____)-Zusatz sind genauer zu spezifizieren.
   Sie gelten jeweils als einzelne Fertigkeit.

Sie dürfen allerdings jede Fertigkeit erneut lernen, wenn Sie eine neue Stufe erreicht haben.

.. admonition:: Beispiel
   :class: admonition note

   Wissen (Biologie) ist eine Fertigkeit, ebenso wie Wissen (Alchemie) eine Fertigkeit ist.
   Das bedeutet ferner, dass mit jedem Stufenaufstieg einmal Wissen (Biologie) und einmal Wissen (Alchemie) gelernt werden kann.
   Sie können aber nicht in einer Stufe zweimal Wissen (Alchemie) lernen.

Kosten des Lernens
==================
Jede Fertigkeit kostet unterschiedlich viele AP.
Die genauen Kosten hängen von Ihrem Beruf ab.

Wie im wirklichen Leben wird es später immer schwieriger, einen Lernfortschritt zu erreichen.
Deshalb müssen Sie für den Rang 5 schon „doppelt” lernen.
Ab Rang 10 verdoppelt sich der Aufwand noch einmal.

Wenn Sie das zweite Lernen bei einer der drei Fertigkeiten (Spruchlisten, Magie entwickeln und Waffen) nutzen, dann steigen auch da schon die Kosten.

Die Darstellung der Kosten erfolgt entweder in Form eines Wertes oder durch zwei Werte.
Wenn nur ein Wert angegeben ist, dann darf die Fertigkeit immer nur einmal pro Stufe gelernt werden.
Die Kosten sind gleich dem Wert.

Finden Sie zwei Werte (dargestellt nach dem Schema Wert1/Wert2), dann darf die Fertigkeit zweimal pro Stufe gelernt werden.
Das erste Mal Lernen kostet dann den ersten Wert.
Dieser ist auch immer niedriger.
Das zweite Lernen kostet den zweiten Wert, der stets höher ist.

.. admonition:: Beispiel
   :class: admonition note

   Nehmen wir als Beispiel einen menschlichen Zauberer.
   Zu Beginn des Spieles startet er in der ersten Stufe und erhält 8 Ausbildungspunkte (AP).
   Weil er ein Mensch ist, erhält er 2 AP auf der ersten Stufe extra.

   Damit stehen ihm 10 AP zur freien Verfügung.
   Die kann er alle verbrauchen.
   Er muss es aber nicht.
   Übrig gebliebene AP können auf dem Charakterb/att vermerkt werden.
   Sie stehen weiterhin zur Verfügung.

   Er entscheidet sich, 2 Ränge in Magie entwickeln zu lernen.
   Für den ersten Rang muss er einen AP aufwenden und für den zweiten 2 AP.
   Von seinen 10 AP bleiben ihm nun noch 7 AP (10 1 2 = 7).
   Die beiden Ränge kann er nun auf seinem Charakterblatt kenntlich machen, indem er die ersten beiden Kästchen ankreuzt.
   Die zwei Ränge in Magie entwickeln geben ihm nun 3 Magiepunkte (MP) pro Rang (2 x 3 = 6), und für seinen IntelligenzBonus (IN-Bonus) von +2 erhält er nochmals 4 Punkte (2 x 2 = 4).
   Insgesamt hat er nun 10MP, die er direkt auf seinem Charakterblatt vermerken kann.
   Er entscheidet sich nun, zwei Ränge in Spruchlisten zu entwickeln.
   Dies kostet ihn 4 AP (1 für den ersten Rang und 3 für den zweiten Rang).
   Er muss sich nun entscheiden, ob er zwei verschiedene Spruch/isten mit je einem Rang lernt oder eine Liste mit zwei Rängen.
   Er wagt es, neben der Weißen Magie (Rang 1) noch die Wilde Magie (Rang 1) zu nehmen.
   Es ist deswegen gewagt, weil er nun als „Schwarzer” gelten kann, siehe Seiten 36ff.
   Diese beiden Ränge notiert er nun ebenfalls auf dem Charakterblatt.
   Dafür füllt er eine der vorgegebenen Zeilen Spruchliste (____) aus und trägt in die Klammern die Spruchliste:

   Spruchliste (Weiße Magie)

   Das erste Kästchen daneben wird von ihm angekreuzt, da er einen Rang in der Liste hat.
   Dann schreibt er in eine leere Zeile:

   Spruchliste (Wilde Magie)

   und kreuzt dort ebenfalls das erste Kästchen an.

   Von seinen ehemals 10 AP hat er 3 für Magie entwickeln aufgewendet und 4 für die Spruchlisten.
   Damit verbleiben noch 3AP.
   Da er keinen gezielten Spruch beherrscht, entscheidet er sich Gezielte Sprüche erst später zu erlernen.
   Er wird stattdessen einen Rang Wissen wählen.
   Hier entschließt er sich, Wissen (Magie) auszusuchen.
   Dieser Rang hat ihn einen AP gekostet.
   Er hat nun noch 2 AP übrig.
   Diese will er noch behalten und später entscheiden, was er damit machen wird.
   Wer weiß, in welche Richtung sich sein Charakter entwickelt.

.. note::

   Es ist demnach möglich mehrere Spruch/isten gleichzeitig zu lernen.
   Die Kosten richten sich jeweils danach, ob man einen oder zwei Ränge lernt.
   Beispielsweise könnte der Zauberer mit 5 AP eine Spruch/iste mit zwei Rängen lernen und eine Spruch/iste mit einem Rang.
   Gleiches gilt für die Waß‘en (und alle anderen Fertigkeiten mit dem ( )Zusatz, die noch genauer spezifiziert werden müssen siehe auch „Erlangen von Rängen” und die Ausnahmen dazu).


Manöver
=======

Wenn Ihr Charakter in einer kritischen Spielsituation ist und handelt, dann nennen wir dies ein Manöver.

Angenommen, Sie würden durch einen Fluss schwimmen wollen, dann wird der Spielleiter von Ihnen einen Würfelwurf verlangen.
Dabei wird er die Manöverschwierigkeit vorher festlegen.
Dann wird er das Attribut bestimmen, das hauptsächlich den Erfolg beeinflusst.
Jetzt sind Sie an der Reihe und würfeln.
Auch hier gilt: Wenn Sie eine 10 gewürfelt haben, dann ist der Wurf nach oben offen.
Sie dürfen dann erneut würfeln und das Ergebnis hinzuaddieren.
Allerdings gilt auch, dass wenn der erste Wurf eine 1 ist, sie in jedem Fall einen Fehlschlag erlitten haben.
Zu Ihrem Würfelwurf addieren Sie nun Ihren Fertigkeitsrang und Ihren Attributsbonus.
Wenn Sie in der Summe die Manöverschwierigkeit schaßen, dann haben Sie das Manöver erfolgreich gemeistert.

Der Erfolg eines Manövers wird also durch den Würfelwurf, den Fertigkeitsrang, den Attributsbonus und die Manöverschwierigkeit festgestellt.
In einer Formel würde dies so aussehen:

Erfolg, wenn: Würfelwurf + Fertigkeitsrang + Attributsbonus Modifikationen durch die Rüstung >= Manöverschwierigkeit

Würfelwurf = 1, dann liegt ein Fehlschlag vor

Die Manöverschwierigkeit (MS) wird vom Spielleiter bestimmt.
Sie besteht aus der Schwierigkeit der Aktion und gegebenenfalls auch aus Bonussen oder Malussen, die aus der Situation resultieren.

So ist beispielsweise das Knacken eines Schlosses ohne Licht wesentlich schwerer.

Wird eine ungelernte Fertigkeit für ein Manöver eingesetzt, so gibt es dadurch keinen Bonus oder Malus anders als bei den Waffen, bei denen ein -2 Malus in aller Regel angewandt wird.

====================   ===
Grund-Manöverschwierigkeit
--------------------------
====================   ===
Routine                  5
sehr einfach             7
einfach                  8
schwer                  10
sehr schwer             12
äußerst schwer          14
blanker Leichtsinn      16
absurd                  18
====================   ===


Besonderer Erfolg
-----------------

Ein Erfolg bedeutet grundsätzlich, dass die Aktion gelungen ist.
Der Spielleiter kann allerdings besondere Erfolge (regelmäßig bei einem Übertreffen größer als 5) durch eine größere Wirkung oder einen kürzeren Zeitaufwand verdeutlichen.


Extremer Fehlschlag
-------------------

Wenn ein Manöver fehlschiägt, dann ist die gewünschte Wirkung nicht erreicht worden.
Ist der Fehlschlag besonders groß (regelmäßig bei einem Misslingen größer als 5 oder einer gewürfelten 1), dann können die Folgen kritisch werden.


Konkurrierende Manöver
----------------------

Bestimmte Aktionen können von dem Erfolg oder Misserfolg einer Aktion oder Reaktion eines anderen abhängen.
Beispielsweise schleicht ein Dieb hinter einer Wache her.
Für dieses Manöver ist die Fertigkeit List einschlägig.
Allerdings hängt die Schwierigkeit nicht nur von dem Untergrund ab (Kies wäre ein Beispiel für schwere Bedingungen) oder vom Sichtschutz (Tag/Nacht, Schatten vorhanden?, Büsche? usw.): Es kommt auch auf die Fertigkeiten der Wache an.
Dazu wird der Rang der Wache in der Fertigkeit Wahrnehmung herangezogen und auf die Manöverschwierigkeit addiert.
Das Manöver wird also schwerer!


Doch kein Erfolg (verdeckte Modifikationen)
-------------------------------------------

Manchmal kann es passieren, dass Sie meinen, ein Manöver erfolgreich geschafft zu haben.
Trotzdem wird sich später herausstellen, dass es ein Misserfolg war.
Dann hat der Spielleiter in diesem Fall verdeckte Modifikationen festgelegt.
Diese stehen für Erschwerungen oder Erleichterungen, die Sie nicht kennen (können).
Beispielsweise hat die Wache nur so getan, als ob sie schläft, und es ist daher wesentlich schwerer für Sie, diese zu überraschen.


Einander helfen
---------------

In bestimmten Situationen können SpielerCharaktere einander unterstützen.
Der Spielleiter wird dann einen möglichen Bonus bestimmen.
Regelmäßig wird dieser mindestens aus einem +1 bestehen.


Athletik
========

Alle nicht durch spezielle Fertigkeiten geregelte Aktionen können unter der Fertigkeit Athletik gefasst werden.
Beispiele sind Balancieren, Springen, Klettern und Ausweichen.
Um die Ausdauer zu bestimmen, kann der Rang mit dem Konstitutionswert multipliziert und so die Zeit in Minuten festgelegt werden.
Danach sind alle [Konstitutionswert mal 2 in Minuten] weitere, immer schwerer werdende Manöver zu bestehen.

.. admonition:: Beispiel
   :class: admonition note

   Ein Charakter hat vier Ränge in Athletik und einen Konstitutionswert von 6.
   Er kann daher auf ebener Fläche 24 Minuten locker laufen.
   Dafür ist ein Routine-Manöver fällig.
   Allerdings würde dafür schon ein Würfelwurf von 1 ausreichen (wenn dies nicht ein automatischer Feb/schlag wäre).

   Das Manöver kann daher vom Spielleiter als automatisch geglückt angesehen werden.
   Würfelwurf 1 + Fertigkeitsrang 4 + Attributsbonus 1 = 6.
   Nach Konstitutionswert 6 mal 2 = 12 Minuten muss er nun ein sehr einfaches Manöver schaffen (7).
   Dazu wird er mindestens eine 2 würfeln müssen.
   Sollte er nach weiteren 12 Minuten noch weiter laufen wollen, wird das nächste Manöver ein einfaches (8), und er wird über 3 Würfeln müssen oder seinen Lauf beenden (usw.).

.. admonition:: Beispiel
   :class: admonition note

   Diesmal möchte ein Charakter einen Berg erklettern.
   Der Spielleiter legt dafür eine Schwierigkeit von „sehr schwer” (12) fest.
   Wie oben besitzt unser Charakter einen KO-Wert von 6 und einen Athletik-Rang von 4.
   Für die ersten 24 Minuten muss er demnach mindestens eine 7 Würfeln: Würfelwurf 7 + Fertigkeitsrang 4 + Attributsbonus 1 = 12 geschafft!
   Misslingt das Manöver, dann kann dies zunächst nur bedeuten, dass man nicht „höher” gelangt.
   Erst ein extremer Fehlschlag kann einen Absturz bedeuten.

Nach einer Pause, die mindestens dem Fünffachen der Aktionszeit entspricht, ist man wieder bereit, ein misslungenes Manöver erneut anzusetzen.

.. admonition:: Beispiel
   :class: admonition note

   Angenommen, die Kletterpartie des Charakter oben hätte nach 24 Minuten durch einen Fehlschlag bei dem nächst schwereren Manöver (äußerst schwer: 14) geendet.
   Dann könnte der Charakter 24 x 5 Minuten pausieren (zum Beispiel auf einem Vorsprung, der dies erlauben würde).
   Danach könnte er sich erneut an dem Berg probieren, und es würde wieder mit einem sehr schweren Manöver beginnen.

Hat ein Charakter keinen Rang in Athletik‚ dann ist seine Ausdauer die Hälfte seines Konstitutionswertes.


Einflussnahme
=============

Mit der Fertigkeit Einflussnahme kann man beispielsweise Personen versuchen zu überreden, einen Handel für sich günstiger zu schließen (Feilschen) oder andere motivieren.
Unter Umständen handelt es sich dann um ein konkurrierendes Manöver (siehe unter „Konkurrierende Manöver”).


Gezielte Sprüche
================

Diese Fertigkeit wirkt auf jeden Angriff mit einem Zauber, wenn dieser vom Zauberer „gezielt“ werden muss.
Diese Angriffszauber sind besonders gekennzeichnet.
Sie werden wie ein Kampf abgewickelt.

Alle Zauber, die mittels eines Zauberstabs geformt werden, müssen gezielt werden.
Dies gilt auch dann, wenn der Zauber nicht gekennzeichnet ist.


Kunst
=====

Unter der Fertigkeit Kunst kann Literatur, Musik, Malerei und Schauspiel verstanden werden.
Für jeden Rang kann man ein Instrument oder Gebiet aussuchen.
Dies sollte in Absprache mit dem Spielleiter passieren.


List
====

Sich anzuschleichen, eine Falle zu stellen, einen Hinterhalt zu legen oder jemanden zu bestehlen jede dieser Aktionen ist eine List.
Darunter fallen noch viele weitere Aktionen, wie Lügen und Intrigieren.


Magie entwickeln (X)
====================

„Magie entwickeln“ steht für die Anzahl der Magiepunkte.
Je nach Beruf wird der lntelligenzbonus (alle bis auf Priester) oder der Charismabonus (Priester) hinzuaddiert.
Für jeden Rang gibt es 3 Magiepunkte und so viele Bonus-Magiepunkte, wie Charisma oder lntelligenzbonus vorhanden sind.
Es darf aber nur ein Attributsbonus hinzugerechnet werden.

Es ist mindestens ein Rang „Magie entwickeln“ notwendig, damit der Charakter überhaupt Magiepunkte haben kann.

.. admonition:: Beispiel
   :class: admonition note

   Ein Priester hat einen Charismabonus von +2 und vier Ränge in „Magie entwickeln”.
   Er hat demnach 4 Ränge x (3 Magiepunkte + 2 BonusMagiepunkte wegen seines hohen CharismabonUS) = 20 Magiepunkte.

Als Formel ausgedrückt [Rang x 3 + Rang x Attributsbonus] oder [Rang x (3 plus Attributsbonus)].
Diese Fertigkeit kann bis zu zweimal pro Stufe gelernt werden.


Natur
=====

Die Fertigkeit Natur hilft beim Umgang mit Tieren, dem Finden von Pfaden, dem Bestimmen von Flora und Fauna und vielem mehr.


Reiten
======

Die Fertigkeit Reiten ermöglicht Ihnen, auf Reittieren zu reiten.
Ihnen unbekannte Reittiere können das Manöver erschweren.
Spezielle Aktionen, wie der Kampf rücklings zu Pferde, erhöhen ebenfalls die Manöverschwierigkeit.


Schwimmen
=========

Die Fertigkeit Schwimmen erlaubt die Fortbewegung im Wasser, sowie das Tauchen über Strecken und in die Tiefe.
Unter günstigen Umständen kann ein Charakter bis zu [Fertigkeitsrang multipliziert mit dem Konstitutionswert in Minuten] lang schwimmen.
Danach sind immer schwerer werdende Manöver im Abstand von [Konstitutionswert in Minuten] abzulegen.

.. admonition:: Beispiel
   :class: admonition note

   Der Charakter besitzt 3 Ränge in Schwimmen und einen Konstitutionswert von 6.
   Er kann demnach 6 x 3 = 18 Minuten schwimmen.
   Möchte er länger als 18 Minuten schwimmen, dann muss er erfolgreich ein Manöver schaffen.
   Nach weiteren 6 Minuten wird er ein sch wereres Manöver absolvieren oder das rettende Land erreicht haben müssen.

Wie lange ein Charakter unter Wasser bleiben kann, ergibt sich aus der Rechnung [eine Minute plus der halbe Fertigkeitsrang].
Danach sind alle [Fertigkeitsrang in Sekunden] schwerer werdende Manöver notwendig.


Spruchlisten (X) (____)
=======================

Mit jedem Rang in dieser Fertigkeit kann eine neue Spruchliste gelernt werden oder eine bekannte Spruchliste in einem höheren Rang gelernt werden.
Bestimmte Spruchlisten sind besonderen Berufen vorbehalten.
Diese Fertigkeit kann pro Spruchliste bis zu zweimal pro Stufe gelernt werden.


Waffen (X) (____)
=================

Mit jedem Rang kann der Umgang mit einer Waffengruppe gelernt werden.
Ungelernt kann man kaum mit einer Waffe umgehen.
Daher erhalten ungelernte Nutzer einen Malus von -2.
Statt eine weitere Waffengruppe zu lernen, kann man auch eine bereits gelernte Waffengruppe verbessern.
Es gibt die Waffengruppen:

- Armbrust
- Axt
- Belagerungs- und Kriegswaffe
- Blasrohr
- Bogen
- Kurze Klingenwaffe
- Lange Klingenwaffe
- Schleuder
- Stangenwaffe
- Stichwaffe
- Waffenlos
- Wuchtwaffe
- Wurfwaffe

Welcher Attributsbonus angewendet wird, hängt von der jeweiligen Waffe ab und kann in der Waffenliste nachgesehen werden.
Manchmal sind mehrere Attribute angegeben, dann kann das mit dem höheren Attributsbonus ausgesucht werden.

Diese Fertigkeit kann pro Waffengruppe bis zu zweimal pro Stufe gelernt werden.

Krieger erhalten keinen Malus von -2, wenn sie eine bestimmte Waffengruppe nicht gelernt haben, da sie eine generelle Übung und Verständnis im Umgang mit Waffen haben.

Wenn Sie eine Waffengruppe gelernt haben, dann notieren Sie die entsprechende Gruppe hinter dem Eintrag „Waffenfertigkeit” in den Klammern auf dem Charakterblatt.
Für jede weitere Gruppe nehmen Sie einfach eine weitere Zeile.


Wahrnehmung
===========

Die Fertigkeit Wahrnehmung erlaubt das Entdecken einer Falle oder eines Hinterhaltes, das Enttarnen und Bemerken von Angreifern, das Entlarven einer Lüge und vieles mehr, das auf die Wahrnehmung in der Situation zurück geführt werden kann.


Wissen (____)
=============

Für jeden Rang darf ein Wissensgebiet ausgewählt werden oder ein bereits gelerntes vertieft werden:

- Alchemie
- Biologie
- Geologie
- Heilkunst
- Kultur
- Magie
- Religion
- Sprache (genauer spezifizieren)

Grundsätzlich sprechen alle die Handelssprache und ihre Landessprache (oder Volkssprache).
Jede weitere Sprache kann über einen Rang Wissen in der Sprache gelernt werden.
Um eine Sprache schreiben zu können, muss diese mit einem Rang von 1 beherrscht werden (auch die eigene Volks- oder Landessprache oder die Handelssprache).

Wenn Sie ein Wissensgebiet gelernt haben, dann notieren Sie das entsprechende Gebiet hinter dem Eintrag „Wissen“ in den Klammern auf dem Charakterblatt.
Für jedes weitere Gebiet nehmen Sie einfach eine weitere Zeile.


Beispiele zu einigen Situationen
================================

.. topic:: Geheimtür finden

   Um eine versteckte Tür zu finden, kann man die Fertigkeit Wahrnehmung einsetzen.
   Bedenken Sie, dass Zwerge und Gnome einen Bonus von +1 auf ihren Würfelwurf erhalten.
   Auch Diebe erhalten hier einen Bonus.

.. topic:: Heilen

   Mit der Fertigkeit Wissen (Heilkunst) können Verwundungen geheilt werden.
   Mehr dazu auf Seiten 34f.

.. topic:: Falle finden

   Fallen können wie Geheimtüren gefunden werden.
   Manche Fallen können umgangen werden, wenn sie einmal entdeckt wurden.
   Auch hier haben Gnome und Zwerge einen +1 Bonus auf ihren Würfe/wurf.
   Auch Diebe erhalten einen Bonus von +1 darauf.

.. topic:: Falle entschärfen

   Fallen die man nicht umgehen kann, muss man entschärfen, damit man sie nicht auslöst.
   Dafür kann man die Fertigkeit List einsetzen.
   Gnome sind herausragenden Mechaniker und erhalten darauf einen +1 Bonus.
   Diebe sind ebenfalls sehr gut darin und erhalten auch einen Bonus von +1.

.. topic:: Feilschen

   Mit der Fertigkeit Einflussnahme kann bei einem Handel der Preis beeinflusst werden.
   Mehr dazu im Spielleiterheft.

.. topic:: Schlösser öffnen

   Ein Schloss ohne den richtigen Schlüssel zu Öffnen, ist ein schwieriges Unterfangen.
   Nicht immer führt hier rohe Gewalt zum gewünschten Erfolg.
   Der Einsatz der Fertigkeit List kann hier weiterhelfen.
   Diebe erhalten darauf einen Bonus.


.. topic:: Magischen Gegenstand verstehen

   Damit magische Gegenstände verwendet werden können, muss man diese zunächst verstehen (siehe auch Spielleiterheft}.
   Hierfür wird die Fertigkeit Wissen (Magie) eingesetzt.
   Beachten Sie in dem Zusammenhang, dass einige Berufe die Verwendung von bestimmten magischen Gegenständen auch ohne das Verständnis erlauben.


Beispiel für Attribute, die den Erfolg beeinflussen
===================================================

Für jedes Manöver kommen ein oder mehrere Attribute in Frage, die den Erfolg beeinflussen können.
Haben Sie die Wahl zwischen mehreren, dann wählen Sie das höhere.
Der Attributsbonus darf siehe oben dann bei dem Manöver eingesetzt werden.

Um das passende Attribut herauszufinden, überlegen Sie einfach, welches überhaupt Einfluss haben könnte: Mit List kann Ihr Charakter ein Schloss öffnen.
Mit einem Dietrich zu hantieren, verlangt eine geschickte Hand.
Daher ist Geschicklichkeit durchaus ein Attribut, das den Erfolg beeinflusst.
Sie könnten aber auch Intelligenz heranziehen, denn die Mechanik eines Schlosses muss sicherlich auch verstanden sein, wenn es nicht ein bloßes Glückspiel sein soll.
Hier kann der Spielleiter zwischen beiden Attributen wählen, welches auch immer in der Situation angemessener ist im Zweifel das, das den höchsten Wert aufweist.
