******
Götter
******

Die Welt Aborea bietet eine Vielzahl von Göttern und Göttlichem.
Fast ausnahmslos alle Lebewesen auf Aborea glauben an einen Gott (Monotheismus), an eine Mehrheit von Göttern (Polytheismus), oder glauben an mehrere, verehren aber nur einen Gott (Henotheismus).
Das kann sicherlich auch darauf zurückgeführt werden, dass viele Ereignisse unerklärlich und derart tragisch sind, dass der Glaube an etwas Göttliches dem erst einen Sinn oder eine Erklärung gibt.
Aber sicherlich ist es auch die Präsenz des Göttlichen zu danken, zum Beispiel durch heilende Priester, dass Aborea in weiten Teilen religiös ist.
Außerordentlich selten sind daher Nichtgläubige (Atheisten).

Glaube und Religion können sich von einem Volk und Kulturkreis zum anderen wenig oder sehr stark unterscheiden.
Diese Differenzen können Anlass für Kriege oder philosophische Dispute sein.
Wie viele Aspekte Sie in Ihrem Spiel übernehmen, ist Ihnen überlassen.
Es ist kaum möglich, alle Formen und Arten zu kennen und im Spiel ständig ins Geschehen mit einzuweben, obwohl Religion sicherlich alltäglich überall anzutreffen ist: In verlassenen Ruinen, im Zusammenhang mit Artefakten oder auf dem Marktplatz, wo gegen überteuertes Geld Talismane angepriesen werden.

Letztlich liegt die Entscheidung bei Ihnen, wie viel Göttliches Sie in ihrem Spiel verwenden.


Das Pantheon
===========-

Die Götter leben mehr oder weniger nebeneinander.
In vielen Götterwelten streiten einige oder alle von ihnen untereinander.
In anderen bilden sie eine Symbiose, und wieder andere Götterwelten haben eine strenge Hierarchie.
Das Pantheon einer Kultur kann einige hundert Gottheiten beinhalten.
Manche Kulturen haben fast jeder Kreatur und jedem Moment eine Gottheit zugeordnet.

Jedes Pantheon ist in sich, in der jeweiligen Theologie, plausibel und vollständig erklärbar und wenn es nur mit der Unergründbarkeit des Göttlichen behauptet wird.
Das Pantheon entwickelt sich auch ständig weiter.
Durch Änderungen in der Kultur können Gewichtungen verschoben werden, so dass zum Beispiel die Gottheit des Fischfangs nun im Laufe der Zeit an Bedeutung verliert und die des Forschens und Wissen an Macht hinzugewinnt.
Manchmal spalten sich auch Aspekte auf, und aus dem Gott des Glücks entwickelt sich der Gott des Reiseglücks und der Gott des fleißigen Glücks.
Nicht selten werden auch Götter mit der Verschiebung von Grenzen, der Einnahme ganzer Kulturen oder dem regen Austausch mit einem anderen Land regelrecht importiert.
Und schließlich gibt es noch Religionen, die durch das Missionieren und Bekehren sich ausweiten und so Einzug in ein fremdes Pantheon halten.


Religion und Kirche
===================

Es ist sehr wichtig, zwischen Religion und Kirche zu unterscheiden.
Oftmals ist es so, dass zu einem Gott verschiedene Kirchen existieren.
Manchmal trägt ein und derselbe Gott verschiedene Namen, je nachdem, welche Kirche maßgeblich ist.
Die organisierte Religion kann mit dem Oberbegriff Kirche erfasst werden.
Davon zu unterscheiden ist sicherlich die Kirche im engeren Sinne, als Gegensatz zur Sekte, zum Orden oder Ähnlichem.
In diesem Zusammenhang ist Kirche im weiteren Sinne gemeint, also als Gegensatz zum nicht organisierten Glauben.

Bei den Kirchen gibt es ebenso viele Unterschiede wie auf allen anderen Ebenen.
Es gibt Kirchen, die andere Kirchen tolerieren, welche, die für sich alleine die Wahrheit beanspruchen und alle anderen verdammen, und die, die einem Pantheon verschrieben sind, einem Teil davon oder nur einer Gottheit.
Jede Kirche besitzt eine Organisationsform.
Wesentlich ist den Meisten dabei die Weitergabe der Lehren.
Oft gibt es Rangfolgen, Schulen, Bibliotheken, Räume zur Andacht und gemeinsame Riten und Feste.
Durch ihre Organisation sind die Kirchen häufig ein großer Machtfaktor.
Weltliche Mächte arrangieren sich mit ihnen, hassen oder lieben sie.
Es gibt sogar Länder, in denen die Kirche offiziell oder inoffiziell die einzige Macht im Land ist.
Da die Kirchen im Zusammenhang mit einer Gottheit stehen, finden sich exemplarische Beschreibungen einiger ausgesuchter Kirchen bei den jeweiligen Beschreibungen der Götter.


Anbetung und Verherung der Götter
=================================

Der Großteil der Bewohner Aboreas glaubt an einen oder mehrere Götter.
Es ist für einige Kulturen selbstverständlich, ein ganzes Pantheon zu verehren.
Es wird nicht nur positiven Gottheiten geopfert, sondern auch denen, die Unglück bringen.
Dort bittet man um Verschonung oder um die Heimsuchung eines Konkurrenten.

Die Verfluchung eines Feindes erfolgt häufig durch ein Blutopfer und die Hingabe eines persönlichen Gegenstandes des Opfers, wie zum Beispiel Haare des Feindes oder einem Gegenstand aus seinem Hausstand.


Göttliches Eingreifen
=====================

Immer wieder entstehen Situationen, in denen die Götter um Beistand angefleht werden.
Das sind nicht nur Gefahrenmomente, wie ein Kampf, ein schweres Unglück oder Ähnliches, sondern manchmal die Zeit, in der auch ganz alltägliche Dinge wie das Glück beim Spiel, Erreichen von Liebe, Erfolg bei einer Unternehmung, eine gute Ernte oder das Fernbleiben von Unglück, Krankheit und Flüchen eine Rolle spielen.
Ob ein Gott eingreift oder sogar die Götter, ist vollkommen willkürlic.
Die Wege und Entscheidungen der Götter folgen keinem Muster.
Sie sind unergründlich, was auf die für Sterbliche enorme Komplexität der Entscheidung zurück geführt wird.
Trotzdem haben sich in der Lehre und Forschung Faktoren herausgebildet, die eine Bitte erfolgreicher sein lassen können.

Gläubige können im Laufe der Zeit in der Gunst der Götter steigen.
Der Spielleiter kann dazu göttliche Punkte verteilen.
Diese Punkte stehen stellvertretend für die Höhe des Standes in der Gunst des Gottes.
Mit steigender Anzahl der Punkte ist ein Eingreifen umso wahrscheinlicher.
Allerdings sollen schon große Günstlinge der Götter in einer Gefahrensituation trotz energischem Bitten keinen Beistand erlangt haben.
Die Entscheidung, der Bitte nicht nachzukommen, diente sicherlich einer größeren Sache, und der Günstling tut gut daran, die Entscheidung des Gottes nicht anzuzweifeln, sondern dafür dankbar zu sein.
Auf der anderen Seite gibt es Berichte über Verzweifelte, die erst in einem dramatischen Moment religiös wurden und auch prompt Hilfe der Götter erhielten.
Es soll sogar der kritische Dialog mit einem Gott im Gebet nicht immer falsch sein.

Weitere Faktoren können Opfer und Spenden sein; nicht selten berichten Gläubige, die göttlichen Beistand erfahren haben, von einer Vision, in der sie mit einer göttlichen Mission betraut wurden.
Diese göttlichen Aufträge können unterschiedlich ausfallen: Die Errichtung eines Tempels, die Restaurierung eines Schreins, die Eroberung einer Reliquie, die Bekehrung Ungläubiger oder Ähnliches.
Die Aufgaben stehen in der Regel im Verhältnis zur erlangten Hilfe.
Für einfache oder weitverbreitete Wünsche bieten einige Organisationen und Kirchen geregelte Abläufe an: Wer zum Beispiel Fruchtbarkeit erlangen will, kann für eine Spende von wenigen Münzen ein einfaches Holzamulett in einem Tempel eines Fruchtbarkeitsgottes erhalten.
Eine reiche Ernte kann durch die Opferung eines Singvogels über dem heiligen Feuer oder durch die Gabe eines Teils der Ernte an die Kirche erbeten werden.
Es gibt sogar fahrende Priester, die ihren Segen gegen eine kleine Spende anbieten; einige Kirchen erteilen diesen Priestern gegen einen Obolus ein Beglaubigungsschreiben und verfolgen die, die dieses nicht haben.


Wenn die Götter eingreifen, dann ist dies stets auf ihre Art und Weise, und die Intervention ist zumeist eine Mischung aus Natürlichem und Übernatürlichem.
Skeptiker können die plötzliche Hilfe oder die Wendung des Glücks mit natürlichen Ereignissen und mit Hilfe der Wissenschaft erklären.
Gläubige werden immer darin die Einmischung des Göttlichen erkennen und hegen an der Intervention keine Zweifel.
Beispiele können das plötzliche Aufreißen der Wolkendecke sein und das Blenden der Angreifer durch die nun sichtbare Sonne oder der umstürzende Baum, der den Verfolgern den Weg versperrt oder (klassisch) der krachende Blitzeinschlag.

In sehr seltenen Fällen beseelt ein Gott eine Kreatur oder bemächtigt sich ihrer.
Das müssen nicht immer der Krieger oder der Heiler sein, auch der hilfreiche Bär und der wegweisende Falke sind mögliche Boten oder Formen der Götter.
Aber auch bei alltäglichen Tätigkeiten fühlt sich mancher kurzzeitig zumindest erleuchtet.
Ein Schwertkämpfer, der gerade einen mächtigen Streich parieren konnte oder einen übermächtigen Gegner bezwingen konnte, wird sicherlich danach berichten, dass ein Gott der Stärke oder des Krieges bei ihm war und sein Schwert geführt hat.
Üblich sind auch Aussprüche wie dass ein Gott der Kunst dem Künstler erschienen sein muss, denn wie hätte er sonst ein so überragendes Werk schaffen können.
Oftmals wirkt sich diese Begründung auch immens wertsteigernd für das Kunstwerk aus schließlich kauft man ein Stück Göttlichkeit damit ein.


Die Schöpfung Aboreas
=====================

Die Geschichte der Entstehung Aboreas und allen Seins ist unterschiedlich nach Volk, Kulturkreis und Glauben.
Manchmal sind es nur marginale Unterschiede, die aber mit dem Schwert verteidigt oder bekehrt werden.
In aller Regel aber weichen zumindest die Schöpfungsgeschichten der Völker stark voneinander ab.
Am ehesten differieren die Erklärungen der langlebigen Völker zur Schöpfung.
Die kurzlebigen, aber stark interagierenden Völker neigen eher zu einer Adaption der Geschichten der langlebigen Völker.
Doch gerade dort findet sich auch die meiste Zersplitterung: Fast jede Sekte erklärt die Genese auf ihre Art und Weise, und manchmal streiten sogar innerhalb einer Kirche verschiedene Auffassungen über die Entstehung des Lebens.
Viele haben es auch kunstvoll verstanden, die unterschiedlichen Erklärungen zu vermengen und nebeneinander existieren zu lassen.


Schöpfungsgeschichte der Elfen
------------------------------

Die Elfen aller Himmelsrichtungen glauben im Wesentlichen an die gleiche Schöpfungsgeschichte.
Die zentrale Rolle hat dabei die Gottheit Othil, die Sonne, die Aborea erschaffen hat.
Bevor sie Aborea erschuf, war sie überall.
Ihr Licht war unendlich.
Doch mit der Schöpfung Aboreas entstand auch die Finsternis.
Jeder Körper in der Unendlichkeit wirft einen Schatten, denn immer nur eine Seite kann Othil zugewandt sein.
Doch Othils Wärme und Kraft wirkte auch während der Nächte fort.
Damit die Kreaturen auch in der Nacht sich an Othil erinnern sollten, schuf sie |Anaira|, den Mond, der fortan die Finsternis beleuchtete, wenn auch wesentlich schwächer als Othil selbst.
Aber in der Zwischenzeit hatte die Finsternis schon ein Eigenleben entwickelt, und |Nairoth| herrschte über sie.
Er wehrte sich gegen das grelle Mal Othils und seitdem ist |Anaira| einem Wechsel unterworfen.
Der Wechsel ist klar am Firmament als zu- und abnehmender Mond erkennbar.
Die Kreaturen des Lichts aber suchten die Nähe Othils, und bald schon nahm Othil einen von ihnen zu sich: Er war der erste Stern, und Othil ließ ihn auch zu der Zeit |Nairoths| leuchten.

Während des bis heute noch andauernden Kampfes zwischen Licht und Dunkelheit wurden die Sterne immer mehr.
Es entstand das Sternenmeer.
|Nairoth| gab aber nicht auf und eroberte die Unterseite Aboreas, denn dorthin reichte das Licht Othils nicht.
Und er eroberte den Schatten, sodass er auch am Tag versuchen konnte, die Kreaturen des Lichts zu vergiften, denn er zürnte den Sternen, doch die waren zu weit entfernt, und er reichte nicht bis dorthin.
Seine langen Arme reichten zwar bis in die Räume zwischen ihnen, doch sie selbst waren unbesiegbar.

Dann sann er darauf, den Kreaturen des Lichts möglichst viel zu schaden, damit sie nicht länger zu Sternen würden.
Er überredete die Elemente, ihm zu dienen.
Doch die waren wankelmütig und dienten keinem gerne und lang.
Eines Tages aber gelang es ihm für wenige Tage, sie alle für seine Sache zu nutzen.
Er spielte sie gegenseitig aus und spann ein Netz aus lntrigen und Lügen.
Feuer verbrannte die Erde, Stürme schäumten das Meer auf, und Wellen griffen nach dem Land, Feuer brach durch die Erde und griff sogar nach dem Wasser.
Das Gesicht Aboreas wurde an diesen Tagen des Chaos für immer verändert.
Überrascht von den Gewalten der Elemente konnte Othil nur wenige Wesen vor der Vernichtung retten.
Zu ihnen gehören die Stammesväter.
Doch der Schrecken und das Leid und die Trauer saßen tief in ihren Seelen, und so ließ Othil sie Ruhe in der Musik finden.
Die Überlebenden konnten so das Vergangene vergessen und sich der Zukunft zuwenden.
Die Erinnerung aller beginnt mit der ersten Musik.
|Nairoth| zürnte, da es ihm nicht gelungen war, alle zu vernichten, und er entsandte seine Schöpfungen, auf dass sie solange die Kinder des Lichts bekämpfen sollten, bis alle vom Angesicht Aboreas verschwunden sind.
Unterdessen aber fanden die ersten Elfen (|Iareth|) zu |Anaira| und stimmten dort im Einklang mit dem Sternenmeer die Eine Melodie an.
Die Musik schwächt |Nairoth|.
Noch ist die Musik nicht laut genug, um |Nairoth| zu vertreiben.
Immer wenn ein |Iareth| bereit ist und die Eine Melodie stärken kann, dann wird er gerufen und kann zu |Anaira| aufsteigen.
Irgendwann wird die Beständigkeit Othils über den aggressiven |Nairoth| siegen.
Und auf Aborea selbst wütet der ewige Kampf zwischen denen der Finsternis und denen des Lichts.


|Anaira|
^^^^^^^^

- Aspekte: Frieden, Tod
- Waffe: Bogen

|Anaira| (Ill A-nai-rha) ist ein ständiger Bezugspunkt der |Iareth| und in jedem Kulturkreis auf die eine oder andere Art vorhanden.
Die zentralen Aspekte der Gottheit prägen Kunst, Lebensweise, Sitten und Gebräuche.
Während im Norden das größte Fest der |Iareth| das jährliche „Orilum Anæra” ist, begehen einige Stämme im Süden sogar traditionelle Feierlichkeiten zu jedem Vollmond.

Die Gottheit selbst findet keine Darstellung als Person, sondern manifestiert sich nur im vollen Mond Aboreas.
Die |Iareth| sind davon überzeugt, dass am Ende ihres Lebens auf Aborea ein Nachleben dort stattfindet.
In absolutem Frieden und Perfektion soll nur noch eine Melodie dort klingen.
Sie beginnt mit dem zunehmenden Mond und endet mit Abschluss einer Mondphase in Erinnerung an die Endlichkeit alles Seins.
Außer der Einen Melodie ist Nichts, keine störenden Elemente denn Aborea ist fern.

Die Eine Melodie wird irgendwann die Finsternis und |Nairoth| besiegen.
Dann wird Othils Licht alles erhellen.


|Nairoth|
^^^^^^^^^

- Aspekte: Unlicht, Finsternis, Dunkelheit, Nacht, Leere
- Waffe: Schwert

|Nairoth| (Nai-rosz) ist der Herr der Kreaturen der Finsternis, der dunkle Meister des Unlichts.
Er ist der Schöpfer der Wesen der Tiefe aber auch der, welcher die Seelen vergiftet und Kreaturen des Lichts manchmal auf seine Seite ziehen vermag.
Wenn er unter den Sterblichen wandelt, dann nimmt er ihnen das Licht und verbreitet die Kühle der Nacht.
Es gibt unzählige Berichte über sein Erscheinen, wann immer irgendwo ein Unglück passiert, meint irgendwer |Nairoth| gesehen zu haben.
Er tritt als Schatten in Erscheinung und selten auch in der Hülle eines Besessenen.

Manchmal gelingt es |Nairoth|, einen der Herrscher über ein Element davon zu überzeugen, kurzweilig auf seiner Seite zu stehen.
Dann lässt er ihn mit großen Katastrophen das Licht schwächen.
Ein Zeichen, dass |Nairoth| gerade irgendwo siegreich ist, wird in sternen- und mondlichtlosen Nächten gesehen.
Sie gelten als Fluch und Zeit großer Betrübnis.
Still wird dann der gerade Leidenden gedacht.


Othil
^^^^^

- Aspekte: Leben, Geburt
- Waffe: Schwert

Ohne Othil (Osz-il) gäbe es kein Licht und kein Leben auf Aborea.
Sie ist der Inbegriff von Energie und Belebung.
Ohne Othil wäre |Anaira| an die Dunkelheit gefallen, und ohne sie könnten die Sterne nicht leuchten.
Sie ist der Schöpfer allen Seins auf Aborea und überall sonst.
Einzig die Kreaturen der Nacht und des Unlebens existieren ohne sie.
Sie selbst wandelt nie unter den Sterblichen.
Aber sie beseelt manchmal Wesen des Lichts.
So sollen Frauen vor der Niederkunft von ihr berührt werden: sie gebären lebende Kinder.
Schwangere verehren daher besonders intensiv Othil vor der nahenden Geburt.
Warum Othil einige nicht zuvor berührt, ist unklar.
Unter den Nichtberührten sind viele ohne Fehl und Tadel.

Othil ist gütig und von warmem Gemüt, geduldig und beständig.
Ihrem ärgsten Feind |Nairoth| steht sie mit eben diesen Tugenden entgegen.
Sie wird in den meisten Kulturkreisen als lichtgebadete Frau mit offenen Armen dargestellt.
Verbreitet sind auch Bilder von ihr, wie sie die Handflächen nach unten gedreht die Arme dicht am Körper hat.
Aus ihren Händen strahlt Licht und drängt die Finsternis zu ihren Füßen tief in den Boden.
Dabei blickt sie entweder gütig lächelnd nach unten auf den Betrachter oder ihr Kopf ist nach oben gerichtet und die Augen andächtig geschlossen.
Trotz ihrer großen Bedeutung ist sie aber nicht so gewichtig, wie |Anaira|.

*„Möge das Licht (Othils) mit Dir sein.”*


Schöpfungsgeschichte der Zwerge
-------------------------------

Vom Hornkamm bis zu den Feuerbergen des Zhark überall wird man die Geschichte des Schmiedes hören, in dessen Esse alles seinen Anfang nahm.
Die Funken stoben in den Himmel und sprenkelten das Nachtfirmament.
Die wertvollen Teile fielen auf sein Meisterstück nieder und brannten sich tief in Aborea hinein.
So viel zu den Gemeinsamkeiten und zur Schöpfung.
Danach bricht alles in widersprechende Lieder und Geschichten auf.
Die sieben großen Götter neben dem großen Schmied Esron koexistieren in einer Symbiose aus Streit und Freude.
Nur Tragur wird gefürchtet der Gott der bösen Zauber und des dunklen Wassers... ihm kann man nicht trauen.


Esron
^^^^^

- Aspekte: Leben, Leidenschaft
- Waffe: Hammer

Esron ist der große Schmied, der leidenschaftlich Dinge schafft.
Er schlägt die Essenzen in Form und hört dabei kaum die Stimmen der Sterblichen.
Ein Zwerg muss daher hart sein wie Granit und Eisen.
Wehklagen wird man von Zwergen nicht hören.
Ein Zwerg muss sich über die wenigen Gaben freuen, die Esron in die Erde Aboreas brachte.
Ein Zwerg muss ebenso emsig sein, wie Esron es lehrt.


Hornwan
^^^^^^^

- Aspekte: Freude
- Waffe: Armbrust

Der Gott der Freude wandelt gerne unter den Sterblichen.
Er überbringt die Botschaften der Götter und soll schon mit manchem guten Zwerg einen Humpen gehoben haben.
Die Reisen Hornwans werden Zwergenkindern am Bett erzählt.


Othelia
^^^^^^^

- Aspekte: Gold, Diamanten
- Waffe: Flegel

Man erzählt sich, dass Otheila den Zwergen Glanz unter Tage schenken wollte und daher eine Strähne ihres Haares in Esrons Esse gab.
Dort schmolz sie und wurde zu Gold.
Mancher Zwerg bleibt ehrfürchtig vor einer Goldader stehen, um kurz Otheilas Geschenk zu gedenken.

Die Diamanten verdankt man ebenfalls der Göttin.
Schließlich sind sie die Funken, die Esrons Esse entsprangen, als ihr Haar hineingelangte.


Tragur
^^^^^^
- Aspekte: Magie, Wasser
- Waffe: Bogen, Harpune

Der einzige gefürchtete (aber nicht weniger geehrte) Gott der Zwerge ist Tragur.
Er herrscht über alle Wasser, und in tiefen Brunnen sollen seine Geister unvorsichtige Zwerge in seine kalten Gefilde ziehen.
In den unterirdischen Seen finden sich augenlose Kreaturen, die mit magischen Licht Unglückliche in die Tiefen locken.
Dort unten soll sich das Tor zur Unterwelt befinden.


Egaros
^^^^^^

- Aspekte: Erz, Handwerk
- Waffe: Axt, Hammer

Am Tage Egaros kommen alle Zwerge zusammen, und die besten Handwerker zeigen ihre Kunstwerke.
Unter ihnen wird ein Meister erwählt.
Dieser Titel ist eine der höchsten Auszeichnungen, und die ganze Sippe wird mit Stolz erfüllt sein.


Barros
^^^^^^

- Aspekte: Kampf, Widerstand, Ehre
- Waffe: Zweihand-Axt

Barros, der Herr über die unendlichen Kristallhöhlen, ist der Bruder Hornwans und schätzt diesen sehr.
In seinem Reich beherbergt er alle guten Zwerge nach ihrem Tode.

In den frühen Zeitaltern, als die Dämonen der Unterwelt sich gegen die Götter auflehnten, war es Barros, der den Kampf entschied.
Sein Mut und seine Entschlossenheit haben den Ausschlag gegeben.
Er Widerstand auch den listenreichen Angeboten Tragurs.
(Stattdessen soll er allen Narren, die den Lügen Tragurs aus Dummheit gefolgt sind, die Ohren lang gezogen haben.)
Typischer Spruch eines zwergischen Ausbilders: *„Erst Barros Werk, dann Hornwans Freud‘."*


Minna
^^^^^

- Aspekte: Feuer, Braukunst
- Waffe: Hellebarde

Die Schutzgottheit der angesehenen Braumeister ist Minna.
Sie steht auch für die guten Speisen.
Sie gilt als eifersüchtig und aufbrausend.
An schlimmen Tagen lässt ihr Gemüt ganze Berge Feuer spucken.
Es heißt, alle Zwergenfrauen würden einen Funken von Minna in sich tragen ganz besonders die rothaarigen.


Schöpfungsgeschichte de Menschen (Trion)
----------------------------------------

Es fällt einem schwer, eine einheitliche und nachvollziehbare Schöpfungsgeschichte aus dem trionischen Pantheon niederzuschreiben.
Diese ist durchsetzt von eigenartigen Geschehnissen und (für Sterbliche) unlogischen Verknüpfungen.
Kaum ein anderer Pantheon ist so lebendig und in einem ständigen Fluss der Veränderung verhaftet wie der Trions.
Und ob seiner Größe und seines Einflusses ist er auch der am stärksten verbreitete.
Teilweise finden sich meist in nur leicht abgewandelter Form die Hauptgötter noch in weit entfernten Inselreichen wieder.
Dagegen ist die Erklärung der Genese Aboreas sehr unterschiedlich.
Sogar innerhalb des Großkönigreiches Trion gibt es Differenzen darüber.
Manchmal sogar innerhalb einer Stadt je nachdem, welche Kirche man befragt.

Im Folgenden findet sich eine Variante:
Zu einer Zeit, als nur die Kreaturen der Urschöpfung existierten, schufen die gewaltigen Urgötter Wesen nach ihrem Vorbild.
Sie sollten als ihre Lieblinge von großer Macht und Willen beseelt sein.
Doch ihre jüngste Kreation wandte sich gegen sie und verdrängte ihre Schöpfer in die Unendlichkeit.

Wer und was die Urgötter sind, bleibt unklar.
in den wenigen Aufzeichnungen über sie sind sie schrecklich und grausam dargestellt.
Die meisten wundersamen Wesen, wie Chimären, Sphinxe, Nymphen usw. sollen ihre Urschöpfung sein.
Sie sind auf Aborea die einzigen Zeugnisse der „Alten", wie einige Kirchen die Urgötter bezeichnen.
Wie sie von ihren „Kindern”, den jetzigen oder neuen Göttern, entmachtet wurden, ist ebenfalls ungewiss.
In einer alten Schrift, die kurz nach dem Erwachen verfasst wurde und vielleicht das älteste Dokument überhaupt ist, wird von einem Irrgarten der Unendlichkeit berichtet und von einer Täuschung der „Alten” durch die „Jungen”.

Nach dem Verdrängen der Alten haben die neuen Götter dann (zumindest) Aborea unter sich aufgeteilt.
Aborea wird hier erstmals erwähnt.
Und diese Welt spielt nur eine Nebenrolle.
In der Hauptsache geht es um das Universum und insbesondere um die drei oberen Welten der Götter: Thals, Eor und Fal.
Die drei wichtigsten, aber auch kleinsten Welten sind ständiger Gegenstand der Streitigkeiten.
Die Welten unterhalb Aboreas sind gewaltig, aber klar aufgeteilt (siehe unten).
Aborea selbst, ungefähr in der Mitte angesiedelt, wird erst im Laufe der Zeit immer interessanter und ist schließlich im Hauptinteresse der meisten Gottheiten.

Seit jener Neuordnung verästelt sich nun ein breitgefächerter Baum von Geschichten, der minutiös Sphären der Macht erläutert und in vielen Folgen abwandelt und weiter ausdefiniert.

Die meisten Kirchen lehnen jegliche Macht dieser Urgötter ab.
Viele haben sie (mittlerweile) aus ihren Lehren gänzlich entfernt und halten die „neuen Götter” für die einzigen jemals existenten.

Die „neuen“ Götter, die nachfolgend nur noch Götter genannt werden, lassen sich in Höhere Gottheiten und Niedere Gottheiten unterteilen.
Die Höheren Gottheiten sind Esthion, der häufig auch als eine Art Hauptgottheit gesehen wird, und seine Geschwister Varus und Seista, sowie Otum, Leceia, Neome, Iandara und |Ainora|.
Von ihnen stammen noch weitere Gottheiten ab, die teilweise ebenfalls als „Höhere” gelten und sich dort neben ihren „Eltern“ behaupten.
In der Summe gibt es 15 Höhere Götter.

Während die Zahl der Höheren Gottheiten noch übersichtlich ist, finden sich in Trion unzählige Niedere Gottheiten.
Einige davon sind stark verbreitet und deshalb hier aufgeführt, andere stehen nur exemplarisch für die große Zahl lokaler Götter.

Die (neuen) Götter wurden (nach den neuen Lehren) aus dem Parnex geboren, schufen die Gestirne, die Welten und später auch die Geschöpfe darauf.
Das Parnex ist das Zentrum des Universums.

Nach den meisten Lehren war Aborea selbst einst ein unbelebtes Land der Götter.
Dort schufen sie Kreaturen und Monster.
Die Motive dafür bleiben aber ungeklärt.
Vielleicht kreierte einer der Götter zuerst Wesen nach seinem Abbild, ließ sich durch diese verehren oder stärkte seine Macht durch deren Essenz.
Dann hätten alle anderen nachgezogen und es ihm gleich getan.
Aber vielleicht ist es auch ganz anders gewesen.
In einer Schrift von Zurak heißt es „wir sind nur zur Belustigung ihrer geschaffen".
Betrachtet man einige Geschichten über „göttliche Eingriffe", dann kann man dem vielleicht sogar zustimmen.

Fakt aber ist, dass die Genese Aboreas (und der Wesen darauf) keine große Rolle für die Kirchen spielt.
Viel entscheidender ist das wirre Netz der Verhältnisse der Götter untereinander, wie man die Gunst der Götter erlangen kann und wie die Existenz nach dem Tod, die alle unterstellen, sein wird.

Nach dem Glauben der Trioner leben sie auf einer Zwischenwelt, Aborea, die über den Unterwelten und unter den oberen Welten liegt.


|Ainora|
^^^^^^^^

- Einflusssphären: Dunkelheit, Nacht, Tod, Zeit
- Zeichen: Schwarz gefüllter Kreis, Helm
- Waffe: Armbrust, Dolch

Wer meint, dass |Ainora| eine böse Gottheit sei, der irrt.
Wie bei allen Göttern des trionischen Pantheons gibt es keine klare Unterscheidung zwischen Gut und Böse.
Vielmehr ist das Verhalten und die Natur der Götter eine Frage der Auslegung und Perspektive.
|Ainora| herrscht über die Unterwelten und gebietet über den Tod.
Sie gilt als geduldig und unnachgiebig.
In den Versen Akkonions heißt es, dass sie einen Pakt mit Esthion habe, der deren Verhältnisse klar regle.
Sie wird vielfach verführerisch schön dargestellt, aber es gibt auch Bildnisse von ihr, auf der sie furchterregend monströs erscheint teilweise sogar als körperloser Schrecken.
Wenn es nach dem Lied über die Schlacht der drei Jahrhunderte geht, dann hat sie zumindest für den Gott Varus gewisse Reize gehabt.
Dem leidenschaftlichen Verhältnis soll Ateom entstammen.


Aone
^^^^

- Einflusssphären: Ehe, Liebe
- Zeichen: Weiße Kugel, Muschel
- Waffe: Dolch

Dem hohen Lied Peolas kann man entnehmen, dass Aone der Verbindung der Götter Iandara und Varus entstammt.
Sie ist die Schwester Mycaels; beide ergänzen sich und gelten als unzertrennlich.
Man sagt von ihr, dass sie einst einen Sterblichen zum Partner nahm.
Die anderen Götter duldeten keinen Sterblichen dauerhaft unter ihnen.
Doch soll er ohne Krankheiten gewesen sein und lange gelebt haben.
Sie blieb ihm bis an das Ende seines kurzen Lebens treu.
Die Trioner erbitten daher auch bei schweren Krankheiten Hilfe von ihr.
Sie gilt aber auch als neckisch und launenhaft.

Nach einer langen Zeit der Trauer und der Werbung durch Ateom ging sie mit diesem eine Bindung ein.
Doch diese war nicht dauerhaft (den Schriften der Obersten Kastelle nach wurde sie von Ateom betrogen).
Aus dem Betrug soll Zia entstanden sein.

Ein beliebtes Bild Aones stellt diese kurz nach dem Tod ihres sterblichen Partners dar.
Die Oberste Kastelle hat beim Großkönig den „TrauerErlass” erreicht, der mittlerweile verbindlich die Trauerzeiten für Witwen und Witwer vorschreibt.


Ateom
^^^^^

- Einflusssphären: Feuer, Reichtum
- Zeichen: nach oben offener Halbkreis
- Waffe: Dreizack

Ateom gilt als leidenschaftlich und zärtlich.
Er ist aber auch listenreich und nicht immer ehrlich.
Aus dem Bruch seiner Beziehung mit Aone resultiert Zia, die ihm in vielem bereits den Rang abgelaufen hat.

Ateom ist der Gott der Diebe, und einige Kirchen lehren die Regeln des „Schaffenden Gewinns“ (Evolution durch Anhäufung von Reichtümern).

Man sagt, dass gewissenlose Händler heimlich Ateom opfern.
Ateom wird von Liebenden angebetet (spöttisch Ehebrechern).
Tatsächlich erbittet fast jeder vor einer Liebesnacht um Ateoms Feuer.

Seine göttliche Mutter AEnora schützt angeblich die „Kinder Ateoms”.


Esthion
^^^^^^^

- Einflusssphären: Führerschaft, Hüterschaft, Stärke, Gerechtigkeit, Wahrheit, Friede
- Zeichen: unten offenes, nach oben gerichtetes Dreieck; Adler; Flügel
- Waffe: Lanze, Speer, Schwert

Esthion wird von vielen als die Hauptgottheit im Pantheon der Trioner angesehen.
Das darf aber nicht darüber hinweg täuschen, dass sein größter Widersacher Otum, der Zweigesichtige, ihm völlig ebenbürtig ist.
Die Trioner wissen das und sind sehr emsig, was das Erlangen der Gunst beider Gottheiten betrifft.
Nichtsdestotrotz ist Esthion beinahe so etwas wie der Schutzpatron Trions.
Sein Zeichen, ein gewaltiger Adler mit abschwingenden Flügeln, ist zugleich die Ehrenstandarte Trions.
Esthion ist der Inbegriff von Wahrhaftigkeit, Stärke und Gerechtigkeit.
Er wacht über Ehre und Frieden, steht für Stärke und den Sieg und beherrscht den Himmel.

Betrachtet man die Vielzahl von Geschichten über den Höheren Gott, dann könnte man auch zu dem Schluss kommen, dass er herrschsüchtig, arrogant und bestimmend ist.

Für viele ist Esthion auch der Herr über Gesetz und Ordnung.

Die Blutlinie des Großkönigs von Trion selbst soll aus der Verbindung des rechtschaffenden Gottes mit einer Sterblichen entstammen.
Folgerichtig sind die Gesetze des Großkönigs zugleich göttliche Regeln.
Eine göttliche Partnerin hat er nie erwählt.

Abbilder des Gottes zeigen ihn meist allmächtig die Arme über alles ausbreitend.
Zu seinen Beinen liegen Speere und Schwerter und sein Blick ist auf die Betrachter hinab gesenkt, so als würde er sie abwägend anschauen.

Trioner schwören bei Esthion, flehen ihn um den Sieg in einer Schlacht an, bitten ihn um Frieden und berufen sich auf das Recht des Stärkeren in Namen Esthions.
Varus ist der Bruder Esthions, Seista seine Schwester.


Hermon
^^^^^^

- Einflusssphären: Frieden, Führerscheft, Krieg
- Zeichen: eine oben und unten offene Acht
- Waffe: Langschwert

Hemron wird mit Aspekten wie Edelmut, Fairness und Besonnenheit in Verbindung gebracht.
Für viele Ritterorden ist er daher Schutzgottheit.
Allerdings sagt man Hemrons Kirche nach, mit dem Schwert zu bekehren.
Arme werden häufig von Hemrons Kirchen versorgt.


Iandra
^^^^^^

- Einflusssphären: Licht, Liebe, Sonne, Wetter
- Zeichen: Halber Sonnenkreis mit Strahlenkranz unten
- Waffe: Stab und Speer

Iandaras Handlungen sind kraftvoll und beharrlich.
Sie ist zugleich liebevoll und wärmend.
Eigenschaften, die Varus seit jeher angezogen haben.
Man sagt, er habe sich an sie verloren.
Doch Iandara bindet sich nicht.
Es gibt eine Stelle im Großen Buch Lesons (welches derzeit in der Lichtkathedrale behütet wird), aus denen hervorgeht, dass Aone aus Varus Liebesklagen hervorgegangen ist.
Immer wieder gibt es Lieder von Sterblichen (und Halbgöttern), die aus Liebe zu Iandara bemerkenswerte Taten vollbrachten.


Juvio
^^^^^

- Einflusssphären: Handel, Glück, Musik, Tanz
- Zeichen: Horizontaler Zacken
- Waffe: Schleuder und Hammer

Die Taverne zum Juvio, die zentral in Padova liegt, zeigt in ihrer kunstvoll geschnitzten Ausstattung die 72 Geschichten Juvios aus den Tagen der großen Blüte.
Allen ist ein ausgelassener und fröhlicher Juvio gemein, nicht selten mit einem Trinkhorn und tanzenden Schönheiten.
Viele Schurken, Glücksritter und Händler verehren Juvio und hoffen auf ein ewiges Leben auf Juvios berauschenden Festen.


Leceia
^^^^^^

- Einflusssphären: Handwerk, Heilung, Kunst, Kultur, Mond, Weisheit
- Zeichen: Mondsichel
- Waffe: Bogen

Leceia ist still, zurückhaltend und überlegt.
Die Töchter Leceias sind Musen, Heilerinnen und Lehrerinnen.
Nur als wehrhafte Kleriker sind Männer Diener Leceias.
Sie wird aber von beiden Geschlechtern gleichermaßen angebetet.

Mycael
^^^^^^

- Einflusssphären: Jagd, Pflanzenwelt, Tiere
- Zeichen: Stilisierter Bogen, Pferde
- Waffe: Bogen

Mycael ist der Bruder Aones.
Oft wird er mit wilden Tieren zusammen dargestellt.
Er wird zumeist von Waldläufern verehrt.
Einige Darstellungen zeigen einen jugendlichen Mycael andere einen gealterten Mann, der unter |Ainora| leidet.
Für viele Anhänger Mycaels sind Pferde heilige Tiere.


Neome
^^^^^

- Einflusssphären: Erde, Fruchtbarkeit, Geburt, Kinder, Landwirtschaft
- Zeichen: Dreieck, Löwin als Botin, Ähren, Obst, stillende Frau
- Waffe: Kurzschwert

Neome ist eine der wenigen Gottheiten, die mit keinem anderen Gott im Streit liegt.
Als Göttin der Fruchtbarkeit ist sie äußerst wichtig für alle Trioner.
Sie wird fast überall verehrt.


Otum
^^^^

- Einflusssphären: Meere, Flüsse, Schicksal, Wind
- Zeichen: Delphin / Hai
- Waffe: Alle Stangenwaffen

Otum, der mit Leceia in der Zeit der Gefangenschaft Esthions (Ballade vom Blutberg) über Aborea herrschte, ist der Gott des Meeres.
Aus diesem Bund kam Hemron hervor.
Otum ist launenhaft und gewaltig.
Er steht für das Schicksal und den Sturm, die Lüge und die Wut.
Aber auch für Klarheit und Wahrheit.
Der Zweigesichtige, wie Otum auch genannt wird, trägt eine riesige Harpune und ein Horn aus dem er Ozeane fließen lassen kann.
Seine Töchter sind kapriziös, und Seeleute singen jeden Abend, um sie zu beschwichtigen.

Seinem Reigen mit Neome entstammt Juvios.


Seista
^^^^^^

- Einflusssphären: Altvordern, Magie
- Zeichen: Raute, Sterne
- Waffe: Stab

Die Schwester Esthions ist das Sternenlicht.
Sie weist Seefahrern den Kurs und Wanderern den Weg.
Sie ist die Göttin des Wissens und der Magie.
In vielen Schriften wird sie als Sonderheit unter den Göttern benannt.
Sie scheint etwas von den „älteren“ Göttern zu haben.

Die Lichter Seistas, wie die Dienerinnen Seistas sich nennen, haben ewige Jungfräulichkeit gelobt.


Thioas
^^^^^^

- Einflusssphären: Gewalt, Blitz
- Zeichen: Blitz
- Waffe: Flegel und Schleuder

Derjüngste der Götter und Bruder des Hemron, ist gewalttätig und streitlustig.
Er wurde von seinem Vater aus der Oberweit auf Zeit in die Unterwelt verbannt.
Doch soll er wütend seine Tage absitzen.
Es gilt als unklug sein Zeichen offen zur Schau zu tragen, wenn man nicht die anderen Götter verärgern will.


Varus
^^^^^

- Einflusssphären: Krieg, Wettkampf
- Zeichen: Halbkreis auf Ebene, Wolfsrudel
- Waffe: Schwert, meist Bastardschwert (und Schild)

Esthions Bruder ist der Gott des Krieges.
Manchmal mit Schwert und Schild dargestellt, manchmal mit Speer und Schild, thront er über den Schlachten Aboreas.
Er gilt als unbezwingbar und wild.
Nach dem Lied über die Schlacht der drei Jahrhunderte ist er aber gegenüber jeglicher Furcht blind.
Dort wäre es beinahe geschehen, dass Varus selbst gefallen wäre.
Doch sein Bruder Esthion und lEnora retteten ihn.

Varus wird jung und kraftvoll dargestellt, er gilt als übermütig, waghalsig und spontan.
Es ist kaum ein Krieger verstellbar, der nicht Varus in einer Schlacht angerufen hat.
Viele versuchen, durch ihren Mut Varus zu gefallen.
Sein Missfallen nämlich gilt als Garant für eine Niederlage.


Zia
^^^

- Einflusssphären: Rache, Gerechtigkeit, Vergeltung
- Zeichen: Raute mit offenen Dreiecken oben und unten
- Waffe: Axt

In der Hafeneinfahrt Angors steht die Zia nach Gzarnael gewandt.
Die Arme hält sie über Kreuz in Brusthöhe und überdeckt ihre Nacktheit.
In jeder Hand trägt sie eine Axt und Zorn scheint Blitze aus ihren Augen zu feuern.
Sie trägt die Krone der Gerechtigkeit (die befeuert wird).
Die Statue ist mit über 20 Metern Höhe zugleich der größte Leuchtturm der Bucht.
Zia steht für Gerechtigkeit und Rache.
Sie kann zornig und unberechenbar sein.
Viele Trioner opfern Zia, um von Schmerzen verschont zu werden oder um Vergeltung zu erreichen.
Zia ist eine junge Gottheit, doch sie gilt als eine der mächtigsten.


Andere Schöpfungsgeschichten
----------------------------

Es gibt unzählige weitere Schöpfungsgeschichten, Göttermythen und Legenden auf Aborea.
Als abschließende Beispiele sollen die Gnome und Halblinge folgen.

Die Gnome glauben daran, dass alles aus den Elementen besteht.
Diese sind lebendig und die einzigen Götter.
Sie stehen für Gutes und Schlechtes gleichermaßen.
Der Legende nach gibt es neben den bekannten Elementen noch ein weiteres.

Halblinge meinen, dass sie direkte Kinder, Enkel und Urenkel der Großen Mutter lnsa und dem Großen Vater Elm sind.
Pria ist das einzige der vielen Kinder, das sie zu sich genommen haben.
Alle anderen dürfen im Garten Aboreas ihr Glück finden.
Dort sind sie nun ihr eigener Herr, können selbst Familien gründen und ihr Schicksal bestimmen.


Ausgewählte Riten & Bräuche der Trioner
=======================================

Geburt
------

Bei einer Geburt ist, wenn möglich, eine Schwester Neomes zugegen.
Ihr großes Wissen und ihre Fertigkeiten hierbei haben schon so manchen Tod im Kindbett dabei verhindert.
Sie erhalten dafür von der Familie Früchte und Korn für den Tempel.
Von einem Teil ernähren sich die Schwestern, und alles andere wird Neome zu Ehren geopfert.
Es ist üblich, dass der Mutterkuchen ebenfalls der Göttin dargeboten wird.

Verstirbt das Kind und/oder die Mutter bei der Niederkunft, dann kehrt die Schwester mit blutverschmiertem Gesicht und laut schreiend in dem Tempel Neomes zurück.
Allerdings nicht ohne vorher gewissenhaft die Ursachen zu prüfen.
War nicht Neome selbst der Grund, und hat sich eine andere Kreatur in das natürliche Wirken Neomes eingemischt, dann wird diese die Rache Neomes (bzw. des Tempels) treffen.

Dieser Umstand hat einige dazu motiviert, die Schwestern überhaupt nicht mehr zu einer Geburt zu rufen teils aus Angst vor der Rache oder der Willkür, die damit einhergehen kann.


Reife
-----

Mit Vollendung des 14. Lebensjahres tritt die erste Reife ein.
Sie symbolisiert den letzten Schritt vom Kind zum Mündigen.
Vielerorts und insbesondere in Trion ist damit das Recht zur Vermählung und des Handels verbunden. (Allerdings darf man bereits in jüngeren Jahren durch die Vormünder verheiratet werden, was durchaus die Regel darstellt, da dann die Vorzüge aus einer solchen Verbindung schon genossen werden können.)


Schwert-Reife
-------------

Die Schwert-Reife gilt als Voraussetzung für die Erlangung des Eigentums an einem Schwert bzw. einer „hohen“ Waffe.
Vorher ist das Führen einer Waffe zwar möglich, aber diese wird dann einem anderen gehören.
Damit geht regelmäßig die Pflicht zum Waffenbeistand einher.

Diese Regel ist von dem Dogma Hemrons flankiert, und die Weihe zur Schwert-Reife erfolgt durch die Kirche Hemrons.
Auf Schlachtfeldern häufig ist die Kriegs-Weihe, die dann von einem bereits Schwert-Reifen mit den Worten: *„Hemron, seht, vor Euch seht Ihr diesen Mann/Frau. Besonnen verspricht er/sie die Klinge zu führen und nur denen dies zu erlauben, für die er/sie vor Euch bürgt. Seht, er/sie ist nun eine/einer von uns.“*  vollzogen wird.
Überlebenden wird dann oftmals später noch ein feierlicher Ritus in einem der Häuser Hemrons zuteil.
Mit der Schwert-Reife wird das 21. Lebensjahr vollendet.


Hochzeit
--------

Der Bund der Ehe wird von einer bestellten Person der Kirche Esthions geschlossen.
In vielen, insbesondere kleineren Orten fällt dieses Amt mit einem öffentlichen Amt zusammen.
So kann ein Ritter, ein Graf usw. auch zugleich das Oberhaupt bzw. der höchste Vertreter der Kirche Esthions (und auch anderer) sein.
In größeren Städten ist, falls auch hier Personalunion besteht, aber dann eine andere öffentliche Person unterbevollmächtigt.
Diese können dann auch Priester des Esthion sein.
Es ist aber auch durchaus üblich, dass ein Ritter die Ehe schließt (formell), und ein Mönch oder Priester Esthions die Bitten und Segnungen spricht.

In ländlichen Gegenden schließen Geistliche gleich welcher Kirche die Lebensbünde.
Manchmal auch selbsternannte Propheten lokaler (und niederer) Götter.

Vor der Krone Trions gültig ist aber nur eine eingetragene Verbindung.
Diese gilt fort, solange die Personen leben.
Trennungen sind nur möglich, wenn schwere Verstöße stattgefunden haben.
Die Unfruchtbarkeit oder das Unvermögen die Familie zu ernähren gehören allerdings dazu.

Mehrfach-Ehen sind aber auf Grund des inoffiziellen Charakters einiger Bünde durchaus verbreitet.

Der Schluss einer Ehe wird mit einem großem Fest begangen, bei dem Arme gespeist werden müssen.

Ein fast vergessener Ritus während einer Hochzeit ist die Herausforderung des künftigen Ehemanns unter Bezugnahme auf das Recht des Stärkeren.
In einem Ringkampf kann dann der Herausforderer um die Frau kämpfen.
In den meisten Regionen ist dieses „Recht” durch die Krone aufgehoben, um den Frieden zu wahren.


Alter
-----

Das 5O. Lebensjahr wird bei Bessergestellten gerne mit einer Feier zu Ehren Leceias begangen.
Lobgesänge auf den „Alten“ und seine Weisheit sind dabei ebenso üblich wie die Darbietungen von Künstlern.
In größeren Städten hat sich auch die Sitte eingebürgert, dem ersten Gratulanten, der noch nicht das siebte Lebensjahr vollendet hat, die Ausbildung zu zahlen.


Tod
---

Mit dem Tod wird häufig eine Überfahrt oder Reise verbunden.
Nur selten werden die Leichname vergraben.
Häufiger ist das Verbrennen der Toten.
Dabei werden sie in Boote gebettet oder auf Holzstapeln prachtvoll aufgebahrt.
Die Überreste werden dann je nach Stand unterschiedlich verwahrt.
Von Hügelgräbern, Löchern, Urnen, Reliquienschreinen, Gruften bis hin zu der „Gabe ans Meer" ist alles in Trion vorzufinden.
Frauen scheren ihre Köpfe und sind verpflichtet, vorbestimmte Zeiten zu trauern, die sich auch nach dem Stand des Verstorbenen richten.
Man trägt Schwarz, kann Trauergesellschaften kaufen, richtet Leichenschmause aus und streitet um das Erbe. (Der Lehnsherr erhält das „Erbfünftel” eine Steuer in Höhe von 20%. Darüber hinaus soll eine Gabe von 10% des Erbes die Götter gütlich stimmen.)

Beim Abschied ist es üblich, dass alle (guten) Taten vorgetragen werden.
Dies soll den Göttern helfen, sich (gut) an den Toten zu erinnern.


Nachleben
---------

Die Seele ist unsterblich.
Nach dem Tod des Körpers gibt es drei Wege, die eine Seele nehmen kann: Die Wiedergeburt, der Untergang oder der Aufstieg.
Die Entscheidung, welcher Weg eingeschlagen wird, ist aber häufig beschwerlich.

Alle „bösen” Taten belasten die Seele.
Sie sickern durch Aborea und nähren die (persönlichen) Dämonen.
Werden sie zu stark, können sie den Täter sogar zu Lebzeiten aufsuchen und versuchen zu verzehren.
Sie warten aber auch, bis man die sterbliche Hülle verlässt und lauern dann einem auf.
Dann kommt es entscheidend auf die Stärke des Dämons an, ob es einem gelingt, ihm zu entwischen.
Einige arbeiten sogar mit Täuschungen und lassen die Seelen herumirren.
Der Kampf mit einem Dämon kann unendlich lange dauern.
Verliert man ihn, wird die Seele in die unteren Welten abstürzen.
Der Dämon wird einen dann wieder und wieder heimsuchen.

Welche Tat „böse” ist, wird der Auslegung überlassen. (Vielleicht auch ein Grund, warum Zurak die Götter für kapriziös hält.)

Dämonen sind aber nicht nur auf eine Person fixiert.
Sie können sich auch von mehreren nähren oder auch versuchen, aus ihrer Welt auszubrechen.

Wer die Dämonen meistert, gelangt in die Stadt der Toten, die häufig auch einfach Unterwelt genannt wird.
In der gigantischen Nekropolis verliert Raum und Zeit seine Bedeutung.
Irgendwann aber findet die Seele den Weg hinaus und zurück nach Aborea oder hinauf zu den Oberwelten.
Natürlich kann es auch noch hinab gehen...

Der Aufstieg passiert eigentlich nur über die Entscheidung eines Gottes.
Findet eine Gottheit Gefallen an einer Seele (auch zu Lebzeiten), dann kann sie diese zu sich nehmen, wann immer es ihr beliebt.
Sie kann die Seele auch untergehen lassen oder erneut in die Wirren des Lebens auf Aborea entsenden.
Die Wiedergeburt löscht alle Erinnerungen an das vorherige Leben. (Allerdings berichten einige von Personen, die tatsächlich noch einiges aus ihrem Vorleben wissen.)

Der Aufstieg führt in eine der Oberwelten.
In welche obliegt der Gottheit.
Ein weitverbreiteter Wunsch ist der, auf den Feldern der Glückseligkeit des Juvios zu wandeln oder in die Feste Esthions zu gelangen.


Große Feiertage
===============

Tag des Juvio
-------------

Am Abend nach dem ersten Tag der Weinernte wird Juvio zu Ehren ein rauschendes Fest gefeiert.
ln ausgelassener Stimmung wird bis in die Morgenstunden getrunken und gelacht.
Junge Leute tanzen meist um Mitternacht unbekleidet und heimlich um entfachte Feuer.
Die in dem Jahr Gereiften (siehe Reife, oben) werden im Kreis der anderen aufgenommen, indem sie über das Feuer springen.

Unverheiratete Frauen tragen häufig farbige Bänder in den Haaren.


Deihfest
--------

Mit der Aussaat wird ein blutiges Opferfest begangen.
Unter Anrufen Neomes werden Ziegen der Göttin mit der Bitte um eine gute Ernte dargebracht.
Die Bauern müssen für die Gunst einen Schluck des Blutes trinken.
Der einflussreichste Landwirt pflegt sogar das Herz des Opfertieres zu essen.
Das Fest wird von München der Neome angeleitet.
Die Schwestern dürfen nicht zugegen sein.
Traditionell werden mit Kräutern gewürzte Ziegenwürste gegessen.


Friedtag
--------

Am Friedtag dürfen keine Auseinandersetzungen passieren.
Es gilt strenges Waffenverbot.
Prozessionen finden überall statt.
Alle Waffen werden angebunden und um Speerspitzen werden weiße Lappen gewunden.
Den ganzen Tag über wird nicht gegessen.
Es darf nur verdünnter Wein getrunken werden, und erst Abends wird gemeinsam an großen Tafeln gespeist und getrunken meist einfaches Essen (verbreitet ist ein Brei aus gemahlenem Korn, der mit Beerenfrüchten gesüßt ist).


Das Große Flehen
----------------

Wenn in Trion die Sonne den niedrigsten Punkt erreicht und der kürzeste Tag des Jahres anbricht, dann kommt Das große Flehen.

Früher, bevor die Großkönige die Barbarei abgeschafft hatten, wurden sogar Blutopfer dargebracht.
Die Sonne sollte wiederkehren und Aborea nicht für immer verlassen.
Das Große Flehen dauert den ganzen Tag an.
Viele reisen sogar Stunden und Tage zu besonderen Orten, speziellen Kultstätten, an denen im Laufe der Jahre immer größere Sonnentempel entstanden.
Der wohl größte steht in Ambur, auf der Insel in der Mündung des Imperia.
Hunderte von Säulen, Kristallen und spiegelnden Kacheln reflektieren die Sonne in immer breiteren Fächern und ergeben ein verwobenes Muster.
Doch genau nach dem Tag des Großen Flehens bricht die Sonne in nur einem einzigen Strahl durch die Hallen und wird um Ecken und durch Räume geleitet um endlich in einem großen gebündelten Strahl als leuchtende Säule in den morgendlichen Himmel zu schießen.

Entgegen den Gesetzen werden hier bis heute der Göttin blutige Geschenke gemacht.
Rund um den Tempel campieren zehntausende von Gläubigen und bereits Tage davor wimmelt es hier nur so von Menschen.

In den großen Städten Trions hingegen werden auf den Plätzen Kohlebecken aufgebaut.
Dabei ist es Sitte, dass jeder seinem Flehen durch das Verbrennen wertvoller Kräuter Ausdruck verleiht.
Im Norden ist es zudem Brauch, den Bedürftigen etwas zu schenken.
Doch viele Mächtige wollen sich weiterhin der Gunst versichern, in dem sie im Geheimen die alten Riten noch praktizieren.
Dort gilt sogar, dass je größer das Opfer ist, desto wahrscheinlicher ist die Gunst.
Auch auf dem Land verlässt man sich nicht auf die Hingabe von Kräutern, sondern opfert heute Tiere.
Wird das Flehen erhört, dann bricht ein neuer Zyklus an.

Den Berichten einiger Fernhändlern zufolge, gibt es ähnliche Rituale auch in den nördlicheren Gebieten Paleas.


Die Wehr
--------

Im Gedenken an das Tritum, soll sich jeder Trioner an diesem Tag seiner Wehrhaftigkeit und Bereitschaft versichern.

Jeder ausgemusterte Scundor, gleich wie alt und wie lange schon aus dem Dienst entlassen, muss sich in der nächsten Kaserne oder einem öffentlichen Gebäude in voller Montur und kampfbereit melden.

Jeder Bürger hat „Schwert und Schild”, wie es heißt, zu ergreifen und sich bei der Miliz zu melden.

Während die alten Veteranen noch dienstbeflissen aufmarschieren, wurde in den letzten Jahren die Miliz immer kleiner.
Viele schauen lieber zu und verlegen den Festbeginn für sich nach vorne.

Nach einer heute eher halbherzigen Inspektion, werden Schaukämpfe veranstaltet und in größeren Städten auch ganze Schlachten aus vergangenen Zeiten nachgestellt.
Es ist dann ein Volksfest, auf dem alte Siege glorifiziert und Geschichten immer großartiger werden.

Auf dem Land ist Die Wehr mittlerweile nur noch ein Fest und vielerorts erinnert man sich schon gar nicht mehr an den eigentlichen Sinn.

Auch wenn heute noch kleine Sträuße mit blauen Blumen nach alter Sitte überall angeboten werden: Kaum einer weiß noch, dass dies in Erinnerung an das Abschiedsgeschenk passiert, das damals die Daheimgebliebenen den ausziehenden Verteidiger gegeben haben.


Leitmagie formen
================

Das Zaubern für Leitmagie-Kundige folgt den gleichen Regeln, wie das der Essenz-Zauberer.
Lediglich das Bezugsattribut ist Charisma für Kleriker statt Intelligenz.
Zur Unterscheidung von Leitmagie und Essenz siehe auch Seite 37.


Göttliche Punkte
================

Der Spielleiter kann für positive Aktionen (aus Sicht ler verehrten Gottheit) göttliche Punkte vergeben.
Diese können dann in besonderen Situationen eingesetzt werden.
Näheres dazu im Spielleiterheft.
