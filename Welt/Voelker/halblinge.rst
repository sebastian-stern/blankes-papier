Halblinge
=========

Viele Bezeichnungen haben sich für dieses kleine Volk etablieren können.
Sie selbst nennen sich Windler oder Hügler, andere nennen sie Halbhohe oder Halblinge.
Ungebildete können sie auch mit Zwerg, Gnom, Wicht oder anderem betiteln.
Diese Verwechslungen passieren nicht zufällig, denn sie sind ebenfalls klein wie die Zwerge (aber etwas kleiner und zugleich leichter; und ihr Körpergewicht lässt sich eher auf Honigkuchen und untergäriges Bier zurückführen), und sie leben teils auch „unter Tage”.
Teils deswegen, weil sie in Hügeln oder Höhlen leben und durchaus einen Großteil des Tages im Freien verbringen.
Es ist daher aber nachvollziehbar, dass wenn jemand Zwerge zuvor nur aus Lagerfeuergeschichten kennt die Halblinge für Zwerge gehalten werden.

Dabei sind sie aber auch sonst so völlig verschieden zu den Zwergen.
Während diese als eifrig, arbeitsam ja sogar als gierig charakterisiert werden, treffen diese Attribute überhaupt nicht auf die Halblinge zu.
Sie sind gemütlich, mit sich selbst sehr zufrieden, lieben die Ruhe und verbringen ein ganzes Leben so, wie andere sich ihren Ruhestand vorstellen.
Dabei sind sie auch ein wenig konservativ, will man meinen.

Sie bauen in kleinen Gärten alles an, was sie zum Leben brauchen, und sie sind kunstfertige Handwerker.
Sie nehmen sich dabei so viel Zeit, wie das Ergebnis eben braucht, um schön anzusehen zu sein.
Ihre Kunstwerke werden, wenn sie überhaupt in den Handel gelangen, hochpreisig angeboten.
Sie sind vortreffliche Literaten und Musiker.
Ihre Stimme ist bei weitem nicht so fein wie die eines Elfen, aber sie versprühen Lebensfreude mit ihren Trink- und Festliedern.
Das soll aber keineswegs bedeuten, dass sie oberflächlich sind und nur die freudigen Seiten des Lebens kennen.
Dieses Vorurteil hält sich leider hartnäckig, obwohl sie auch tiefsinnige Philosophen sein können.
Aber im überwiegenden Teil bestimmt dieses gelassene und voll Freude gelebte Leben das äußere Erscheinungsbild.

Ihre Kleidung ist naturbelassen, und es werden vorzugsweise Schafe als Ressource zur Textilherstellung genutzt: Ihre Felle, das Leder oder die Wolle gefärbt wird höchstens mit Erdtönen aus Mineralien oder Pflanzen, ganz selten mit roten oder blauen Farbstoffen aus Pflanzen oder Kleintieren.
Halbling-Männer und Frauen stricken und häkeln gerne.

Ihre Produkte verschenken sie dann unter Freunden oder ihren weitreichenden Verwandtschaften.
Ihre Stammbäume halten sie akribisch in Schriftrollen und dicken Folianten fest, verfolgen sie über enorme Distanzen und organisieren Familientreffen in regelmäßigen Abständen, bei denen auch die entferntesten Mitglieder anreisen und man zusammen ein riesiges Fest begeht.
Sie sind monogam, werden bis zu 150 Jahre alt und haben selten mehr als vier Kinder.

Eines ist noch erwähnenswert: Halblinge erhalten sich ihr ganzes Leben lang das ehrliche Lachen eines Kindes.
(Natürlich können sie sich bei seltenen Gelegenheiten auch trotz ihrer friedliebenden Art bestens streiten und zanken.
Meist ging dem Streit eine lange Tradition von Lästerungen voraus.
Gerade unter Nachbarn kann dies vorkommen.)
Hinzukommt, dass sie regelmäßig auch bis ins hohe Alter ein kindliches Gesicht behalten und bartlos daher kommen.
Kein Wunder, dass es Menschen manchmal schwer fällt, sie im ersten Augenblick ernst zu nehmen.
Dabei haben sie aber allerlei versteckte Talente und außerordentliche Fähigkeiten.
Ihre augenfällig behaarten und großen Füße, die sie eigentlich nie in Schuhen verhüllen, sind widerstandsfähig gegen allerlei Unbill, und ihre friedliche und unschuldige Art macht sie wohl zu Günstlingen der Götter.
Denn wie soll es sich sonst erklären lassen, dass sie gegen Zauber und Magie so ungewöhnlich resistent sind?

Halblinge glauben an drei Urgötter: Elm, Insa und Pria.
Aber es gibt auch eine Unmenge an Geistern und kleineren Gottheiten.

Siedlungen der Halblinge kann man vor allem in hügeligen Regionen vorfinden.
Diese sind bis zu 30 (Wohn-) Erdhügel groß und idyllisch angelegt.
Auf Palea gibt es kaum mehr als 100.000 Halblinge nach neuesten Schätzungen.
Da sie häufig mit anderen Kleinwüchsigen verwechselt werden (siehe oben), wird die tatsächliche Zahl noch darunter liegen.


Sprachen
--------

Die Sprache ist abwechslungsreich und vielstimmig.
Sie gilt als größere Sprache und ist schwer zu lernen.
Es gibt unter anderem eine verbreitete Schreibschrift, die schnörkelig und verspielt aussieht.


Besondere Eigenschaften
-----------------------

Auf Grund Ihrer Größe sind Halblinge schwerer zu treffen (+1 Rüstung).
Außerdem widerstehen sie Magie besser (+1).
Mehr dazu in der Übersichtstabelle Völker, Seite 10, und im Spielleiterheft.
