Zwerge
======

Von den großen Völkern sind die Zwerge das kleinste (vom Wuchs her).
Das Gro ist um einen Meter dreißig groß, und selten gibt es einen Zwerg, der mehr als eineinhalb Meter misst.
Die Frauen sind im Schnitt fünf Zentimeter kleiner.
Beim Gewicht hingegen gehören sie in die Spitzenklasse.
Jeder Zentimeter wird grob mit einem Kilo aufgewogen.
Frauen können bis zu 10% weniger auf die Waage bringen.
Wer nun meint, Zwerge wären fettleibig, irrt und könnte diesen Irrtum auch bitter reuen.
Denn ein Großteil davon sind das überaus widerstandsfähige Skelett und die Muskeln.
Diese Kompaktheit ist mit einer enormen Ausdauer gepaart und lässt Zwerge tagelang durchmarschieren, stundenlang Tunnel graben oder Schlachten schlagen, und sie schließen in aller Regel als Letzte die Zechrunde.

Legendär sind die zwergischen Taten in Schlachten.
Sie gelten als unbezwingbare Verteidiger und unaufhaltsame Angriffswalzen.
Ihre Erzfeinde, die Trolle, fürchten und hassen sie darum nur noch mehr.
Auch Drachen können als natürliche Feinde der Zwerge eingestuft werden.
Allen anderen Völkern sind die Zwerge grundsätzlich neutral gegenüber eingestellt.
Im Laufe der Zeit haben sich aber die Orks recht hoch in der Unbeliebtheitsskala platzieren können.
Traurigerweise haben auch die Elfen dort einen Platz.
Während die Orks in kriegerischen Auseinandersetzungen sich diesen Platz erkämpft haben, schaffen die Elfen dies durch das Fernbleiben bei solchen Konflikten.
Viele dieser Momente liegen in ferner Vergangenheit, aber Zwerge und Elfen begegnen einander selten, und Zwerge haben ein enormes Gedächtnis für solche Dinge.
Nichts, was man nicht wieder hinbekommen kann, aber es existieren schon eine Menge Vorurteile und Vorbehalte gegenüber den „selbstgefälligen Langohren".

.. sidebar:: Der Kampf um das Ghalgrat, 1217 AZ

   Als vor Urzeiten die prächtigen Hallen Sorogs durch eine große Zahl von Feinden bedroht wurden, haben die Zwerge seinerzeit die Elfen des Casnewydd um Beistand gebeten.
   Die Elfen tagten tagelang und verpassten die Chance, gemeinsam mit den Zwergen den Feind zu schlagen.
   Stattdessen verschanzten sich die Zwerge auf sich alleine gestellt im Berg.
   Der Feind nahm seinen Weg durch die Heimatgebiete der Elfen im Casnewydd, und fast alle Elfen starben bei der Verteidigung.
   Dann stürmte der Feind siegreich die Feste der Zwerge.
   Zusammen hätte man damals vielleicht siegreich sein können, und die Hallen wären nie verlorenen gegangen.
   Diese Wunde sitzt bei den Zwergen tief.

Das Sprichwort „Mit dem Kopf durch die Wand wollen” könnte durchaus mit dem Volk der Zwerge zusammenhängen.
Zum einen gelten sie als das vielleicht sturste Volk auf Palea.
Zum anderen hat man durchaus schon Zwerge mit dem Kopf rammend durch dicke Holzwände brechen (oder an Steinwänden scheitern) sehen.
Wenn sich einmal ein Zwerg etwas in den Kopf gesetzt hat, dann ist es schon ein Kunststück, ihn wieder davon abzubringen.
So sehr das für männliche Zwerge gilt, so wenig wiederum kann man dies den Zwergenfrauen nachsagen.
Sie treten in aller Regel vernünftig und überlegt auf außer wenn es um ihre Männer geht.
Dann nämlich lösen sie Konflikte wie diese: Mit Kraft und Gewalt.
Das ist insbesondere deswegen problematisch, weil sie zudem auch extrem eifersüchtig sind, zugleich aber gerne anderen Männern schöne Augen machen (auch wenn diese in festen Händen sind).
Natürlich gibt es auch hier Ausnahmen.

Zwerge sind familiär und binden sich recht früh an eine Frau meistens für ein ganzes Leben.
Es ist durchaus üblich, im Laufe der Bindung dann bis zu dreißig Kinder zu zeugen und großzuziehen.
Diese werden dann mit achtzig ein vollwertiges Mitglied der Gesellschaft und gründen selbst eine Familie.
Trotzdem sind sie weiterhin eng mit ihrem Vater, Mutter und Geschwistern verbunden und darüber hinaus auch mit allen Generationen davor.
Sie glauben fest daran, dass sie alle untereinander verbunden sind, dass der Geist der Familie in jedem lebt und sie auf diese Weise unsterblich sind.

Der Schöpfer Aboreas ist der Große Schmied des Lebens, Esron, der auch die Gestirne und den Mond erschuf.
Sie huldigen ihm und weiteren sechs großen Göttern, sowie einer Vielzahl kleinerer Gottheiten.
Sie erbitten Wunder von ihnen, aber sie wissen auch, dass sie deren Gunst erst (durch Taten) erwerben müssen.
Alles Unerklärliche kommt von diesen Göttern.
Sowohl alles Gute, als auch alles Schlechte.
Zum Schlechten gehört auch die Magie, die Tragurs Werk ist.
Tragur ist der Gott der schwarzen Wasser, der auch über die Ozeane und die tiefen Brunnen herrscht.
Er gilt als launisch und schelmenhaft.
Daher fürchten sie auch das Meer und die Seen, und nur wenige betreten je ein Schiff.
Böse Zungen behaupten, Zwerge würden sich nicht waschen.
Tatsächlich aber wollen sie Tragur nicht erzürnen und nehmen nur an seltenen Gelegenheiten ein Bad.
Etwas anderes gilt, wenn sie trinken, denn der gute Hornwan, Götterbote und Freund der guten Unterhaltung, schwatzt Tragur jeden Tropfen ab.
(Man braucht nicht zu erwähnen, bei welcher Gelegenheit der Spruch „Zahlen wir Hornwans Schuld ab.” Verwendung findet.)
Besser noch ist es, wenn der Braumeister das Wasser gereinigt und veredelt hat.
Dieser ist ein beliebter und angesehener Mann und verantwortlich für die vielen Fässer, die man für einen Zwergenfest braucht.

Manch einer behauptet über die Zwerge, dass sie mürrisch, humorlos und stets schlecht gelaunt sein.
Wer aber jemals bei einem Zwergenfest (oder besser: Gelage) gewesen ist, wird das Gegenteil wissen.
Selbst die (unausweichlichen) Prügeleien mindern nicht den Eindruck, schließlich liegen sie sich später wieder in den Armen.
Dann mit einem Zahn weniger, einem blauen Auge und um ein paar Erfahrungen reicher werden wieder Trinklieder geschmettert oder Geschichten und Gedichte vorgetragen.

Diese Feiern werden in den großen Hallen veranstaltet, die tief in einen Berg geschlagen sind oder in großen Tiefen eingerichtet wurden.
Zwerge leben nämlich vorzugsweise unter der Erde oder besser noch in den Bergen.
Diese Bereiche sehen sie als ihr Territorium an, gleich wer oberhalb der Erde meint, herrschen zu wollen.
(Was natürlich zu Konflikten führen kann.)
Sie können ohne Tageslicht und sogar in absoluter Dunkelheit sehen (sofern diese nicht magischen Ursprungs ist).

Sie sind passionierte Bergleute und schürfen nach allem Edlem unter Tage.
Was als Stollen beginnt, kann im Laufe der Zeit zu einer prächtigen, säulengetragenen Halle werden oder sogar ganze Burgen im Berg beheimaten.
Ganze Zwergenreiche sind so entstanden, und es ist schon eine außerordentliche Seltenheit, eine Siedlung von Zwergen über Tage zu finden.
Dabei kommen Zwerge auch mit dem Leben dort gut zu recht.
Einige von ihnen haben sich sogar nahe den Menschen oder unter ihnen angesiedelt.
Dort unterhalten sie zum Beispiel Handels- oder Gasthäuser oder gehen einem Handwerk nach.
Häufig spüren sie aber irgendwann den Ruf der Berge und kehren dann dorthin zurück.
Wenn Zwerge ein Menschenleben lang unter Menschen gelebt haben, dann war dies für deren Verhältnisse lange, wenn es auch nur ein Bruchteil ihrer natürlichen Lebensspanne ist.
Durchschnittlich werden Zwerge 400 Jahre alt, und manche überdauern sogar 80 Jahrzehnte.
Die meisten aber sterben an einem unnatüriichem Tod: im Kampf gegen Feinde, bei Unglücken unter Tage oder durch andere Situationen.
Die gesamte Population von Zwergen auf Palea wird auf 16 Millionen geschätzt.

Unter ihnen gibt es weit mehr als nur Bergleute und Krieger:
Zwergische Schmiede gelten als die besten ihres Faches, und eine Zwergenarmee rüstet sich üblicherweise in hervorragender Platte und Schild.
Die bevorzugten Waffen der „wandelnden Festungen” sind Kriegshammer, Axt und Armbrust.
Aber sie greifen auch zu anderen Klingenund Stangenwaffen.

.. sidebar:: Die Zipfelmütze

   Einige Zwerge tragen tatsächlich zu passenden Gelegenheiten Zipfelmützen.
   Diese sind die traditionelle Kopfbedeckung und gelten noch heute als kleidsam.
   Es gibt sie als Woll- oder Filzvariante und manchmal sogar mit Pelzbesatz.
   Auch der Schnitt ist abwechslungsreich, so gibt es zum Beispiel welche mit Ohrenschützern oder mit Trageband.
   Rote Mützen werden selten getragen, häufiger ist grau oder dunkelbraun.
   Manche sind gehärtet, stehen stets aufrecht und bieten Schutz vor herabfallenden Kieseln und kleineren Steinen.
   In unsicheren Zeiten ist es auch üblich, noch einen Helm darunter zu tragen.
   Außerhalb des Zwergenreiches oder in Gefahrensituationen wird die Zipfelmütze in der Kleidertruhe gelassen oder im Tornister verstaut.
   Dann präsentiert man sich mit Helm.

Unter der Rüstung oder beim Tagwerk (wenn dieses nicht aus dem Kampf besteht) tragen die Zwergenmänner meist farblose Kleidung: Lederhäute, Felle und grobe Stoffe in Naturfarben.
Nur zu besonderen Anlässen kleiden sie sich so, wie man es von den Zwergenfrauen kennt: Reich verzierte Strickwaren, gefärbte Kleider und bester Schmuck.

Sie tragen Bärte, und in manchen Stämmen ist es Mode, darin Schmuck zu tragen, ihn zu flechten und darin Ringe zu verwenden.
Die Gesichtsbehaarung kann von dem Kopfhaar farblich abweichen, wenn auch selten.
Rotes und braunes Haar sind weit verbreitet, schwarzes ist seltener, und blondes gilt als Zeichen der Götter.

Der Schmuck hingegen ist farbenfroh, und die Schmiede sind auch Meister der Schleifkunst: Sie facettieren Rohlinge so kunstvoll, dass sie das Licht perfekt brechen und reflektieren.

Neben dem Schmiedehandwerk gibt es auch andere hervorragende Handwerker.
Berühmt sind die zwergischen Ingenieure, die Wasserleitungen, Brunnenanlage, Fördersysteme, Lorentrassen und ähnliches konstruieren.
Und sie sind gefürchtet, wenn sie Kriegsmaschinen bauen oder Verteidigungsanlagen und ihre Feinde mit raffinierten Fallen empfangen.
Mit diesen mechanischen Verteidigungsanlagen schützen sie auch ihre Horte, die tief in ihren Festungen liegen und wo sie ihre Reichtümer aufbewahren.

Der Reichtum der Zwerge besteht vor allem aus den gewonnenen Bodenschätzen und den prachtvollen Handwerksleistungen.
(Einiges davon soll sogar besondere Kräfte der Götter tragen, die gesegnete Schmiede in sie gelegt haben.)
Der Großteil ist aber Gold und Silber: Teils in Barren, teils aber mit den Zeichen der Götter geprägte Münzen.
Manchmal, wenn Zwerge den Hort einer räuberischen Kreatur beschlagnahmen, dann prägen sie die Münzen daraus sogar um.
Nach dem Glauben der Zwerge sind die Edelmetalle Erzeugnisse der Götter (genaueres siehe Religion der Zwerge) und damit ein Stück weit heilig.
Selbstverständlich horten Zwerge auch gerne Reichtümer und erfreuen sich an ihrem Glanz.
Daher gelten sie als sparsam mancher mag auch sagen als gierig.


Sprachen
--------

Es gibt etliche Sprachen unter den Zwergen.
Sie unterscheiden sich in der Aussprache teilweise erheblich, teils sind es aber nur Akzentuierungen.
Auch gibt es sprachliche Schwerpunkte, also einen größeren Wortschatz in einem Sinnfeld, und teils auch andere Wortbedeutungen.
Im Kern ähneln sie sich aber und weisen im Kernbestand sogar eine erstaunliche Kontinuität auf.
Besonders deutlich wird dies im Schriftbild, das zum Großteil aus eckigen und geraden geometrischen Figuren besteht.
In der großen Bibliothek von Isandris sind über 800 verschiedene Zeichen aufgeführt.

Ein zentrales Zeichen ist das nach oben gerichtete Dreieck, was zugleich auch für den Gott des Erzes steht (Egaros).
Das Zeichen für Stein ist zum Beispiel das Dreieck mit einem geraden Strich darunter (Sear [se:-a:r]).
Der Plural wird durch eine Zeichenwiederholung oder (wie bei „Steine”) durch einen Strich unter dem Symbol ausgedrückt (also ein Dreieck und darunter in dem Fall zwei Striche: Seear [SeAr]).

Der Satzbau richtet sich nach Subjekt Prädikat Objekt.
Eine Frage wird umgekehrt aufgebaut.
Beispielsweise bedeutet „Ga Tor Phawr Sear”: Ich kann Stein bearbeiten (Ich kann bearbeiten Stein).
In Zeichen sähe der Satz so aus: .
Als Frage würde sie so lauten: Stein bearbeiten kann ich? (Sear Phawr Tor Ga)

Ausgesprochen viele Worte existieren für die Steine, Metalle oder Werkzeuge im Vergleich zu den Sprachen der Menschen.
Es gibt zum Beispiel ein Wort für „Weiche Steinablagerung mit Eisenerz mit einem Anteil von 6 zu 10” (Seargaon).
Wortkarg ist die Sprache regelmäßig bei oberirdischen Pflanzen was aber durch den Lebensraum bedingt ist.

Soweit gilt das vorgenannte alles im Allgemeinen für die meisten Zwerge.
lm Speziellen kann es aber Unterschiede geben.


Untervölker
-----------

Von Süden nach Norden lassen sich die großen Kulturräume oder Untervölker der Zwerge festhalten.
Den Anfang im Süden macht das Zwergenreich im Zhark-Gebirge.
Umgangssprachlich werden die dort lebenden Zwerge auch Feuerzwerge genannt.
Man sagt ihnen nach, in der Gunst Paras zu stehen.
Danach folgen die Kronzwerge (aus dem Krongebirge), die Grauzwerge aus dem Andar und die Hornkamm-Zwerge aus den gleichnamigen Bergen.
Es gibt aber viele weitere Stämme, die aber zahlenmäßig diesen weit unterlegen sind zum Beispiel die Whalbra oder die Stiegbahr.
Eine zu erwähnende Unterart sind die „Verlorenen” (oder auch „Tragurer").
Auch wenn diese vermutlich nur wenige Tausend sind, muss man sie aber aufführen, da sie so andersartig sind.
Einige gehen sogar so weit zu sagen, dass es sich dabei überhaupt nicht mehr um Zwerge handelt.


Feuerzwerge
-----------

Das feuerspuckende Zhark-Massiv im gleichnamigen Gebirgszug ist die Heimat des wahrscheinlich kriegerischsten Zwergenreiches.
Umgeben von feindlichen Kreaturen und an der Grenze zum Orkreich, behauptet sich das Reich seit ewigen Zeiten erfolgreich.
Dabei sind schon Armeen wie wogende Wellen gegen den Berg geschlagen.
Die Zhark-Zwerge sind im ständigen Kampftraining, und ihre ganze Kultur ist auf den Krieg ausgelegt.
Sie befehlen über das Feuer und sind in der Lage, den ganzen Berg Verteidigungsperimeter für Verteidigungsperimeter-zu entzünden.
Dazu leiten sie flüssigen Stein in ständig gewarteten Kanälen und Betten um den Berg im Falle eines Angriffs.
Wer auch immer dies überlebt oder überwindet, sieht sich brutalen Kriegsmaschinerien, heißen Ölbädern und Tausenden von scharfen und geübten Äxten gegenüber.
Ihre Siege pflegen die Feuerzwerge mit Feuerwasser zu begießen.
Die Anzahl der zerschmetterten und gespaltenen Feinde werden in die Haut tätowiert und zeugen so von der Macht des Zwerges.
Was sich so überragend liest, täuscht über das zerbrechliche Konstrukt hinweg.
Die Gunst Paras und eine gehörige Portion Glück gehören schon dazu, um sich hier erfolgreich zur Wehr zu setzen.
Und selbst damit sind sie bloße Verteidiger und von sich aus nicht in der Lage, die Bedrohung endgültig abzuwehren.
Am meisten beunruhigend ist aber die Tatsache, dass innerhalb der Zharkberge ein Feind haust.
Immer wieder nun schon seit Jahrhunderten verschwinden Zwerge plötzlich.
Die Lage ist im Griff, solange Vorsichtsmaßnahmen ergriffen werden.
Die Feuerzwerge kämpfen einen nicht enden wollenden Zweifrontenkrieg gegen die Orks von draußen und gegen einen unsichtbaren Feind im Inneren.


Grauzwerge
----------

Die Geschichte der Grauzwerge füllt ganze Bibliotheken, und sie stehen für die meisten Menschen für den typischen Zwerg.
Sie leben im Andar, dem größten Gebirge Paleas.
Derzeit sind mindestens fünf große Reiche der Grauzwerge bekannt.
Jedes einzelne ist größer als alle Zwergenreiche außerhalb des Andars.
Umso unvorstellbarer ist die Größe des geeinten Reiches der Grauzwerge, das Garnog 737 A2 errichtete und das 1505 AZ auf dem Sterbebett Arnors zerbrach.

Die Grauzwerge schlugen in gigantischen Schlachten Armeen Noireth, die Uahren und waren Teil der Kräfte Paleas, als es um die Abwehr der Invasion ging.
Sie unterhalten ein weitverzweigtes Handelsnetzwerk und verfügen über die wohl reichste Kultur der Zwerge.

Ansonsten kann man sie zu Recht für den Archetypen eines Zwerges halten.


Tragurer („Die Verlorenen”)
---------------------------

Lediglich die Statur der Verlorenen erinnert an Zwerge und ihren Lebensraum unter Tage.
Ansonsten sind sie aber grundverschieden.
Sie haben glanzlose, leere Augen, graubleiche Haut, graue oder farblose Haare und zahllose Narben von barbarischen Ritualen.
Aber irgendetwas anderes ist unsagbarfurchteinflößend an ihnen.

Es ist kaum etwas über sie bekannt.
Es gibt keine Berichte über Kommunikation mit Nicht-Tragurern, kein Handel, kein friedlicher Kontakt.
Sie greifen ohne erkennbaren Grund andere Wesen an.
Sie hassen offensichtlich alle anderen Zwerge, und wo immer Tragurer auf andere Zwerge treffen, kämpfen sie bis zum Tod.

Sie fürchten kein Wasser und haben kein Wertgefühl für Edelmetalle oder andere Reichtümer.
Sie sind asozial, und doch funktionieren sie wie eine Maschine, als würde etwas anderes sie zusammenhalten und führen.
Es gibt keine erkennbare Kultur bis auf die Zeichen der rituellen Selbstverletzungen.

Begegnungen mit Tragurer hat man bislang aus dem Andar und dem Ghalgrat berichtet.

.. warning::

   Tragurer sollten nicht als Spielercharaktere verwendet werden.
   Sie sind „Monster“ und hassen alles andere/Leben.


Besondere Eigenschaften
-----------------------

Zwerge gelten als unverwüstlich, sie erhalten zu Beginn des Spiels zwei Extra-Trefferpunkte.

Zwerge sind auch bei 0 TP noch handlungsfähig, verlieren jedoch ebenfalls das Bewusstsein und beginnen zu sterben, wenn ihre Trefferpunkte in den negativen Bereich fallen (siehe S. 32).

Sie können in absoluter Dunkelheit sehen, indem sie zwischen Warm- und Kalttönen unterscheiden können.
Unter Tage sind sie so in der Lage, sich selbst in großen Tiefen ohne Licht zu orientieren.
Selbst kleinste Temperaturunterschiede zwischen Gesteinsschichten und Einschlüssen werden von ihnen wahrgenommen.

Zwerge haben eine Art sechsten Sinn, wenn es um die Entdeckung von verborgenen Mechanismen geht.
Sie können daher Fallen und Geheimtüren besser entdecken.

Da Zwerge kleiner sind, erhalten sie einen Büstungsbonus von +1.
Mehr dazu in der Übersichtstabelle Völker, Seite 10, und Im Spielleiterheft.
