Gnome
=====

Es gibt große Unterschiede unter den Gnomen, hier aber sollen nur zwei auffällige Arten unterschieden werden.
Die einen, die sich immer tiefer in Aborea graben, und die anderen, die mittlerweile den Weg auf die Oberfläche zurück gefunden haben.
Beiden ist gemein, dass sie nur 80cm im Schnitt groß werden, von schwächlicher Statur sind, aber eine hohe Fingerfertigkeit besitzen.
Sie gelten allesamt als Meister der Mechanik.
Wohl als Ausgleich für ihre körperlichen Schwächen haben sie Maschinen konstruiert, die graben, fördern und befördern können.
Aber auch vieles andere stammt aus ihren Werkstätten.
Beispielsweise sei da nur die Wasserförderanlage angeführt, die durch eine Schraube Wasser nach oben „dreht“.
Viele Menschen glauben, dies wäre der Verdienst des Gelehrten Phyran aus Elbia in Wirklichkeit hat er diese einfache aber effektive Mechanik einem Gnom namens Turk zu verdanken.

Gnome kränkeln schnell, und es scheint so, als habe das Leben unter Tage keinen guten Effekt auf sie.
Im Gegensatz zu den Zwergen, die sogar im Dunkeln sehen können, brauchen Gnome Licht aus Öllampen, Pechfackeln oder anderem, damit sie sich zurechtfinden.
Dazu kommt, dass viele von ihnen an einer Sehschwäche leiden.
Trotzdem treibt es sie tiefer in Aborea als alle anderen Kreaturen.

Eine kleinere Gruppe lebt auf der Oberfläche und frönt dort der Leidenschaft des Angelns und des Konstruierens.
Angeblich haben sie Kappen, die unsichtbar machen, und sind (trotz ihrer sehr trotzig wirkenden Art) außerordentlich hilfsbereit.
Beides zusammen macht sie zu den legendären unsichtbaren Helfern.
Leider ist dies ein Irrtum, denn einzig Kobolde und Pixies vollbringen dies und das aus unterschiedlichsten Gründen heraus.

Gnome lieben Fische.
Sie sind aber keine Seefahrer.
(Sie können deswegen auch gar nicht die Klabautermänner sein, wie einige mutmaßen.)
Sie nutzen kleine Ruderboote oder sitzen am Ufer eines unterirdischen oder oberirdischen Sees.
Sie gelten als absolut schwindelfrei, aber sie klettern nicht auf Berge.
Diese Eigenschaft ist dennoch sehr nützlich, wenn sie Hunderte von Metern tiefe Schächte in die Erde bohren, die senkrecht hinab fallen.
Auf Bäume allerdings steigen sie schon hinauf.
Eine Untergruppe wählt sogar die Wälder als Heimstatt und errichtet Häuser in den Bäumen.
Sie gelten aber als absolute Außenseiter.

Wie auch die Halblinge und Elfen haben Gnome spitze Ohren.
Sie gehören zu den eher kurzlebigen Völkern und werden im Schnitt 40 Jahre alt.
Allerdings erreichen einige von ihnen das extrem hohe Alter von über 1000 Jahren.
Die Zusammenhänge dabei sind bislang nicht geklärt.

Gnom-Familien sind klein, es gibt selten mehr als ein Kind.
Viele sterben bei der Geburt, und die ersten Jahre sind lebensgefährlich für die kleinen Gnome.
Sie haben in der Regel schwarze Haare und graue Augen.
Sie haben graue oder aschfahle, glatte Haut, und nur die ältesten werden später faltig.

Auf Palea mag es in der Summe kaum mehr als 50.000 Gnome geben.
Einige sprechen sogar von nur noch 10.000.
Man kann sie selten unter Menschen finden, manchmal in Höhlen oder aber in ihren Siedlungen tief in der Erde.

Es grenzt an ein Wunder, dass sie in den großen Tiefen überleben können.
Es gibt Spekulationen, dass dies mit Magie zusammenhängt.
Unter den Gelehrten gelten sie als überaus talentiert.
Andere sagen den Gnomen nach, mit den Elementen befreundet zu sein.
Fest steht nur, dass sie die vielleicht rätselhafteste Kultur unter den hier angeführten haben.
Eines der Rätsel ist es auch, warum der größte Teil der Gnome sich immer tiefer eingräbt.
Gnome verehren Erdgeister und Elementare.

Sprachen
--------

Die Sprache der Gnome ist knackend und klackernd, ihre Schrift besteht aus keilförmigen Zeichen.


Besondere Eigenschaften
-----------------------

Gnome haben ein sagenhaftes Verständnis für Mechaniken.
Sie können daher auch Geheimtüren und Fallen besser wahrnehmen (+1).
Außerdem sind sie auf Grund ihrer Körpergröße ein schwierigeres Ziel und erhalten einen Bonus von +1 auf Rüstung.
Mehr dazu in der Übersichtstabelle Völker, Seite [O, und im Spielleiterheft.
