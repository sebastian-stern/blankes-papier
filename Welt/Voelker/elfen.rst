Elfen
=====

Gerade einmal auf knapp 5 Millionen wird die Population aller Elfen auf Palea geschätzt.
Sie sind damit die zahlenmäßig kleinste Gruppe der Großen Völker.
Viele Menschen leben ihr gesamtes Leben, ohne jemals einem Elfen zu begegnen.
Sie sind aber bis zu zwei Meter groß, wiegen selten mehr als achtzig Kilogramm, und die meisten wirken zart und behände.
Ihr Geschick ist außerordentlich, und sie gelten als Meister der Bogenkunst.
Nahezu lautlos sind sie unterwegs, und wenn man es nicht besser wüsste, könnte man meinen, sie hätten die Fähigkeit, sich unsichtbar zu machen.

Elfen sind ungewöhnlich schön, und ihre Augen vermögen die Herzen zu verzaubern.
Sie tragen ihr Haar, das nur sehr selten rot ist, meist lang, manchmal geflochten.
Ihre Gesichter sind schmal, und die Haut ist wie aus feinstem Marmor.
Besonders auffällig sind ihre spitzen Ohren.
Sie kleiden sich in naturfarbenen Leinen oder Leder, aber können auch in feinsten Seidenkleidern erscheinen, die selten eine andere Farbe als Weiß zeigen.

Die Elfen oder auch Elben, wie sie mancherorts genannt werden sind auch in den Nächten aktiv, tanzen in Kreisen und musizieren, oder sie meditieren (was bei einer vierstündigen Dauer einer Erholung von acht Stunden Schlaf gleichzusetzen ist).
Sie können auch bei nur schwachem Sternenlicht hervorragend sehen (bei Tageslicht sagt man ihnen sogar die Augenschärfe eines Raubvogels nach).
Besonders bei mondhellen Nächten begehen sie zuweilen rituelle Feste, die komplizierten Zeremonien folgen.
Dann fühlen sie sich ihren Schwestern und Brüdern nahe (siehe Religion der Elfen).

.. Link auf Religionen

Elfen streben nach Perfektion und versuchen, die Harmonie mit der Einen Melodie zu erreichen.
Auch wenn sie unsterblich sind, ist das Erreichen dieses Zieles zugleich Anfang und Ende.
Sie wandeln sich dann und gehen in der Einen Melodie auf.
Dieser Zeitpunkt ist individuell, aber der Durchschnitt liegt bei ungefähr 800 Jahren.
Um diese Harmonie zu erreichen, versuchen sie im Einklang mit der Natur und Schöpfung der Gottheit Othil zu leben.

.. Link auf Othil

Sie lieben daher das Leben in der Freiheit, naturverbunden und unter dem offenen (Sternen-) Himmel.
Manche Kulturen errichten beispielsweise ihr Heim in Zelten und Jurten, andere formen filigrane Paläste in oder an Bäumen, und wieder andere konstruierten in schwindelnde Höhen stakende Türme, die an gigantische Sprösslinge erinnern.
Sie verstehen es wie kaum ein anderes Volk, in Jahrhunderte dauernder Arbeit vortreffliche Kunstwerke zu erschaffen.
Kein Wunder also, dass Gäste anderer Völker die Wunder der elfischen Heime kaum fassen können, ist doch kaum etwas damit vergleichbar.

Diese Momente sind allerdings äußerst selten.
Denn Elfen sind scheu, zurückhaltend und bleiben gerne unter sich.
Sie misstrauen den wenig erfahrenen und kurzlebigen Völkern, die sie teils für unreif erachten, aber dennoch als Schöpfung Othils respektieren.
Immer wieder aber erlangen einzelne Individuen die elfische Achtung, wenn sie in wenigstens einem Aspekt den Einklang mit der Einen Melodie innerhalb ihrer so kurzen Zeit schon erzielt haben.
In kontinentalen Konflikten ist es bisher nur solchen Personen (manchmal) gelungen, die Elfen in eine wenigstens vorübergehende Allianz zu ziehen.
In aller Regel nämlich sind sie neutral und sehen solche Konflikte als „natürlich“ und nur „vorübergehend" an.
Man kann dann auf kleinere Hilfen von ihnen hoffen, die dann meist in der Heil- oder Zauberkunst der Elfen besteht.
Um eine solche Bitte vorzutragen, muss man die Elfen aber zunächst aufsuchen und folglich finden können, was an sich bereits ein schweres Unterfangen ist.
Ihre Siedlungen werden durch starke Zauber verborgen und geschützt.

Absolute Ausnahmen sind Elfen, die unter Menschen leben oder nahe bei den Siedlungen der Menschen.
Gründe für ein solch sonderliches Verhalten könnte eine Mission, (unnatürliche) Neugier, die Liebe zu einem Menschen oder andere Motive sein.

Wenigen Menschen ist es vergönnt, die Gemeinschaft der Elfen zu besuchen und noch weniger erlangen die Ehre, sogar die Künste bei ihnen zu erlernen.

Noch seltener ist die Zusammenarbeit zwischen Elfen und Zwergen.
Zwerge symbolisieren in vielen Bereichen das Gegenteil der elfischen Kultur.
Sie gelten als impulsiv, unüberlegt, aggressiv, dreckig und zu emotional.
Insbesondere aber schreckt sie das Leben unter Tage ab.
Vielleicht weil es ihnen vor einem Leben ohne offenen Himmel graust, vielleicht aber auch, weil es die Heimat der Schwarzelfen und anderer Geschöpfe der dunklen Gottheit Næroths ist.

.. Link auf Næroth


Sprachen
--------

Im Gegensatz zu den Zwergen haben die Elfen ein Alphabet und verfügen über keine gemeinsame (Kern-) Schrift, sondern alleine auf Palea gibt es gut ein Dutzend verschiedene Zeichensätze.
Es lassen sich aber drei Sätze verzeichnen, die am weitesten verbreitet sind.
Deren Alphabet besteht teilweise aus über 32 Zeichen.
Hinzu kommen etliche Zusatzzeichen.

Die Aussprache ist weich und melodisch.
Das T wird zum Beispiel dz und th mit δ ausgesprochen, Æ wie ai und ï wie ei.

Erstaunlich ist der Wortschatz, der in Summe als viermal so groß wie der größte unter den Menschen geschätzt wird.
Kennzeichnend für alle elfischen Sprachen ist die schwache Bedeutung des Wortes „Ich".
Dafür gibt es aber viele Variationen von „Wir”.
Eine davon kann mit „Ich” übersetzt werden.
Allerdings gibt es „Mein" und „Dein“.

Beispiele für die elfische Ausdrucksform wären:

*Wir musizieren mit Harfen und Flöten*.

:llar mÆne Leani eanÆ:

oder in Bildsprache

.. Bild einfügen

oder in einer Mischung aus Bildund Zeichensprachen:

.. Bild einfügen

Im Einzelnen gibt es große Unterschiede zwischen den Elfenvölkern und Arten.
Eine Auswahl findet sich im Folgenden.


Wildelfen (Zendarin)
--------------------

Die größte Gruppe unter den Elfen stellen die Wildelfen.
Sie sind die klassische Elfenart.
Sie sind besonders resistent gegen Krankheiten, und ihre Selbstheilungskräfte schaffen es sogar, Narbenbildung zu verhindern.
Sie haben häufig eine außerordentliche Begabung für Magie.

Sie leben zwar in großen Gemeinschaften, aber nur selten binden sie sich an einen Partner.
Lediglich ein Drittel findet eine solche Person, die dann auch als der fehlender Teil der Einen Melodie verstanden wird.
Daher hält eine solche Bindung ewig.
Stirbt der Partner, ist der Überlebende regelmäßig zu einem Leben in Disharmonie verurteilt.
Ein Ausweg daraus ist der zeremonielle Übergang in die Welt der Schatten.

Dieses Schicksal ereilt all jene, die sich mit einem Sterblichen einen.
Die Nachkommen aus einer solchen Beziehung sind dann sogenannte Halbelfen.
(Wobei Nachkommen sowieso äußerst selten sind. Die Zahl der Elfen nimmt daher ab gerade bei kriegerischen Konflikten sprunghaft.)

Die Wildelfen nennen sich selbst nur „Elfen“ und sind eigentlich, streng wissenschaftlich betrachtet, Dunkelelfen.
Allerdings ist es der Einfachheit der Menschen geschuldet, dass der Name Dunkelelfen mittlerweile für die Schwarzelfen steht.
Selbst der Name „Elfen“ wird von den Menschen mal für Feen, mal für Pixies und mal für die Wildelfen gebraucht.
Alle drei Spezies sind in der Tat auch verwandt, doch die sprachliche Ungenauigkeit der Menschen hat damit wenig zu tun.


Lichtelfen (Luminör)
--------------------

In dem Moment, wenn ein Wildelf den Zustand absoluter Harmonie erreicht, wird er zu einem sogenannten Lichtelfen.
Dieses Stadium ist eine Warteposition, die dem richtigen Zeitpunkt für den Übergang entgegen strebt.
Dabei können wenige Stunden vergehen oder bis zu 80 Jahre.

Der Wandel vom „normalen Elfen” zum Lichtelfen passiert dabei fließend.
Ihre Haut wird makelloser und scheint von selbst zu strahlen, so als würde sie aus Mondlicht bestehen.
Ihre Haare bleichen aus, und sie wirken unendlich ruhig.
Ihre Handlungen sind langsam und von einer Endgültigkeit geprägt.
Gleichzeitig verblassen ihre Erinnerungen an alles Vergängliche.

Lichtelfen sind der Grund für die wundersamsten Geschichten.
Viele Kinder lauschen den Legenden von Lichtelfen, und wenn sie tatsächlich einmal einer solchen Elfe begegnen, erfüllt sich ein Lebenstraum und sie zehren lange von der damit einhergehenden Hoffnung auf alles Gute.

Die kleinste der Elfenarten entwickelt ausgeprägte heilende Kräfte.
Es gibt sogar Gerüchte über lebensschenkende Zauber.


Schwarzelfen (Unzadrun oder Noireth)
------------------------------------

Umgangssprachlich (aber wissenschaftlich nicht zutreffend) werden die Schwarzelfen auch Dunkelelfen genannt.
Sie unterscheiden sich gravierend von allen anderen Elfen und gelten als die Kinder Næroths.

Sie sehen nahezu perfekt in absoluter Dunkelheit und scheuen das Sonnenlicht.
Sie leben in unterirdischen Palästen, wo sie ihre Fähigkeit nutzen, Edelsteine und Kristalle zum Leuchten zu bringen.
Überhaupt haben sie eine eigenartige Verbundenheit zu bestimmten Edelsteinen, angeblich können sie sogar darin leben.

Schwarzelfen scheinen alles andere Lebende zu verachten.
Als Unsterbliche sehen sie alles Sterbliche als minderwertig an.
Sie streben ebenfalls nach Perfektion, aber völlig verschieden zu den Wildelfen.
Perfektion drückt sich auch in Stärke (angeborener Stärke) aus, und wer zu schwach bei der Geburt ist, wird als unwürdig getötet.

Etwas kleinwüchsiger als ihre oberirdischen „Verwandten“ aber stärker als diese, sind sie kampfstarke „Wald”-läufer.
Ihr Verständnis für die Magie und ihre aggressive Natur stellen eine schreckliche Kombination dar.
Außerdem sind sie herausragende Schmiede, aber ihre Waffen sind verflucht (für alle anderen).

Wenn ihre Territorien an die anderer Völker grenzen, dann stellen sie ihren Anspruch mit Gewalt klar, wie wenn ein tyrannischer Vater einem unliebsamen Kind mit Schlägen die Grenzen aufweist.
Ihrem Glauben nach sind Sterbliche mit Intelligenz und Bewusstsein Missgeburten und Fehler der Natur.
Es ist so gut wie nichts über die schwarzelfische Kultur bekannt.
Aber es scheint richtige Kreuzzüge zu geben, die Aborea von den missgestalteten Kreaturen reinigen sollen.


Schattenelfen
-------------

Die Schattenelfen, oder Elfen des Zwielichts, sollen hier nur der Vollständigkeit wegen angeführt werden.
Sie sind Geister und gelten als gut, aber eigensinnig und sonderbar.
Es handelt sich um einstige Wildelfen, die gewaltsam aus dem Leben gerissen wurden, bevor sie den Übergang erreicht haben.
Im Zwielicht bekämpfen sie Næroth (siehe Religion der Elfen).


Halbelfen
---------

Halbelfen sind äußerst selten und entstammen regelmäßig der Verbindung eines Elfen mit einem Menschen.
Richtigerweise müsste man Halbmenschen sagen, denn alles, was sie von ihrem elfischen Elternteil erben, ist die Neigung zur Magie und manchmal (schwach ausgeprägt) die Gesichtszüge und die Ohren.
Sie fühlen sich zwischen zwei Welten gefangen und werden weder von der einen, noch der anderen richtig akzeptiert.


Kristallelfen
-------------

Kaum mehr als die Lichtelfen zählen die Kristallelfen.
Gelehrte würden anmerken, dass es sich doch eigentlich um Wildelfen handelt, die aber auf Grund sonderbarer Begebenheiten rote Augen und weißes Haar von Geburt an haben.
Recht früh verlassen sie freiwillig ihr Elternhaus und ihre Wildelfen Gemeinschaft.
Sie fühlen sich nicht verstanden und ausgegrenzt.
In einer manchmal Jahrzehnte dauernden Suche finden sie Ihresgleichen.
In Gebirgswäldern und versteckten Tälern leben sie dann in verschworenen Gruppen, verbergen sich vor allen anderen Völkern und bleiben unter sich.
Nur einer von Tausend wird außerhalb der einmal gefundenen neuen Heimat auf Abenteuer oder mit einer Mission unterwegs sein.

Ihren Namen haben sie von ihrer Leidenschaft, Monumente aus Kristallen zu schaffen.
Auch ihre Waffen sind aus Kristall gefertigt, und man behauptet, dass sie große Magie in diese zu legen pflegen.

Kristallelfen engagieren sich für ihre Umwelt und nehmen aktiv am Kampf gegen die Geschöpfe Næroths teil.


Besondere Eigenschaften
-----------------------

Elfen haben besonders gute Augen.
Sie erhalten deswegen einen Bonus von +1 auf entsprechende Manöver.
Elfen sind gegen natürliche Krankheiten immun.
Mehr dazu siehe auf Seite 10, Übersichtstabelle Völker und Spielleiterheft.

.. Link auf "Seite" 10