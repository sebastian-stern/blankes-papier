Kleine Völker
=============

Zunächst sollte klar gestellt werden, dass die kleinen Völker keineswegs unbedingt von geringerer Menge sind, aber in der Lehre Paleas als nicht so bedeutend gelten.
Ferner muss man festhalten, dass eine Vielzahl von Lebewesen auf Palea existieren und hier nur eine kleine Auswahl dargestellt werden kann.
Einige davon könnte man auch generalisierend als Monster bezeichnen daher sehen Sie es nach, wenn einige hier und andere im Kapitel "Monster", Spielleiterheft, verzeichnet sind.

.. admonition:: Halblinge
    :class: admonition note

    Der Name „Halblinge“ ist eine einfache Beschreibung.
    Halblinge sind nur „halb so groß wie Menschen“ sagt man.
    Im Schnitt stimmt dies nicht, denn der liegt bei einem Meter.
    Sie bringen dabei zwischen 0,5 und 0,8kg pro cm auf die Waage.

.. admonition:: Snerge
    :class: admonition note

    Unter den Zwergen werden Halblinge Snerge genannt.
    Man weist jegliche Verwandtschaft der Art weit von sich und hält Snerge für nutzlose Erdwühler.
    Es gibt Spottgeschichten, die dies auf die zwergische Spitze treiben und von Snergen berichten, die einen Hügel mit einem Edelmetallvorkommen ausgeräumt haben.
    Danach hätten sie den wertvollen Teil des Aushubs liegen lassen, da der neu entstandene Hügel gar passend für den Setzling einer Grüneiche gewesen sei.
