Menschen
========

Keines der Großen Völker ist so groß, keines so reich an Varianten wie das der Menschen.

Im Norden gibt es Menschen, die über zwei Meter groß werden.
Im Süden dagegen lebt ein anderer Schlag, diese Menschen werden im Schnitt nur 1,60 Meter groß.

Es gibt muskulöse, gerade bei den körperlich hart arbeitenden, und schwächliche Menschen.
Sie können ausgehungert sein oder sich fettleibig zeigen.
Der durchschnittliche Palea-Mensch ist ein Meter achtzig und dabei 72 Kilogramm schwer wobei das Mittel hier kaum aussagekräftig ist.
Ihre Hautfarbe ist regelmäßig der klimatischen Zone entsprechend von dunklem Teint (Süden) bis blass (Norden).
Ihre Haarfarben können nach Häufigkeit sortiert werden: Blond ist häufiger im Norden, im Süden herrschen dunklere Farben vor.
Aber auch dies ist stark verallgemeinernd, denn einzelne Kulturen meiden die Sonne (und weisen trotz südlicher Lage eine helle Haut auf) oder haben einen hohen Anteil an roten Haaren.

Menschen werden im Mittel 50 Jahre alt.

Diese Vielseitigkeit drückt sich auch ansonsten aus.
Ihre körperlichen Eigenschaften sind absolut ausgeglichen (keine Attributsbonusse oder Malusse).
Sie sind manchmal hier, mal dort begabt, und jeder entwickelt individuelle Stärken und Schwächen.

Sie werden klar von ihrem Kulturraum und der näheren Umgebung geprägt, besitzen aber einen ungewöhnlich starken Willen.

Kein Wunder, dass die meisten Geschichten und Legenden tatsächlich von Menschen handeln.
Sie wachsen über sich und alle Maße hinaus und wissen stets zu überraschen.

Sie leben überall, auch wenn sie oberirdisches Leben dem Leben unter Tage vorziehen.
Einige behaupten sogar, dass es für den Menschen unmöglich wäre, ständig unter der Erde zu leben.
Andererseits verfügen die Menschen über eine unglaubliche Anpassungsfähigkeit.

Es existieren Kulturen und Gemeinschaften wie Sand am Meer.
Die Kulturen und Religionen der Menschen unterscheiden sich extrem.

Elfen und Zwerge halten Menschen für selbstsüchtig, besitzergreifend, gierig und aggressiv.
Schon lange wurde den Menschen ein selbstzerfleischendes Ende vorhergesagt.
Ihr kurzes Leben verleite sie zu einem Höchstmaß an Egoismus.
Dazu würden sie letzten Endes doch immer zum Schwert als Lösung von Konflikten greifen.
Und doch sind es die Menschen, die Reiche im Frieden erblühen lassen und die sich für ihre Kinder, die Freiheit und andere Menschei aufopfern, die einander helfen und beistehen und hohe Künste und Ideale besitzen.

Die Menschen haben keinen wirklichen Erzfeind, sie sind in der Lage, Kriege zu führen, Frieden zu schließen, nachtragend zu sein und zu verzeihen.
Eines zeichnet Menschen auch noch besonders aus: Sie haben einen Forschungsdrang.
Während andere Völker durchaus mit der Tatsache leben können, dass sie nicht alles wissen, reisen Menschen mehr als andere Völker in unbekannte Länder, nehmen Gefahren auf sich, hinterfragen und streben nach mehr Wissen.


Sprachen
--------

Es gibt einen ganzen bunten Blumenstrauß an Sprachen, Akzenten und Mundarten bei den Menschen. Bisweilen reichen wenige Kilometer schon für einen neuen Dialekt. Die Übergänge sind dabei fließend, und nur dort, wo durch gewaltsame Landnahme oder andere plötzliche Ereignisse die Verhältnisse sich ändern, kann es zu starken Differenzen kommen.

Weit verbreitet ist die Handelssprache, und unter den „Bessergestellten" spricht man gerne Imperial (siehe Spielleiterheft).


Besondere Eigenschaften
-----------------------

Menschen sind sehr vielseitig und lernen schnell.
Sie können daher jeden Beruf wählen und erhalten zwei Ausbildungspunkte zu Beginn mehr als andere Völker.
